<?php

require_once('config/config.php');
require_once('../application/vendor/twitter/TwitterOAuth.php');

$config = new Config();
$conectar = $config->conectar();
$conectar->exec("SET NAMES 'utf8' COLLATE 'utf8_general_ci'");

$sql = "select ri.*,ra.access from redes_autorizacion ra "
        . "inner join redes_item ri on ra.id = ri.autorizacion_id "
        . "where ra.social_id = 2";

$stmt = $conectar->prepare($sql);
$stmt->execute();
$lista =  $stmt->fetchAll(PDO::FETCH_ASSOC);

foreach ($lista as $value) 
{
    $access_token = json_decode($value["access"], true);
    $twitter = new TwitterOAuth($access_token['app_key'],$access_token['app_secret'],$access_token['oauth_token'],$access_token['oauth_token_secret']); 
    
    $hoy = date("Y-m-d",strtotime("-1 day"));
    
    $fecha_inicio = $hoy." 00:00:00" ;
    $fecha_fin = $hoy." 23:59:59" ;
    
    $lineadetiempo = array();

    if(isset($value["date_time"]) and !empty($value["date_time"]))
    {
        $lineadetiempo = json_decode($value["date_time"], true);
    } 
    
    $anio = date("Y",strtotime($hoy));
    $mes = date("m",strtotime($hoy));
    $dia = date("d",strtotime($hoy));
    $lineadetiempo[$anio."-".$mes."-".$dia][0] = followers($twitter,$fecha_inicio,$fecha_fin);
    $lineadetiempo[$anio."-".$mes."-".$dia][1] = retweets($twitter,$fecha_inicio,$fecha_fin);      
    $lineadetiempo[$anio."-".$mes."-".$dia][3] = listed_count($twitter,$fecha_inicio,$fecha_fin);      
    $lineadetiempo[$anio."-".$mes."-".$dia][4] = top_retweeted_posts($twitter,$fecha_inicio,$fecha_fin);
    $lineadetiempo[$anio."-".$mes."-".$dia][5] = top_mentions_by_followers_count($twitter,$fecha_inicio,$fecha_fin); 
    
    $sql = "update redes_item set date_time = '".json_encode($lineadetiempo)."' where id = ".$value["id"];
    $stmt = $conectar->prepare($sql);
    $stmt->execute();
}

    function followers($user,$fecha_inicio,$fecha_fin)
    {      
        $aux = $user->get("account/verify_credentials");
        return $aux->followers_count;
    }

    function retweets($user,$fecha_inicio,$fecha_fin)
    {
        $parametros = array();
        $parametros["exclude_replies"] = true;
        $parametros["include_rts"] = false;
        $parametros["count"] = 200;
        $tweets = $user->get("statuses/mentions_timeline", $parametros);
        return retweetsTotal($tweets,$parametros,$fecha_inicio, $fecha_fin, $user);
    }

    function listed_count($user,$fecha_inicio,$fecha_fin)
    {
        $user = $user->get("account/verify_credentials");
        return $user->listed_count;
    }

    function top_retweeted_posts($user,$fecha_inicio,$fecha_fin)
    {
        $parametros = array();
        $parametros["exclude_replies"] = true;
        $parametros["include_rts"] = true;
        $parametros["count"] = 200;

        $tweets = $user->get("statuses/retweets_of_me", $parametros);
        
        return top_retweets($tweets,$parametros,$fecha_inicio, $fecha_fin, $user);
    }

    function top_mentions_by_followers_count($user,$fecha_inicio,$fecha_fin)
    {
        $parametros = array();
        $parametros["exclude_replies"] = true;
        $parametros["include_rts"] = false;
        $parametros["count"] = 200;

        $tweets = $user->get("statuses/mentions_timeline", $parametros);
        return top_mentions($tweets,$parametros, $fecha_inicio, $fecha_fin, $user);
    }

    function check_in_range($start_ts, $end_ts, $date_from_user)
    {
      // Convert to timestamp
      date_default_timezone_set('America/Lima');
      $start_ts = strtotime($start_ts);
      $end_ts = strtotime($end_ts);
      $user_ts = strtotime($date_from_user);

      // Check that user date is between start & end
      if(($user_ts >= $start_ts) and ($user_ts <= $end_ts)) 
      {
          return 1;
      }else if ($user_ts > $end_ts)
      {
          return 2;
      }else {
          return 3;
      }
    }

    function top_retweets($retweets,$parametros,$fecha_desde, $fecha_hasta, $user, $lista_tweets = array())
    {
        $total = count($retweets);
        $inicio = 0;

        foreach($retweets as $tweet)
        {
            $inicio ++;
            $fecha_tweet = date('Y-m-d H:i:s', strtotime($tweet->created_at));
            
            if(check_in_range($fecha_desde,$fecha_hasta,$fecha_tweet) == 1)
            {
                if(isset($tweet->retweeted_status))
                {
                   array_push($lista_tweets, array("cantidad" => $tweet->retweet_count, "texto" => $tweet->text, "id" =>$tweet->id_str)); 
                }
            }else if(check_in_range($fecha_desde,$fecha_hasta,$fecha_tweet) == 3){
                
                return $lista_tweets;
            }

            if($total == $inicio)
            {
                $parametros["max_id"] = $tweet->id_str;
                $retweets = $user->get("statuses/retweets_of_me", $parametros); 
                
                return array_merge($lista_tweets,top_retweets($retweets,$parametros,$fecha_desde, $fecha_hasta,$user,$lista_tweets));  
            }
        }          
    }
    
   function top_mentions($menciones,$parametros,$fecha_desde, $fecha_hasta, $user, $lista_menciones = array())
    {
        $total = count($menciones);
        $inicio = 0;

        foreach($menciones as $tweet)
        {
            $inicio ++;
            $fecha_tweet = date('Y-m-d H:i:s', strtotime($tweet->created_at));
            
            if(check_in_range($fecha_desde,$fecha_hasta,$fecha_tweet) == 1)
            {
                array_push($lista_menciones, array("cantidad" => $tweet->retweet_count, "texto" => $tweet->text, "id" =>$tweet->id_str)); 
            }else if(check_in_range($fecha_desde,$fecha_hasta,$fecha_tweet) == 3){
                
                return $lista_menciones;
            }
            
            if($total == $inicio)
            {
                $parametros["max_id"] = $tweet->id_str;
                $menciones = $user->get("statuses/mentions_timeline", $parametros); 
                
                return array_merge($lista_menciones,top_mentions($menciones,$parametros,$fecha_desde, $fecha_hasta,$user,$lista_menciones));  
            }
        }          
    }

    function retweetsTotal($tweets,$parametros,$fecha_desde, $fecha_hasta,$user)
    {
        $total = count($tweets);
        $inicio = 0;
        $suma = 0;

        foreach($tweets as $tweet)
        {
            $inicio ++;
            $fecha_tweet = date('Y-m-d H:i:s', strtotime($tweet->created_at));
            
            if(check_in_range($fecha_desde,$fecha_hasta,$fecha_tweet) == 1)
            {   
                if(isset($tweet->retweeted_status))
                {
                $suma += (int) $tweet->retweet_count;
                }
            }else if(check_in_range($fecha_desde,$fecha_hasta,$fecha_tweet) == 3){
                return $suma;
            }
            
            if($total == $inicio)
            {
                $parametros["max_id"] = $tweet->id_str;
                $tweets = $user->get("statuses/user_timeline", $parametros); 
                return $suma + (int)retweetsTotal($tweets,$parametros,$fecha_desde, $fecha_hasta,$user);  
            }
        }          
    }
?>