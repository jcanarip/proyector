<?php
class Config
{
    public $conf;

    public function Config()
    {
        $this->conf = array();

        $host        = 'tribal121.com';
        $user        = 'tribu121_proyect';
        $password    = 'tribal121';
        $bdatos    = 'tribu121_proyecto121';

        $this->conf['servidor'] = $host;
        $this->conf['usuario']  = $user;
        $this->conf['password'] = $password;
        $this->conf['bdatos']   = $bdatos;
    }

    public function conectar()
    {
        date_default_timezone_set('America/Lima');
        $servidor = $this->conf['servidor'];
        $usuario  = $this->conf['usuario'];
        $password = $this->conf['password'];
        $bdatos   = $this->conf['bdatos'];

        return new PDO('mysql:host='.$this->conf['servidor'].';dbname='.$this->conf['bdatos'], 
                $this->conf['usuario'], 
                $this->conf['password'],
                array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                ));
    }

}
?>