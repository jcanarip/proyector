<div id="message"class="alert alert-<?php echo $message->type; ?>" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <ul>
<?php
	if( is_array( $message->message ) ):
		foreach( $message->message as $msg ): ?>
	<li><?php echo $msg; ?></li>
<?php
		endforeach;
	else: ?>
	<li><?php echo $message->message; ?></li>
<?php endif; ?>
    </ul>
</div>