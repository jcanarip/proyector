var URL = "http://www.tribal121.com/proyectotribal121/";
function Alerta(msg) {
    jQuery.prompt(msg, { persistent: false });
}

function startTour(h) 
{
    var tour = introJs()
    tour.setOption('tooltipPosition', 'auto');
    tour.setOption('positionPrecedence', ['left', 'right', 'bottom', 'top'])
    tour.start();
    
    tour.oncomplete(function() {
        document.cookie  = "value="+$.base64.encode(h);
    });
    
    tour.onexit(function() {
        document.cookie  = "value="+$.base64.encode(h);
    });
    
}

jQuery(function () {
    //BEGIN MENU SIDEBAR
    jQuery('#sidebar').css('min-height', '100%');
    jQuery('#side-menu').metisMenu();

    jQuery(window).bind("load resize", function () {
        if (jQuery(this).width() < 768) {
            jQuery('div.sidebar-collapse').addClass('collapse');
        } else {
            jQuery('div.sidebar-collapse').removeClass('collapse');
            jQuery('div.sidebar-collapse').css('height', 'auto');
        }
        if(jQuery('body').hasClass('sidebar-icons')){
            jQuery('#menu-toggle').hide();
        } else{
            jQuery('#menu-toggle').show();
        }
    });
    //END MENU SIDEBAR

    //BEGIN TOPBAR DROPDOWN
    jQuery('.dropdown-slimscroll').slimScroll({
        "height": '250px',
        "wheelStep": 5
    });
    //END TOPBAR DROPDOWN

    //BEGIN CHECKBOX & RADIO
    /*if(jQuery('#demo-checkbox-radio').length <= 0){
        jQuery('input[type="checkbox"]:not(".switch")').iCheck({
            checkboxClass: 'icheckbox_minimal-grey',
            increaseArea: '20%' // optional
        });
        jQuery('input[type="radio"]:not(".switch")').iCheck({
            radioClass: 'iradio_minimal-grey',
            increaseArea: '20%' // optional
        });
    }*/
    //END CHECKBOX & RADIO

    //BEGIN TOOTLIP
    jQuery("[data-toggle='tooltip'], [data-hover='tooltip']").tooltip();
    //END TOOLTIP

    //BEGIN POPOVER
    jQuery("[data-toggle='popover'], [data-hover='popover']").popover();
    //END POPOVER

    //BEGIN THEME SETTING
    jQuery('#theme-setting > a.btn-theme-setting').click(function(){
        if(jQuery('#theme-setting').css('right') < '0'){
            jQuery('#theme-setting').css('right', '0');
        } else {
            jQuery('#theme-setting').css('right', '-250px');
        }
    });

    // Begin Change Theme Color
    var list_style = jQuery('#theme-setting > .content-theme-setting > select#list-style');
    var list_color = jQuery('#theme-setting > .content-theme-setting > ul#list-color > li');
    // FUNCTION CHANGE URL STYLE ON HEAD TAG
    var setTheme = function (style, color) {
        $.cookie('style',style);
        $.cookie('color',color);
        jQuery('#theme-change').attr('href', 'css/themes/'+ style + '/' + color + '.css');
    }
    // INITIALIZE THEME FROM COOKIE
    // HAVE TO SET VALUE FOR STYLE&COLOR BEFORE AND AFTER ACTIVE THEME
    if ($.cookie('style')) {
        list_style.find('option').each(function(){
            if(jQuery(this).attr('value') == $.cookie('style')) {
                jQuery(this).attr('selected', 'selected');
            }
        });
        list_color.removeClass("active");
        list_color.each(function(){
            if(jQuery(this).attr('data-color') == $.cookie('color')){
                jQuery(this).addClass('active');
            }
        });
        setTheme($.cookie('style'), $.cookie('color'));
    };
    // SELECT EVENT
    list_style.on('change', function() {
        list_color.each(function() {
            if(jQuery(this).hasClass('active')){
                color_active  = jQuery(this).attr('data-color');
            }
        });
        setTheme(jQuery(this).val(), color_active);
    });
    // LI CLICK EVENT
    list_color.on('click', function() {
        list_color.removeClass('active');
        jQuery(this).addClass('active');
        setTheme(list_style.val(), jQuery(this).attr('data-color'));
    });
    // End Change Theme Color
    //END THEME SETTING

    //BEGIN FULL SCREEN
    jQuery('.btn-fullscreen').click(function() {

        if (!document.fullscreenElement &&    // alternative standard method
            !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
            if (document.documentElement.requestFullscreen) {
                document.documentElement.requestFullscreen();
            } else if (document.documentElement.msRequestFullscreen) {
                document.documentElement.msRequestFullscreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullscreen) {
                document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }
        }
    });
    //END FULL SCREEN

    // BEGIN FORM CHAT
    jQuery('.btn-chat').click(function () {
        if(jQuery('#chat-box').is(':visible')){
            jQuery('#chat-form').toggle('slide', {
                direction: 'right'
            }, 500);
            jQuery('#chat-box').hide();
        } else{
            jQuery('#chat-form').toggle('slide', {
                direction: 'right'
            }, 500);
        }
    });
    jQuery('.chat-box-close').click(function(){
        jQuery('#chat-box').hide();
        jQuery('#chat-form .chat-group a').removeClass('active');
    });
    jQuery('.chat-form-close').click(function(){
        jQuery('#chat-form').toggle('slide', {
            direction: 'right'
        }, 500);
        jQuery('#chat-box').hide();
    });

    jQuery('#chat-form .chat-group a').unbind('*').click(function(){
        jQuery('#chat-box').hide();
        jQuery('#chat-form .chat-group a').removeClass('active');
        jQuery(this).addClass('active');
        var strUserName = jQuery('> small', this).text();
        jQuery('.display-name', '#chat-box').html(strUserName);
        var userStatus = jQuery(this).find('span.user-status').attr('class');
        jQuery('#chat-box > .chat-box-header > span.user-status').removeClass().addClass(userStatus);
        var chatBoxStatus = jQuery('span.user-status', '#chat-box');
        var chatBoxStatusShow = jQuery('#chat-box > .chat-box-header > small');
        if(chatBoxStatus.hasClass('is-online')){
            chatBoxStatusShow.html('Online');
        } else if(chatBoxStatus.hasClass('is-offline')){
            chatBoxStatusShow.html('Offline');
        } else if(chatBoxStatus.hasClass('is-busy')){
            chatBoxStatusShow.html('Busy');
        } else if(chatBoxStatus.hasClass('is-idle')){
            chatBoxStatusShow.html('Idle');
        }


        var offset = jQuery(this).offset();
        var h_main = jQuery('#chat-form').height();
        var h_title = jQuery("#chat-box > .chat-box-header").height();
        var top = (jQuery('#chat-box').is(':visible') ? (offset.top - h_title - 40) : (offset.top + h_title - 20));

        if((top + jQuery('#chat-box').height()) > h_main){
            top = h_main - 	jQuery('#chat-box').height();
        }

        jQuery('#chat-box').css({'top': top});

        if(!jQuery('#chat-box').is(':visible')){
            jQuery('#chat-box').toggle('slide',{
                direction: 'right'
            }, 500);
        }
        // FOCUS INPUT TExT WHEN CLICK
        jQuery('ul.chat-box-body').scrollTop(500);
        jQuery("#chat-box .chat-textarea input").focus();
    });
    // Add content to form
    jQuery('.chat-textarea input').on("keypress", function(e){

        var $obj = jQuery(this);
        var $me = $obj.parent().parent().find('ul.chat-box-body');
        var $my_avt = 'https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg';
        var $your_avt = 'https://s3.amazonaws.com/uifaces/faces/twitter/alagoon/48.jpg';
        if (e.which == 13) {
            var $content = $obj.val();

            if ($content !== "") {
                var d = new Date();
                var h = d.getHours();
                var m = d.getMinutes();
                if (m < 10) m = "0" + m;
                $obj.val(""); // CLEAR TEXT ON TEXTAREA

                var $element = ""; 
                $element += "<li>";
                $element += "<p>";
                $element += "<img class='avt' src='"+$my_avt+"'>";
                $element += "<span class='user'>John Doe</span>";
                $element += "<span class='time'>" + h + ":" + m + "</span>";
                $element += "</p>";
                $element = $element + "<p>" + $content +  "</p>";
                $element += "</li>";
                
                $me.append($element);
                var height = 0;
                $me.find('li').each(function(i, value){
                    height += parseInt(jQuery(this).height());
                });

                height += '';
                //alert(height);
                $me.scrollTop(height);  // add more 400px for #chat-box position      

                // RANDOM RESPOND CHAT

                var $res = "";
                $res += "<li class='odd'>";
                $res += "<p>";
                $res += "<img class='avt' src='"+$your_avt+"'>";
                $res += "<span class='user'>Swlabs</span>";
                $res += "<span class='time'>" + h + ":" + m + "</span>";
                $res += "</p>";
                $res = $res + "<p>" + "Yep! It's so funny :)" + "</p>";
                $res += "</li>";
                setTimeout(function(){
                    $me.append($res);
                    $me.scrollTop(height+100); // add more 500px for #chat-box position             
                }, 1000);
            }
        }
    });
    // END FORM CHAT

    //BEGIN PORTLET
    jQuery(".portlet").each(function(index, element) {
        var me = jQuery(this);
        jQuery(">.portlet-header>.tools>i", me).click(function(e){
            if(jQuery(this).hasClass('fa-chevron-up')){
                jQuery(">.portlet-body", me).slideUp('fast');
                jQuery(this).removeClass('fa-chevron-up').addClass('fa-chevron-down');
            }
            else if(jQuery(this).hasClass('fa-chevron-down')){
                jQuery(">.portlet-body", me).slideDown('fast');
                jQuery(this).removeClass('fa-chevron-down').addClass('fa-chevron-up');
            }
            else if(jQuery(this).hasClass('fa-cog')){
                //Show modal
            }
            else if(jQuery(this).hasClass('fa-refresh')){
                //jQuery(">.portlet-body", me).hide();
                jQuery(">.portlet-body", me).addClass('wait');

                setTimeout(function(){
                    //jQuery(">.portlet-body>div", me).show();
                    jQuery(">.portlet-body", me).removeClass('wait');
                }, 1000);
            }
            else if(jQuery(this).hasClass('fa-times')){
                me.remove();
            }
        });
    });
    //END PORTLET

    //BEGIN BACK TO TOP
    jQuery(window).scroll(function(){
        if (jQuery(this).scrollTop() < 200) {
            jQuery('#totop') .fadeOut();
        } else {
            jQuery('#totop') .fadeIn();
        }
    });
    jQuery('#totop').on('click', function(){
        jQuery('html, body').animate({scrollTop:0}, 'fast');
        return false;
    });
    //END BACK TO TOP

    //BEGIN CHECKBOX TABLE
    jQuery('.checkall').on('ifChecked ifUnchecked', function(event) {
        if (event.type == 'ifChecked') {
            jQuery(this).closest('table').find('input[type=checkbox]').iCheck('check');
        } else {
            jQuery(this).closest('table').find('input[type=checkbox]').iCheck('uncheck');
        }
    });
    //END CHECKBOX TABLE

    //BEGIN JQUERY NEWS UPDATE
    jQuery('#news-update').ticker({
        controls: false,
        titleText: ''
    });
    //END JQUERY NEWS UPDATE

    jQuery('.option-demo').hover(function() {
        jQuery(this).append("<div class='demo-layout animated fadeInUp'><i class='fa fa-magic mrs'></i>Demo</div>");
    }, function() {
        jQuery('.demo-layout').remove();
    });
    jQuery('#header-topbar-page .demo-layout').on('click', function() {
        var HtmlOption = jQuery(this).parent().detach();
        jQuery('#header-topbar-option-demo').html(HtmlOption).addClass('animated flash').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            jQuery(this).removeClass('animated flash');
        });
        jQuery('#header-topbar-option-demo').find('.demo-layout').remove();
        return false;
    });
    jQuery('#title-breadcrumb-page .demo-layout').on('click', function() {
        var HtmlOption = jQuery(this).parent().html();
        jQuery('#title-breadcrumb-option-demo').html(HtmlOption).addClass('animated flash').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            jQuery(this).removeClass('animated flash');
        });
        jQuery('#title-breadcrumb-option-demo').find('.demo-layout').remove();
        return false;
    });
    // CALL FUNCTION RESPONSIVE TABS
    fakewaffle.responsiveTabs(['xs', 'sm']);

});



