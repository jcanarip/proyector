$(document).ready(function() {
    show_periodo_by_tipodashboard();
    show_info_tipodashboard()
});


function show_periodo_by_tipodashboard()
 {
    jQuery("body").on('change',"select#tipo",function(){
       var id = jQuery(this).val();
        $.ajax({
            method: "POST",
            url: URL+"dashboard/dashboard/showPeriodoByTipodashboard",
            data: {id: id}
        }).done(function(html) {
            jQuery("select#periodo").html(html);
        }).fail(function(xhr){
           Alerta(xhr.responseText);
        });
    });
 };
 
 function show_info_tipodashboard()
{
    jQuery("select#tipo").change(function(){
        var texto = jQuery(this).find("option:selected").attr("title");
        var value = jQuery(this).val();
        var clase = "note-info";
        
        if(value == 2)
        {
           clase = "note-warning"; 
        }else if(value == 3){
           clase = "note-danger"; 
        }
        jQuery("div#info-tipo-plantilla").removeClass("note-info").removeClass("note-warining").removeClass("note-danger");
        
        if(texto == "")
        {
           jQuery("div#info-tipo-plantilla").hide(); 
           jQuery("div#info-tipo-plantilla p").html(""); 
        }else{
           jQuery("div#info-tipo-plantilla").addClass(clase);;
           jQuery("div#info-tipo-plantilla").show();; 
           jQuery("div#info-tipo-plantilla p").html(texto); 
        }
        
     });
}

