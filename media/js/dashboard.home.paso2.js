jQuery(document).ready(function() {
    check_uncheck_items();
});

function check_uncheck_items()
{
    jQuery(".list-group li.list-group-item .items").click(function(){
        if (jQuery('.list-group li.list-group-item input[type=checkbox]:checked').length === 0) {
            jQuery(".btn-primary").hide();
        }else{
            jQuery(".btn-primary").show();
        }   
    });
}