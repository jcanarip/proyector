$(document).ready(function() {
    load_cuenta_by_social();
    load_items_by_cuenta();
    check_uncheck_items();
    show_info_tipodashboard();
    show_periodo_by_tipodashboard();
});

 function show_periodo_by_tipodashboard()
 {
    jQuery("body").on('change',"select#tipo",function(){
       var id = jQuery(this).val();
        $.ajax({
            method: "POST",
            url: URL+"dashboard/dashboard/showPeriodoByTipodashboard",
            data: {id: id}
        }).done(function(html) {
            jQuery("select#periodo").html(html);
        }).fail(function(xhr){
           Alerta(xhr.responseText);
        });
    });
 };

function show_info_tipodashboard()
{
    jQuery("select#tipo").change(function(){
        var texto = jQuery(this).find("option:selected").attr("title");
        var value = jQuery(this).val();
        var clase = "note-info";
        
        if(value == 2)
        {
           clase = "note-warning"; 
        }else if(value == 3){
           clase = "note-danger"; 
        }
        jQuery("div#info-tipo-plantilla").removeClass("note-info").removeClass("note-warining").removeClass("note-danger");
        
        if(texto == "")
        {
           jQuery("div#info-tipo-plantilla").hide(); 
           jQuery("div#info-tipo-plantilla p").html(""); 
        }else{
           jQuery("div#info-tipo-plantilla").addClass(clase);;
           jQuery("div#info-tipo-plantilla").show();; 
           jQuery("div#info-tipo-plantilla p").html(texto); 
        }
        
     });
}


function load_cuenta_by_social()
{
    jQuery("select#social").change(function(){
       var id = jQuery(this).val();
        $.ajax({
            method: "POST",
            url: URL+"dashboard/dashboard/getCuentasBySocial",
            data: {id: id}
        }).done(function( html ) {
            jQuery("select#cuenta").html(html);
        }).fail(function(xhr){
           Alerta(xhr.responseText);
        });
    });
}

function load_items_by_cuenta()
{
    jQuery("body").on('change',"select#cuenta",function(){
       var id = jQuery(this).val();
        $.ajax({
            method: "POST",
            url: URL+"dashboard/dashboard/getItemsByCuenta",
            datatype: "json",
            data: {id: id}
        }).done(function( data ) {
            jQuery("#itemsde .list-group").html("");
            var html = "";
            jQuery.each($.parseJSON(data), function( index, value ) {
                var checked = '';
                $( "#itemspara .list-group li input" ).each(function( index, element ) {
                    if(value.value == $( this ).val())
                    {
                       checked = 'checked'; 
                    }
                })
                html+= '<li class="list-group-item"><input type="checkbox" '+checked+' class="items" id="'+value.id+'" name="auxitem[]" value="'+value.value+'">&nbsp;&nbsp;<span>'+value.descripcion+'</span></li>';
             });
            jQuery("#itemsde .list-group").html(html);
        }).fail(function(xhr){
            jQuery("#itemsde .list-group").html('<li class="list-group-item">Ningùn Item cargado</li>');

           Alerta(xhr.responseText);
        });
    });
}

function check_uncheck_items()
{
    jQuery('body').on("click","#itemsde .list-group .items",function(){

        var check = jQuery(this);

        var id = check.attr('id');
        var value = check.val();
        var text = check.next("span").text();
        jQuery("#itemspara .list-group").css("padding","0px");
        if(check.is(':checked'))
        {
            jQuery("#itemspara .list-group").append('<li id="para-'+id+'" class="list-group-item"><input type="hidden" name="items[]" value="'+value+'" />'+text+'</li>');
        }else{
            jQuery("#itemspara .list-group #para-"+id).remove();
        }
        
        jQuery("#itemspara .list-group #noitempara").remove();
        
        if(jQuery("#itemspara ul.list-group").find("li").length == 0)
        {
          jQuery("#itemspara .list-group").html('<li class="list-group-item" id="noitempara">Ningùn Item seleccionado</li>');  
        }

    });
}