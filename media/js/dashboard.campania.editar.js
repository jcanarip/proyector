jQuery(document).ready(function() {
    jQuery('#datetimepickerinicio').datetimepicker({
        locale: 'es',
        minDate: "now",
        format: "DD-MM-YYYY HH:mm",
        useCurrent : true
    });
    
    jQuery('#datetimepickerfin').datetimepicker({
        locale: 'es',
        format: "DD-MM-YYYY HH:mm",
        useCurrent : true
    });
    
    jQuery("#datetimepickerinicio").on("dp.change", function (e) {
        jQuery('#datetimepickerfin').data("DateTimePicker").minDate(e.date);
    });
        
    jQuery("#datetimepickerfin").on("dp.change", function (e) {
        jQuery('#datetimepickerinicio').data("DateTimePicker").maxDate(e.date);
    });

    jQuery("#profile_id").change(function(){
        if(jQuery(this).val() == 1)
        {
            jQuery("#groupempresa").hide();
        }else{
            jQuery("#groupempresa").show();
        }
    });
    
    get_cuentas_by_social();
    get_item_by_cuenta();
    get_objeto_by_indicador();
    
});

 function get_objeto_by_indicador()
 {

    jQuery("body").on('change',"select#indicador",function(){
        
       var indicador = jQuery(this).val();
       var tipo = jQuery(this).find('option:selected').data("tipo");
       var item_id = jQuery("#item_id").val();
       var social_id = jQuery("#social_id").val();
       var autorizacion_id = jQuery("#autorizacion_id").val();

       if(tipo == 1)
       {
           $(".objeto").show();
            $.ajax({
                method: "POST",
                url: URL+"dashboard/campania/getobjetobyindicador",
                data: {indicador : indicador, item_id: item_id, social_id : social_id, autorizacion_id : autorizacion_id}
            }).done(function(html) {
                jQuery("select#objeto").html(html);
            }).fail(function(xhr){
               Alerta(xhr.responseText);
            });
            
       }else if(tipo == 2){
           $(".objeto").hide();
       }else{
           $(".objeto").show();
           var html = '<option value=""  >-- Elige una opcion --</option>';
           jQuery("select#objeto").html(html);  
       }
        

    }); 
 }

 function get_cuentas_by_social()
 {
     var html = '<option value=""  >-- Elige una opcion --</option>';
    jQuery("body").on('change',"select#social_id",function(){
       jQuery("select#autorizacion_id , select#item_id , select#indicador, select#objeto").html(html);  
       var id = jQuery(this).val();
       
       if(id)
       {
            $.ajax({
                method: "POST",
                url: URL+"dashboard/campania/getcuentasbysocial",
                data: {id: id}
            }).done(function(html) {
                jQuery("select#autorizacion_id").html(html);
                 $.ajax({
                    method: "POST",
                    url: URL+"dashboard/campania/getindicadoresbysocial",
                    data: {id: id}
                }).done(function(html) {
                    jQuery("select#indicador").html(html);
                }).fail(function(xhr){
                   Alerta(xhr.responseText);
                });
            }).fail(function(xhr){
               Alerta(xhr.responseText);
            });
       }

    });
 };
 
 function get_item_by_cuenta()
 {
    var html = '<option value=""  >-- Elige una opcion --</option>';
    jQuery("body").on('change',"select#autorizacion_id",function(){
       jQuery("select#objeto").html(html);  
       var id = jQuery(this).val();
       
       if(id)
       {
        $.ajax({
            method: "POST",
            url: URL+"dashboard/campania/getitembycuenta",
            data: {id: id}
        }).done(function(html) {
            jQuery("select#item_id").html(html);
        }).fail(function(xhr){
           Alerta(xhr.responseText);
        });
       }

    });
 };

