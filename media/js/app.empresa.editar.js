jQuery(document).ready(function() {
    jQuery('#datetimepickerbirthday').datetimepicker({
        locale: 'es',
        maxDate: "now",
        format: "DD-MM-YYYY",
        useCurrent : true
    });
    
    jQuery('#empresa').multiselect({
        enableFiltering: true,
        filterBehavior: 'value'
    });
    
    jQuery("#profile_id").change(function(){
        if(jQuery(this).val() == 1)
        {
            jQuery("#groupempresa").hide();
        }else{
            jQuery("#groupempresa").show();
        }
    });
});