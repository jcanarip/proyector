jQuery(document).ready(function() {
    var item; 
    load_modal();
    show_plantilla_by_item();
    removewidget();
    save_dashboard()
    load_dashboard();

    jQuery(window).resize(function(){
      load_dashboard();
    });
});

jQuery(function () {

    //jQuery('#pagechart').gridstack();

    /*jQuery("#pagechart").gridster({
        widget_selector: "div"
    });*/
    
    jQuery('#datetimepickerfechainicio').datetimepicker({
        locale: 'es',
        maxDate: "now",
        format: "DD-MM-YYYY",
        useCurrent : true
    });

    if($('#datetimepickerfechafin').length > 0)
    {
        jQuery('#datetimepickerfechafin').datetimepicker({
            locale: 'es',
            maxDate: "now",
            format: "DD-MM-YYYY",
            useCurrent : true
        });
        
        var diasdiferencia = 0;
        
        jQuery("#datetimepickerfechainicio").on("dp.change", function (e) {
           jQuery('#datetimepickerfechafin').data("DateTimePicker").minDate(e.date);
            diasdiferencia = calcDiff();

            if(jQuery("#fechainicioopcional").val())
            {
                 var tiempo = Date.parse(jQuery('#datetimepickerfechainicio').data("DateTimePicker").date());
                jQuery('#datetimepickerfechainicioopcional').data("DateTimePicker").maxDate(new Date(tiempo-(diasdiferencia+1)*1000*60*60*24));   
                var date = new Date(($('#datetimepickerfechainicioopcional').data("DateTimePicker").date() + (diasdiferencia*1000*60*60*24)));
                jQuery("#fechafinopcional").val(('0' + date.getDate()).slice(-2) + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getFullYear() );
            }
        });
        
        jQuery("#datetimepickerfechafin").on("dp.change", function (e) {
            jQuery('#datetimepickerfechainicio').data("DateTimePicker").maxDate(e.date);
            
            diasdiferencia = calcDiff();
            var tiempo = Date.parse(jQuery('#datetimepickerfechainicio').data("DateTimePicker").date());
            jQuery('#datetimepickerfechainicioopcional').data("DateTimePicker").maxDate(new Date(tiempo-(diasdiferencia+1)*1000*60*60*24));   
            
            if(jQuery("#fechainicioopcional").val())
            {
                var date = new Date(($('#datetimepickerfechainicioopcional').data("DateTimePicker").date() + (diasdiferencia*1000*60*60*24)));
                jQuery("#fechafinopcional").val(('0' + date.getDate()).slice(-2) + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getFullYear() );
            }
        });

        jQuery('#datetimepickerfechainicioopcional').datetimepicker({
            locale: 'es',
            maxDate: "now",
            format: "DD-MM-YYYY",
            useCurrent : true
        });

        /*jQuery('#datetimepickerfechafinopcional').datetimepicker({
            locale: 'es',
            maxDate: "now",
            format: "DD-MM-YYYY",
            useCurrent : true
        });*/

        jQuery("#datetimepickerfechainicioopcional").on("dp.change", function (e) 
        {
            if(e.date)
            {
                var date = new Date((Date.parse(e.date) + (diasdiferencia*1000*60*60*24)));
                jQuery("#fechafinopcional").val(('0' + date.getDate()).slice(-2) + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getFullYear() );
            }else{
                jQuery("#fechafinopcional").val("");
            }
        });
        /*jQuery("#datetimepickerfechafinopcional").on("dp.change", function (e) {
                jQuery('#datetimepickerfechainicioopcional').data("DateTimePicker").maxDate(e.date);
        });*/
    }
});


function  load_dashboard()
{   
    $( ".widget .chartDiv" ).each(function( index, element ) {

        if($(this).data("datatable"))
        {
            google.load('visualization', '1', {'packages':['corechart']});
            google.setOnLoadCallback(drawChart(JSON.stringify($(this).data("datatable")),$(this)[0]));
        }
    })
}

function save_dashboard()
{
     jQuery("#guardardashboard").click(function(){
        jQuery.ajax({
            method: "POST",
            dataType: "json",
            url: URL+"dashboard/dashboard/savedashboard",
            data: jQuery("#frmview").serialize(),
        }).done(function( data ) {
            if(data.success == true)
            {
                Alerta(data.message);
            }
        }).fail(function(xhr){
           Alerta(xhr.responseText);
        });
    })
    
}

function removewidget()
{
    jQuery("body").on('click',"#pagechart .remove-widget",function(){
        var indice = jQuery("#pagechart .remove-widget").index(this);
        jQuery("#pagechart .widget").eq(indice).remove();
    })
}

function calcDiff(){
    var a=$('#datetimepickerfechainicio').data("DateTimePicker").date();
    var b=$('#datetimepickerfechafin').data("DateTimePicker").date();
    var timeDiff=0
     if (b) {
            timeDiff = (b - a) / 1000;
        }
    return (Math.floor(timeDiff/(60*60*24)));
}

function sortByKey(array, key) 
{
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

function drawChart(jsonData,div) 
{
      // Create our data table out of JSON data loaded from server.
    var data = new google.visualization.DataTable(jsonData);

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.AreaChart(div);
    chart.draw(data, {
        width: '100%',
        height: '150px',
        chartArea: {
          left: "0",
          top: "0",
          height: "100%",
          width: "100%"
        },
        legend : { position : 'none' },
        vAxis:{
          baselineColor: '#fff',
          textPosition: 'none'
        },
        hAxis:{
          baselineColor: '#fff',
          textPosition: 'none'
        } 
    });
 }

function call(pl,fc)
{
    jQuery.ajax({
         method: "POST",
         dataType: "json",
         url: URL+"dashboard/dashboard/showframe",
         data: jQuery("#frmview").serialize()+"&id="+item+"&pl="+pl+"&fc="+fc,
    }).done(function( data ) {
        jQuery("#pagechart").append(data.html);
        
        if(data.success == true)
        {
            var rowboxContent = jQuery(".boxContent .row .gif");
            var htmlprogressbar = '<div class="progress"><div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">0%</div></div>';

            rowboxContent.eq(rowboxContent.length - 1).html(htmlprogressbar);
            
            jQuery.ajax({
                method: "POST",
                dataType: "json",
                url: URL+"dashboard/dashboard/getReportByItem",
                data: jQuery("#frmview").serialize()+"&id="+item+"&pl="+pl+"&fc="+fc,
                xhr: function () 
                {
                    var progressbar = jQuery(".progress-bar");
                    var progress = progressbar.eq(progressbar.length - 1);
                    var xhr = $.ajaxSettings.xhr();
                    
                    /*xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = (evt.loaded / evt.total) * 100;
                            //console.log(percentComplete);
                            $(progress).attr("aria-valuenow",percentComplete);
                            
                            $(progress).css({
                                width: percentComplete + '%'
                            });
                            $(progress).text(percentComplete + '%')
                            if (percentComplete === 1) {
                                $(progress).addClass('hide');
                            }
                        }
                    }, false);*/
                    
                    xhr.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = (evt.loaded / evt.total) * 100;
                            $(progress).attr("aria-valuenow",percentComplete);
                            $(progress).text(percentComplete + '%')
                            $(progress).css({
                                width: percentComplete + '%'
                            });
                        }
                    }, false);
                    
                    return xhr;
                },
            }).done(function( data ) {
                var contenedor = jQuery(".data-"+pl+"-"+fc);
                contenedor.eq(contenedor.length - 1).replaceWith(data.data);
                
                if(data.tipo == 1 || data.tipo == 3)
                {
                    var chartdiv = jQuery(".data-"+pl+"-"+fc+" .chartDiv");
                    
                    if(chartdiv.eq(chartdiv.length - 1).data("datatable"))
                    {
                        google.load('visualization', '1', {'packages':['corechart']});
                        // Set a callback to run when the Google Visualization API is loaded.
                        google.setOnLoadCallback(drawChart(JSON.stringify(chartdiv.eq(chartdiv.length - 1).data("datatable")),chartdiv.eq(chartdiv.length - 1)[0]));
                    }
                }
            }).fail(function(xhr){
                var indice =  jQuery("#pagechart .widget").length - 1;
                jQuery("#pagechart .widget").eq(indice).fadeOut( "slow" ).remove();
                Alerta(xhr.responseText);
            });
        }
    }).fail(function(xhr){
        var indice =  jQuery("#pagechart .widget").length - 1;
        jQuery("#pagechart .widget").eq(indice).fadeOut( "slow" ).remove();
        Alerta(xhr.responseText);
    });

    
}

function show_plantilla_by_item()
{
    jQuery("select#fechainicio").change(function(){
        jQuery("#pagechart").html("");

    }); 
    
    jQuery("#item").change(function(){

        var id = jQuery(this).val().split("-");
        item = id[0];
        var plantillaid = id[1];

        jQuery(".seccionplantilla").hide();
        jQuery("#socialplantilla-"+plantillaid).show();
    });  
}


function load_modal()
{
    jQuery("#plantilla").click(function(){
       jQuery('#item option[value=""]').attr('selected','selected');
       jQuery(".seccionplantilla").hide(); 
       jQuery('#myModal').modal(); 
    });
}