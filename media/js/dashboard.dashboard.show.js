jQuery(document).ready(function() {

    load_dashboard();
    
    jQuery(window).resize(function(){
      load_dashboard();
    });
    
    load_pdf();
    load_excel();
    send_email();
});

function send_email()
{
    jQuery('body').on("click","#sendemail",function(){
        jQuery.ajax({
            method: "POST",
            dataType: "json",
            url: URL+"dashboard/dashboard/sendemail",
            data: jQuery("#frmemail").serialize(),
        }).done(function( data ) {
            if(data.success == true)
            {
                jQuery("#modal-config").modal('hide');
                jQuery("#modal-config #email").val("");
               Alerta(data.message);
            }
        }).fail(function(xhr){
           jQuery("#modal-config").modal('hide');
           Alerta(xhr.responseText);
        });
    });
}

function  load_dashboard()
{   
    jQuery( ".widget .chartDiv" ).each(function( index, element ) {
        if($(this).data("datatable"))
        {
            google.load('visualization', '1', {'packages':['corechart']});
            google.setOnLoadCallback(drawChart(JSON.stringify($(this).data("datatable")),$(this)[0]));
        }
    })
}

function load_pdf()
{
    jQuery('body').on("click","#btnPrint",function(){
        jQuery("#btnPrint").attr("id","");
        var t =  jQuery("#t").val();
        jQuery.ajax({
            method: "POST",
            dataType: "json",
            url: URL+"dashboard/dashboard/getpdf",
            data: {t : t},
        }).done(function( data ) {
           jQuery(".btnPrint").attr("href",data.ruta).attr('target','_blank');;
           window.open(data.ruta);
        }).fail(function(xhr){
           Alerta(xhr.responseText);
        });
    })
}

function load_excel()
{
   jQuery('body').on("click","#btnExcel",function(){
        jQuery("#btnExcel").attr("id","");
        var t =  jQuery("#t").val();
        jQuery.ajax({
            method: "POST",
            dataType: "json",
            url: URL+"dashboard/dashboard/getexcel",
            data: {t : t},
        }).done(function( data ) {
           jQuery(".btnExcel").attr("href",data.ruta).attr('target','_blank');;
           window.open(data.ruta);
        }).fail(function(xhr){
           Alerta(xhr.responseText);
        });
    })
}

function drawChart(jsonData,div) 
{
      // Create our data table out of JSON data loaded from server.
    var data = new google.visualization.DataTable(jsonData);

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.AreaChart(div);
    chart.draw(data, {
        width: '100%',
        height: '150px',
        chartArea: {
          left: "0",
          top: "0",
          height: "100%",
          width: "100%"
        },
        legend : { position : 'none' },
        vAxis:{
          baselineColor: '#fff',
          textPosition: 'none'
        },
        hAxis:{
          baselineColor: '#fff',
          textPosition: 'none'
        } 
    });
 }