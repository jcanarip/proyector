<nav id="sidebar" role="navigation" data-step="2" data-intro="Template has &lt;b&gt;many navigation styles&lt;/b&gt;"
    data-position="right" class="navbar-default navbar-static-side">
    <div class="sidebar-collapse menu-scroll">
        <ul id="side-menu" class="nav">

            <div class="clearfix"></div>
            <?php
            if($oUser->profile_id == 1)
            {
            ?>
             <li>
                <a href="<?php echo url::base(true)?>app">
                    <i class="fa fa-user fa-fw">
                        <div class="icon-bg bg-orange"></div>
                    </i>
                    <span class="menu-title">Ir a principal</span>
                </a>
            </li>
            <?php
            }
            ?>
            <li data-step="1" data-intro="Crear, edita, previsualiza y exporta los dahsboards que usaran las redes sociales que autorizaste." 
                <?php echo ($seleccion == 'dashboard/index')? 'class="active"' :'';?>>
                <a href="<?php echo url::base(true)?>dashboard">
                    <i class="fa fa-user fa-fw">
                        <div class="icon-bg bg-orange"></div>
                    </i>
                    <span class="menu-title">Gestión de Dashboards</span>
                </a>
            </li>
            <li data-step="2" data-intro="Autoriza las redes sociales que se usaran en los reportes." 
                <?php echo ($seleccion == 'home/editarautorizacion')? 'class="active"' :'';?>>
                <a href="<?php echo url::base(true)?>dashboard/home/editarautorizacion">
                    <i class="fa fa-user fa-fw">
                        <div class="icon-bg bg-orange"></div>
                    </i>
                    <span class="menu-title">Gestión de redes<!--Permisos y Autorizaciones--></span>
                </a>
            </li>
            <li data-step="3" data-intro="Creacion de campañas" 
                <?php echo ($seleccion == 'campania/index')? 'class="active"' :'';?>>
                <a href="<?php echo url::base(true)?>dashboard/campania">
                    <i class="fa fa-user fa-fw">
                        <div class="icon-bg bg-orange"></div>
                    </i>
                    <span class="menu-title">Gestión de campañas<!--Permisos y Autorizaciones--></span>
                </a>
            </li>
        </ul>
    </div>
</nav>