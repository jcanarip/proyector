<div id="header-topbar-option-demo" class="page-header-topbar">
    <nav id="topbar" role="navigation" style="margin-bottom: 0;" data-step="3" class="navbar navbar-default navbar-static-top">
        <?php if ($oUser): ?>
        <?php
            if ($oUser->profile_id != 3):?>
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target=".sidebar-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                <a id="logo" href="<?php echo url::base(true)?>app" class="navbar-brand">
                    <span class="fa fa-rocket"></span>
                    <span class="logo-text" style="display: none">Xperia</span>
                    <span  class="logo-text-icon"><img src="<?php echo url::base(true)?>media/images/gallery/logo.png" alt="xperia" class="img-responsive"/>
                    </span>
                </a>
            </div>
        <?php endif ?>
        <div class="topbar-main">
           <?php
            if ($oUser->profile_id != 3):?>
            <a id="menu-toggle" href="#" class="hidden-xs"><i class="fa fa-bars"></i></a>            
            <?php endif ?>

            <ul class="nav navbar navbar-top-links navbar-right mbn"   >
                <!--<li class="dropdown"><a data-hover="dropdown" href="#" class="dropdown-toggle"><i class="fa fa-bell fa-fw"></i><span class="badge badge-green">3</span></a>                    </li>
                <li class="dropdown"><a data-hover="dropdown" href="#" class="dropdown-toggle"><i class="fa fa-envelope fa-fw"></i><span class="badge badge-orange">7</span></a>                    </li>
                <li class="dropdown"><a data-hover="dropdown" href="#" class="dropdown-toggle"><i class="fa fa-tasks fa-fw"></i><span class="badge badge-yellow">8</span></a>                    </li>-->
                <li class="dropdown topbar-user"><a data-hover="dropdown" data-step="4" data-intro="Coloca el mouse aqui, y se desplegaran màs opciones." href="#" class="dropdown-toggle"><img src="<?php echo url::base(true)?>media/images/avatar/48.jpg" alt="" class="img-responsive img-circle"/>&nbsp;<span class="hidden-xs">Hola <?php echo $oUser->first_name.' '.$oUser->last_name ?></span>&nbsp;<span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-user pull-right">
                        <li><a class="tienda" href="<?php echo url::base(true)?>app/profile "> Editar Informaciòn</a></li>
                        <li><a class="tienda" href="<?php echo url::base(true)?>app/user/changepass "> Cambiar Password</a></li>
                        <li class="divider"></li>
                        <li><a class="tienda" href="<?php echo url::base(true)?>auth/logout "> Salir</a></li>
                    </ul>
                </li>
                <!--<li id="topbar-chat" class="hidden-xs"><a href="javascript:void(0)" data-step="4" data-intro="&lt;b&gt;Form chat&lt;/b&gt; keep you connecting with other coworker" data-position="left" class="btn-chat"><i class="fa fa-comments"></i><span class="badge badge-info">3</span></a></li>-->
            </ul>
        </div>
        <?php endif ?>
    </nav>
    <!--BEGIN MODAL CONFIG PORTLET-->

    <!--END MODAL CONFIG PORTLET-->
</div>
