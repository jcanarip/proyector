<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            Dashboard <?php echo ($oReporte->descripcion)?>
        </div>
    </div>
    <!--<ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">Tables</li>
    </ol>-->
    <div class="clearfix">
    </div>
</div>
<div class="page-content">
    <div id="tab-general">
        <div id="generalTabContent" class="tab-content">
            <div class="form-group">
                <form action="" method="post" id="frmview">
                    <button type="button" data-step="2" data-intro="Haz click aqui para mostrar la lista de widgets de tus redes agregadas al dashboard." id="plantilla"  class="btn btn-primary" >Plantillas</button>
                    <button type="button" data-step="3" data-intro="Una vez agregado tus widgets, puedes guardar este dashboard haciendo click aqui." id="guardardashboard"  class="btn btn-primary" >Guardar Cambios</button>
                    <input type="hidden" name="dashboard" id="dashboard" value="<?php echo ($oReporte->id)?>" />
                    <input type="hidden" name="periodo" id="periodo" value="<?php echo ($oReporte->periodo)?>" />
                    <input type="hidden" name="tipo_dhasboard" id="tipo_dhasboard" value="<?php echo ($oReporte->tipo)?>" />
                    <?php
                    
                    $fechainicio = null;
                    if(isset($oReporte->datetime) and !empty($oReporte->datetime))
                    {
                        $json = json_decode($oReporte->datetime, true);
                        $fechainicio = $json["fechainicio"];
                    }
                    
                    
                    if($oReporte->periodo == 1):
                        echo View::factory("Dashboard/Dashboard/diario");
                    elseif($oReporte->periodo == 2):
                        echo View::factory("Dashboard/Dashboard/restotiempo")->set(compact("aTiempo","fechainicio"))->set("descripcion","Semanal");
                    elseif($oReporte->periodo == 3):
                        echo View::factory("Dashboard/Dashboard/restotiempo")->set(compact("aTiempo","fechainicio"))->set("descripcion","Mensual");
                    elseif($oReporte->periodo == 4): 
                        echo View::factory("Dashboard/Dashboard/restotiempo")->set(compact("aTiempo","fechainicio"))->set("descripcion","Trimestral");
                    elseif($oReporte->periodo == 5):
                        echo View::factory("Dashboard/Dashboard/restotiempo")->set(compact("aTiempo","fechainicio"))->set("descripcion","Anual");        
                    elseif($oReporte->periodo == 10):
                        echo View::factory("Dashboard/Dashboard/porfecha");
                    endif;
                    ?>
                    <div class="row" id="pagechart">
                    <?php
                    if(isset($oReporte->datetime) and !empty($oReporte->datetime))
                    {
                        $json = json_decode($oReporte->datetime, true);
                        
                        if(count($json["data"]) >0)
                        {
                            foreach ($json["data"] as $value) 
                            {

                                echo View::factory("Dashboard/Dashboard/tipodashboard")
                                        ->set("tipo", $value["tipo"])
                                        ->set("descripcion", $value["descripcion"])
                                        ->set("dashboard", $json["tipo_dhasboard"])
                                        ->set("pl", $value["pl"])
                                        ->set("fc", $value["fc"])
                                        ->set("data", $value)
                                        ->set("estado", true);
                            }
                        }
                    }
                    ?>    
                    </div>
                    <!--<button type="button" id="buscar"  class="btn btn-primary">Buscar</button>-->
                </form>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Plantillas</h4>
                        </div>
                        <div class="modal-body">
                        <div class="form-group">
                            <label for="item">Fuente</label>
                            <div class="input-icon right">
                                <select name="item" id="item" class="form-control">
                                    <?php
                                    if(count($aItem) > 0):
                                    ?>
                                    <option value=""  >-- Elige uno de los items --</option>
                                    <?php
                                    foreach ($aItem as $value):
                                    ?>
                                    <option value="<?php echo $value->id."-".$value->oAutorizacion->oSocial->id?>"  >(<?php echo $value->oAutorizacion->oSocial->descripcion ?>) <?php echo $value->descripcion?></option>
                                    <?php
                                    endforeach;
                                    else:
                                    ?>
                                    <option value=""  >-- No tiene items para este dashboard --</option>
                                    <?php
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                            
                        <div>
                        <?php

                        foreach ($aPlantillas as  $value):
                        ?>
                            <div class="seccionplantilla" id="socialplantilla-<?php echo $value["id"];?>" >
                        <?php
                            foreach ($value["detalle"]  as $indice => $valor):
                        ?>
                            <div>
                                <h2><?php echo $valor["name"]?></h2>
                                <?php
                                $lista = json_decode($valor["detalle"], true);
                                ?>
                                <ul>
                                <?php
                                foreach ($lista as $key => $plantilla):
                                ?>
                                    <li><a href="javascript:call('<?php echo $indice?>','<?php echo $key ?>')"><?php echo $plantilla["descripcion"]?></a></li>
                                <?php
                                endforeach;
                                ?>
                                </ul>
                            </div>
                        <?php
                            endforeach;
                        ?>
                            </div>
                        <?php
                        endforeach;
                        ?>   
                        </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                  </div>
                </div>
            </div>    
        </div>
    </div>
</div>
