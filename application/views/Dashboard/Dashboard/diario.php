<div class="form-group" data-step="1" data-intro="Seleccion la fecha con la que deseas crear tu dashboard.">
    <label for="fechainicio">Fecha</label>
     <div class='input-group date' id='datetimepickerfechainicio'>
         <input type='text'  class="form-control" name="fechainicio" id="fechainicio" placeholder="dd-mm-aaaa" value="" />
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
</div>

