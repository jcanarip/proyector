
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            <?php echo ($oReporte->id)? 'Editar':'Nuevo';?>
        </div>
    </div>
    <!--<ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">Tables</li>
    </ol>-->
    <div class="clearfix">
    </div>
</div>
<div class="page-content">
    <div id="tab-general">
        <div id="generalTabContent" class="tab-content">
        <form action="" method="post">
            <div class="form-group">
                <label for="empresa_id">Marca</label>
                <div class="input-icon right">
                    <select name="empresa_id" id="empresa_id" class="form-control">
                        <option value=""  >-- Elige una Marca --</option>
                        <?php
                        foreach ($aListadEmpresas as $value):
                        ?>
                        <option value="<?php echo $value->id?>" <?php echo ($oReporte->empresa_id == $value->id)? "selected" : ""?>  ><?php echo $value->name ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="social">Fuente</label>
                <div class="input-icon right">
                    <select name="social" id="social" class="form-control">
                        <option value=""  >-- Elige una fuente --</option>
                        <?php
                        foreach ($aSocial as $value):
                        ?>
                        <option value="<?php echo $value->id?>"  ><?php echo $value->descripcion ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
             <div class="form-group">
                <label for="cuenta">Cuenta</label>
                <div class="input-icon right">
                    <select name="cuenta" id="cuenta" class="form-control">
                        <?php
                        if(count($aSocial) > 1):
                        ?>
                        <option value=""  >-- No hay cuenta seleccionada --</option>
                        <?php
                        else:
                            if(count($aCuentas) > 0):
                                foreach ($aCuentas as $value):
                                ?>
                                <option value=""  >-- Elige una cuenta --</option>
                                <option value="<?php echo $value->id?>"  ><?php echo $value->username ?></option>
                                <?php
                                endforeach;
                            else:
                            ?>
                                <option value=""  >-- No hay cuenta seleccionada --</option>
                            <?php
                            endif;
                        endif;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="item">Items</label>
                <div class="row">
                    <div class="col-lg-6" id='itemsde'>
                        <h4>&nbsp;</h4>
                        <ul class="list-group">
                           <li class="list-group-item">Sin items</li>
                        </ul>
                    </div>
                    <div class="col-lg-6" id='itemspara'>
                        <h4>Items eleccionados</h4>
                        <ul class="list-group" style="border: 1px dashed; padding: 18px;">
                            <?php
                                foreach ($oReporte->aItem->find_all() as $value):
                            ?>
                            <li id="para-<?php echo "item-".strtolower($value->oAutorizacion->oSocial->name)."-".$value->id?>" class="list-group-item"><input type="hidden" name="items[]" value="<?php echo $value->id?>" /><?php echo $value->descripcion?></li>
                            <?php 
                                endforeach;
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
           
            <div class="form-group">
                <label for="descripcion">Titulo del dashboard</label>
                <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="" value="<?php echo $oReporte->descripcion ?>">
            </div>
            <div class="form-group">
                <label for="tipo">Tipo del Dashboard</label>
                <select name="tipo" id="tipo" class="form-control">
                    <?php
                    if(count($aTipodashboard) > 0):
                    ?>
                    <option value="" title="">-- Elige un tipo de dashboard --</option>
                    <?php
                    foreach ($aTipodashboard as $value):
                    ?>
                    <option value="<?php echo $value->id?>" title="<?php echo $value->texto?>"  <?php echo ($oReporte->tipo == $value->id)? "selected" : ""?>><?php echo $value->name?></option>
                    <?php 
                    endforeach;
                    else:
                    ?>
                    <option value="" title="">-- No existe ningùn tipo de dashboard --</option>
                    <?php 
                    endif;
                    ?>
                </select>
                <div class="note note-info" id="info-tipo-plantilla" style="display:none">
                    <h4 class="box-heading">Informaciòn importante:</h4>
                    <p></p>
                </div>
            </div>
           <!-- <div class="form-group">
                <label for="plantilla">Plantilla</label>
                <select name="plantilla" id="plantilla" class="form-control">
                    <option value="0" <?php echo ($oReporte->plantilla == '0')? "selected" : ""?>>Vacìa</option>
                </select>
            </div>-->
            <div class="form-group">
                <label for="periodo">Periodo</label>
                <select name="periodo" id="periodo" class="form-control">
                     <?php
                    if(count($aPeriodo) > 0):
                    ?>
                    <!--<option value="" title="">-- Elige un tipo de dashboard --</option>-->
                    <?php
                    foreach ($aPeriodo as $value):
                    ?>
                    <option value="<?php echo $value["id"]?>" title="<?php //echo $value->texto?>"  <?php echo ($oReporte->periodo == $value["id"])? "selected" : ""?>><?php echo $value["descripcion"]; ?></option>
                    <?php 
                    endforeach;
                    else:
                    ?>
                    <option value="" title="">-- No existe ningùn tipo de dashboard --</option>
                    <?php 
                    endif;
                    ?>
                </select>
            </div>
           
          <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
        </div>
    </div>
</div>