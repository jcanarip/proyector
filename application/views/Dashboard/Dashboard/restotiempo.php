<div class="form-group" data-step="1" data-intro="Seleccion el periodo con el que deseas crear tu dashboard.">
    <label for="fechainicio">Periodo <?php echo $descripcion;?></label>
    <div class="input-icon right">
        <select name="fechainicio" id="fechainicio" class="form-control">
            <option value=""  >-- Elige una periodo --</option>
            <?php
            foreach ($aTiempo as $key => $value):
            ?>
            <option value="<?php echo $key ?>" <?php echo ($key == $fechainicio)? "selected":""; ?>  ><?php echo Util::nuevo_formato($value[0])." - ".Util::nuevo_formato($value[1]) ?></option>
            <?php
            endforeach;
            ?>
        </select>
    </div>
</div>