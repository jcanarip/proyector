<?php
if($dashboard == 1):
    if($tipo == 1 or $tipo == 3):
        $datatable= "";
        $totalultimo = "";
        if(isset($data))
        {
            $json = '{"cols": [{"id":"","label":"Topping","pattern":"","type":"string"},{"id":"","label":"Slices","pattern":"","type":"number"}],"rows":'.json_encode($data["chart"]).'}';
            $datatable='data-datatable="'.htmlentities($json).'"';
            $totalultimo = $data["totalultimo"];
        } 
?>
    <div class="col-sm-12 col-md-6 col-lg-3 widget data-<?php echo $pl."-".$fc; ?>" >
        
        <div class="box">
            <div class="wrapRolling">
                <div class="boxHeader"> 
                    <h2 class="widgetTitle"><?php echo $descripcion;?></h2>
                </div>
                <div class="boxContent">
                    <div class="barNb"><?php echo $totalultimo;?></div>
                     <div class="barTrend">
                        <?php 
                        if(isset($data)):
                            if(count($data)):
                                if($estado == true):
                            ?>
                                <input type="hidden" class="date-table" name="date-table[]" value="<?php echo htmlentities(json_encode($data))?>">
                            <?php
                                endif;
                            ?>
                        <div class="chartDiv" style="width: 100%; height: 120px;" <?php echo $datatable?>></div>
                         <?php
                            endif;
                         else:
                         ?>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 gif" style="text-align: center">
                                <img src="<?php echo URL::base(true)?>media/images/loader.gif" />
                            </div>
                        </div>              
                         <?php
                         endif;
                         ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if($estado == true):
        ?>
        <a href="javascript:void(0)" class="btn btn-xs btn-danger remove-widget"><span class="glyphicon glyphicon-remove "></span> </a>
        <?php
        endif;
        ?>
    </div>
<?php
    else:
?>
    <div class="col-sm-12 col-md-12 col-lg-6 widget data-<?php echo $pl."-".$fc; ?>"  >
        <div class="box">
            <div class="wrapRolling">
                <div class="boxHeader"> 
                     <h2 class="widgetTitle"><?php echo $descripcion;?></h2>
                </div>
                <div class="boxContent">
                    <div >
                        <?php 
                        if(isset($data)):
                            if(count($data)):
                                if($estado == true):
                            ?>
                                <input type="hidden" class="date-table" name="date-table[]" value="<?php echo htmlentities(json_encode($data))?>">
                            <?php
                                endif;
                            ?>
                        <table  class="tabletops table table-hover table-bordered" >
                            <tbody>        
                                <?php
                                for($i = 0; $i < count($data["table"]); $i++):
                                ?>
                                <tr>
                                    <td class="legend">
                                        <div  style="width: 10px; height: 10px; display: inline-block;" class="color<?php echo $i + 1;?>"></div>
                                    </td>
                                    <th class="post" title="<?php echo $data["table"][$i]["texto"];?>">
                                        <?php echo $data["table"][$i]["texto"];?>
                                    </th>
                                    <td class="numbers">
                                        <?php echo $data["table"][$i]["cantidad"];?>
                                    </td>
                                </tr>
                                <?php 
                                endfor;
                                ?>
                         </tbody>
                        </table>            
                        <?php
                            endif;
                        else:
                        ?>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 gif" style="text-align: center">
                                <img src="<?php echo URL::base(true)?>media/images/loader.gif" />
                            </div>
                        </div>  
                        <?php
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if($estado == true):
        ?>
        <a href="javascript:void(0)" class="btn btn-xs btn-danger remove-widget"><span class="glyphicon glyphicon-remove "></span> </a>
        <?php
        endif;
        ?>
    </div>
<?php
    endif;
elseif($dashboard == 2):
    if($tipo == 1 or $tipo == 3):
        $datatable= "";

        if(isset($data))
        {
            $json = '{"cols": [{"id":"","label":"Topping","pattern":"","type":"string"},{"id":"","label":"Slices","pattern":"","type":"number"}],"rows":'.json_encode($data["chart"]).'}';
            $datatable='data-datatable="'.htmlentities($json).'"';
        } 
?>
    <div class="col-sm-12 col-md-6 col-lg-3 widget data-<?php echo $pl."-".$fc; ?>"   >
        <div class="box">
            <div class="wrapRolling">
                <div class="boxHeader"> 
                     <h2 class="widgetTitle"><?php echo $descripcion;?></h2>
                </div>
                <div class="boxContent">
                <?php
                if(isset($data)):
                   if(count($data)):
                        if($estado == true):
                    ?>
                        <input type="hidden" class="date-table" name="date-table[]" value="<?php echo htmlentities(json_encode($data))?>">
                    <?php
                        endif;
                    ?>
                    <table class="table3">
                        <tbody>
                            <tr>
                                <td class="wrapSparkine" colspan="3"><div class="chartDiv" style="width: 100%; height: 120px;"<?php echo $datatable?>></div></td>
                            </tr>
                            <tr>
                                <td class="wrapValuedia"><?php echo $data["dias"]["dia1"] ?> dia(s)</td>
                                <td class="wrapValuedia"><?php echo $data["dias"]["dia2"]?> dia(s)</td>
                                <td class="wrapValuedia"><?php echo $data["dias"]["dia3"]?> dia(s)</td>
                            </tr>
                            <tr>
                                <td class="wrapValue"><?php echo $data["periodos"]["columna1"]?></td>
                                <td class="wrapValue"><?php echo $data["periodos"]["columna2"]?></td>
                                <td class="wrapValue"><?php echo $data["periodos"]["columna3"]?></td>
                            </tr>
                        </tbody>
                    </table>
                <?php
                    endif;
                else:
                ?>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 gif" style="text-align: center">
                            <img src="<?php echo URL::base(true)?>media/images/loader.gif" />
                        </div>
                    </div>  
                <?php
                endif;
                ?>  
                </div>
            </div>
        </div>
        <?php
        if($estado == true):
        ?>
        <a href="javascript:void(0)" class="btn btn-xs btn-danger remove-widget"><span class="glyphicon glyphicon-remove "></span> </a>
        <?php
        endif;
        ?>
    </div>
<?php
    else:
?>
    <div class="col-sm-12 col-md-12 col-lg-6 widget data-<?php echo $pl."-".$fc; ?>"   >
        <div class="box">
            <div class="wrapRolling">
                <div class="boxHeader"> 
                     <h2 class="widgetTitle"><?php echo $descripcion;?></h2>
                </div>
                <div class="boxContent">
                    <div >
                        <?php 
                        if(isset($data)):
                            if(count($data)):
                                $aux = $data;
                                $lista = Util::centralizandoList($data["periodos"]);
                                //$data["periodos"] = $lista;
                                
                                if($estado == true):
                                ?>
                        <input type="hidden" class="date-table" name="date-table[]" value="<?php echo htmlentities(json_encode($aux))?>">
                                <?php
                                    endif;
                                ?>
                        <table  class="tabletops table table-hover table-bordered" >
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th class="period"><?php echo $data["dias"]["dia1"] ?> dia(s)</th>
                                    <th class="period"><?php echo $data["dias"]["dia2"] ?> dia(s)</th>
                                    <th class="period"><?php echo $data["dias"]["dia3"] ?> dia(s)</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                for($i = 0; $i < count($lista); $i++):
                            ?>
                                <tr>
                                    <td class="legend">
                                        <div  style="width: 10px; height: 10px; display: inline-block;" class="color<?php echo $i + 1;?>"></div>
                                    </td>
                                    <th class="post" title="<?php echo $lista[$i]["texto"];?>">
                                        <?php echo $lista[$i]["texto"];?>
                                    </th>
                                    <td class="numbers">
                                        <?php echo $lista[$i]["columna1"];?>
                                    </td>
                                    <td class="numbers">
                                        <?php echo $lista[$i]["columna2"];?>
                                    </td>
                                    <td class="numbers">
                                        <?php echo $lista[$i]["columna3"];?>
                                    </td>
                                </tr>
                            <?php 
                                endfor;
                            ?>
                            </tbody>   
                        </table>
                            <?php
                             endif;
                        else:
                        ?>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 gif" style="text-align: center">
                                <img src="<?php echo URL::base(true)?>media/images/loader.gif" />
                            </div>
                        </div>  
                        <?php
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if($estado == true):
        ?>
        <a href="javascript:void(0)" class="btn btn-xs btn-danger remove-widget"><span class="glyphicon glyphicon-remove "></span> </a>
        <?php
        endif;
        ?>
    </div>
<?php
    endif;
elseif($dashboard == 3):
    if($tipo == 1 or $tipo == 3):
   ?>
    <div class="col-sm-12 col-md-6 col-lg-3 widget data-<?php echo $pl."-".$fc; ?>"  >
        <div class="box">
            <div class="wrapRolling">
                <div class="boxHeader"> 
                     <h2 class="widgetTitle"><?php echo $descripcion;?></h2>
                </div>
                <div class="boxContent">
                    <div class="barTrend">
                        <?php
                        if(isset($data)):
                            if(count($data)):
                            if($estado == true):
                    ?>
                        <input type="hidden" class="date-table" name="date-table[]" value="<?php echo htmlentities(json_encode($data))?>">
                    <?php
                            endif;
                    ?>
                        <table class="table3">
                            <tbody>
                                <tr>
                                    <td class="wrapValuedia"><h4>Fecha de reporte</h4></td>
                                    <td class="wrapValuedia"><h4>Fecha a comparar:</h4></td>
                                    <td class="wrapValuedia"><h4>Porcentaje</h4></td>
                                </tr>
                                <tr>
                                    <td class="wrapValue"><?php echo $data[0]?></td>
                                    <td class="wrapValue"><?php echo (!is_null($data[1]))? $data[1] : 0;  ?></td>
                                    <td class="wrapValue"><?php echo $data["porcentaje"] ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <?php
                            endif;
                        else:
                        ?>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 gif" style="text-align: center">
                                <img src="<?php echo URL::base(true)?>media/images/loader.gif" />
                            </div>
                           
                        </div>  
                        <?php
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if($estado == true):
        ?>
        <a href="javascript:void(0)" class="btn btn-xs btn-danger remove-widget"><span class="glyphicon glyphicon-remove "></span> </a>
        <?php
        endif;
        ?>
    </div>
<?php
    else:
?>
    <div class="col-sm-12 col-md-12 col-lg-6 widget data-<?php echo $pl."-".$fc; ?>"   >
        <div class="box">
            <div class="wrapRolling">
                <div class="boxHeader"> 
                    <h2 class="widgetTitle"><?php echo $descripcion;?></h2>
                </div>
                <div class="boxContent">
                    <div >
                        <?php 
                        if(isset($data)):
                            if(count($data)):
                                if($estado == true):
                    ?>
                        <input type="hidden" class="date-table" name="date-table[]" value="<?php echo htmlentities(json_encode($data))?>">
                    <?php
                                endif;
                    ?>
                        <table  class="tabletops table table-hover table-bordered" data-datatable="<?php echo htmlentities(json_encode($data))?>">
                            <tbody>
                            <?php
                                for($i = 0; $i < count($data[0]); $i++):
                                ?>
                                <tr>
                                    <td class="legend">
                                        <div  style="width: 10px; height: 10px; display: inline-block;" class="color<?php echo $i + 1;?>"></div>
                                    </td>
                                    <th class="post" title="<?php echo $data[0][$i]["texto"];?>">
                                        <?php echo $data[0][$i]["texto"];?>
                                    </th>
                                    <td class="numbers">
                                        <?php echo $data[0][$i]["cantidad"];?>
                                    </td>
                                </tr>
                                <?php 
                                endfor;
                                ?>
                            </tbody>
                        </table>
                        <?php
                            endif;
                        else:
                        ?>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 gif"  style="text-align: center">
                                <img src="<?php echo URL::base(true)?>media/images/loader.gif" />
                            </div>
                        </div>    
                        <?php
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if($estado == true):
        ?>
        <a href="javascript:void(0)" class="btn btn-xs btn-danger remove-widget"><span class="glyphicon glyphicon-remove "></span> </a>
        <?php
        endif;
        ?>
    </div>
<?php
    endif;
endif;
?>