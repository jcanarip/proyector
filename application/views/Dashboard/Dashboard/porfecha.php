<div class="row" data-step="1" data-intro="Seleccion los intervalos de fecha con los que deseas crear tu dashboard.">
    <div class='col-md-6'>
        <div class="form-group">
            <label for="fechainicio">Fecha Inicio:</label>
             <div class='input-group date' id='datetimepickerfechainicio'>
                 <input type='text'  class="form-control" name="fechainicio" id="fechainicio" placeholder="dd-mm-aaaa" value="" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
    <div class='col-md-6'>
        <div class="form-group">
            <label for="fechafin">Fecha Fin:</label>
             <div class='input-group date' id='datetimepickerfechafin'>
                 <input type='text'  class="form-control" name="fechafin" id="fechafin" placeholder="dd-mm-aaaa" value="" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
</div>
<h4>Fechas a comparar (opcional)</h4>
<div class="row">
    <div class='col-md-6'>
        <div class="form-group">
            <label for="fechainicioopcional">Fecha Inicio</label>
             <div class='input-group date' id='datetimepickerfechainicioopcional'>
                 <input type='text'  class="form-control" name="fechainicioopcional" id="fechainicioopcional" placeholder="dd-mm-aaaa" value="" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
    <div class='col-md-6'>
        <div class="form-group">
            <label for="fechafinopcional">Fecha Fin:</label>
            <input type='text'  class="form-control" name="fechafinopcional" id="fechafinopcional" placeholder="dd-mm-aaaa" value="" readonly/>
        </div>
    </div>
</div>