<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            Dashboard <?php echo ($oReporte->descripcion)?>
        </div>
    </div>
    <!--<ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">Tables</li>
    </ol>-->
    <div class="clearfix">
    </div>
</div>
<div id="modal-config" class="modal fade" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">
                    ×</button>
                <h4 class="modal-title">Enviar reporte via email</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="frmemail">
                    <input type="hidden" name="time" id="time" value="<?php echo $oReporte->id; ?>">
                    <div class="form-group">
                        <label for="Email">Email</label>
                        <div class="input-icon right">
                            <i class="fa fa-envelope"></i>
                            <input type="email" class="form-control" placeholder="Separe los emails con comas"  name="email" id="email">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cerrar</button>
                <button type="button" id="sendemail" class="btn btn-primary">Enviar</button>
            </div>
        </div>
    </div>
</div>
<div class="page-content">
    <input type="hidden" id="t" name="t" value="<?php echo $oReporte->id ?>">
    <div id="tab-general">
        <div id="generalTabContent" class="tab-content">
            <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 text-right" style="margin-bottom: 15px;">
                            <?php if($oUser->profile_id != 3){?>
                            <a href="<?php echo URL::base(TRUE) ?>dashboard/dashboard/view/<?php echo $oReporte->id ?>" class="btn btn-primary">Editar</a>
                            <?php }?>
                            <a href="#"id="btnPrint" class="btnPrint btn btn-danger"><span class="glyphicon glyphicon-download-alt"></span>  PDF</a>
                            <a href="#"id="btnExcel" class="btnExcel btn btn-danger"><span class="glyphicon glyphicon-download-alt"></span>  EXCEL</a>
                            <a href="#"id="btnEmail" class="btnEmail btn btn-danger" data-toggle="modal" data-target="#modal-config" ><span class="glyphicon glyphicon-download-alt"></span>  ENVIAR A EMAIL</a>
                        </div>
                    </div>
                    <div class="row" id="pagechart">
                    <?php
                    if(isset($oReporte->datetime) and !empty($oReporte->datetime))
                    {
                        $json = json_decode($oReporte->datetime, true);
                        
                        if(count($json["data"]) >0)
                        {
                            foreach ($json["data"] as $value) 
                            {

                                echo View::factory("Dashboard/Dashboard/tipodashboard")
                                        ->set("tipo", $value["tipo"])
                                        ->set("descripcion", $value["descripcion"])
                                        ->set("dashboard", $json["tipo_dhasboard"])
                                        ->set("pl", $value["pl"])
                                        ->set("fc", $value["fc"])
                                        ->set("data", $value)
                                        ->set("estado", false);
                            }
                        }
                    }
                    ?>    
                    </div>
            </div>    
        </div>
    </div>
</div>
