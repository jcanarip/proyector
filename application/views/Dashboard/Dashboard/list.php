<?php
$oUser = Auth::instance()->get_user();

foreach ($datatables->result() as $dashboard)
{
    $texto = "warning";
    $icon = "ok";
    $textomenu ="ACTIVAR";
    if($dashboard->status == 1 ) 
    {
        $texto = "danger";
        $icon = "trash";
        $textomenu ="SUSPENDER";
    }
    
    $menu = '<a href="'.URL::base(TRUE).'dashboard/dashboard/show/'.$dashboard->id.'" class="btn btn-sm btn-green">Ver</a>&nbsp;&nbsp;<a href="'.URL::base(TRUE).'dashboard/dashboard/view/'.$dashboard->id.'" class="btn btn-sm btn-primary">Preview</a>&nbsp;&nbsp;<a href="'.URL::base(TRUE).'dashboard/dashboard/editar/'.$dashboard->id.'" class="btn btn-sm btn-info">EDITAR</a>&nbsp;&nbsp;<a href="'.URL::base(TRUE).'dashboard/dashboard/eliminar/'.$dashboard->id.'" class="btn btn-sm btn-'.$texto.'">'.$textomenu.'</a>';
    if($oUser->profile_id == 3)
    {
        $menu = '<a href="'.URL::base(TRUE).'dashboard/dashboard/show/'.$dashboard->id.'" class="btn btn-sm btn-green">VER</a>';
    }
    
    $datatables->add_row(array
    (
        $dashboard->id,$dashboard->descripcion,$menu
    ));
}
?>