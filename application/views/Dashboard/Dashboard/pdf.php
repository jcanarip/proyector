<html lang="en"><head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="#">
<link rel="apple-touch-icon" href="#">
<link rel="apple-touch-icon" sizes="72x72" href="#">
<link rel="apple-touch-icon" sizes="114x114" href="#">
<title>CMS :: DASHBOARDS</title>
<link rel="shortcut icon" href="/media/img/favicon.png">
<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
<link type="text/css" rel="stylesheet" href="http://www.tribal121.com/proyectotribal121/media/css/jquery-ui-1.10.4.custom.min.css">
<link type="text/css" rel="stylesheet" href="http://www.tribal121.com/proyectotribal121/media/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" href="http://www.tribal121.com/proyectotribal121/media/css/simple-line-icons.css">
<link type="text/css" rel="stylesheet" href="http://www.tribal121.com/proyectotribal121/media/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="http://www.tribal121.com/proyectotribal121/media/lib/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css?1452093129">
<link type="text/css" rel="stylesheet" href="http://www.tribal121.com/proyectotribal121/media/css/animate.css">
<link type="text/css" rel="stylesheet" href="http://www.tribal121.com/proyectotribal121/media/css/main.css?1452093129">
<link type="text/css" rel="stylesheet" href="http://www.tribal121.com/proyectotribal121/media/css/style-responsive.css">
<link type="text/css" rel="stylesheet" href="http://www.tribal121.com/proyectotribal121/media/css/zabuto_calendar.min.css">
<link type="text/css" rel="stylesheet" href="http://www.tribal121.com/proyectotribal121/media/css/pace.css">
<link type="text/css" rel="stylesheet" href="http://www.tribal121.com/proyectotribal121/media/css/jquery.news-ticker.css">
<link href="https://ajax.googleapis.com/ajax/static/modules/gviz/1.1/core/tooltip.css" rel="stylesheet" type="text/css"><style id="holderjs-style" type="text/css"></style></head> 
<body class="" style="
    padding: 0px;
">
    <div>
        <div id="wrapper">
            <div id="page-wrapper" style=" margin: 0px;">
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">
                            <?php echo $oReporte->descripcion?>        
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="page-content" style=" padding: 0px;">
                    <input type="hidden" id="t" name="t" value="7">
                    <div id="tab-general">
                        <div id="generalTabContent" class="tab-content" style="width: 997px;">
                            <div class="form-group">
                                <div class="row" id="pagechart">
                                   <?php
                                    if(isset($oReporte->datetime) and !empty($oReporte->datetime))
                                    {
                                        $json = json_decode($oReporte->datetime, true);

                                        if(count($json["data"]) >0)
                                        {
                                            foreach ($json["data"] as $value) 
                                            {

                                                echo View::factory("Dashboard/Dashboard/tipodashboard")
                                                        ->set("tipo", $value["tipo"])
                                                        ->set("descripcion", $value["descripcion"])
                                                        ->set("dashboard", $json["tipo_dhasboard"])
                                                        ->set("pl", $value["pl"])
                                                        ->set("fc", $value["fc"])
                                                        ->set("data", $value)
                                                        ->set("estado", false);
                                            }
                                        }
                                    }
                                    ?>    
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
                <div id="footer">
                    <div class="copyright">
                       <a href="#">© Tribal121 2015. Designed by Tribal121</a>
                    </div>
                </div>
            </div>    
        </div>
    </div>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/jquery.js"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/jquery-ui.js"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/bootstrap.min.js"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/bootstrap-hover-dropdown.js"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/html5shiv.js"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/respond.min.js"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/jquery.metisMenu.js"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/jquery.slimscroll.js"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/jquery.cookie.js"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/icheck.min.js"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/jquery.news-ticker.js"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/jquery.menu.js"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/holder.js"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/responsive-tabs.js"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/zabuto_calendar.min.js"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/main.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['corechart']}]}?1452109167"></script><link href="https://www.google.com/uds/api/visualization/1.1/5887f2021b7365b5d9cdb0c833d0c5b8/ui+es_419.css" type="text/css" rel="stylesheet"><script src="https://www.google.com/uds/api/visualization/1.1/5887f2021b7365b5d9cdb0c833d0c5b8/webfontloader,format+es_419,default+es_419,ui+es_419,corechart+es_419.I.js" type="text/javascript"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/moment-with-locales.js?1452109167"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js?1452109167"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/js/dashboard.dashboard.show.js?1452109167"></script>
<script type="text/javascript" src="http://www.tribal121.com/proyectotribal121/media/lib/jquery.base64.min.js?1452109167"></script>
</body></html>
