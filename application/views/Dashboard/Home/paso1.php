<div class="page-content">
    <div id="tab-general" class=" text-center">
        <div id="generalTabContent" class="tab-content">
            <h2>Paso1: Conecta sus Redes</h2>
            <div class="container">
            <div class="row">
            <?php
            foreach($lSocial as $oSocial)
            {
            ?>
            <div class="col-sm-6 col-md-3">
            <div class="thumbnail">
            <img src="<?php echo URL::base(true)."media/images/".strtolower($oSocial->name).".png"?>" alt="<?php echo $oSocial->name?>">
            <div class="caption">
              <h4><?php echo $oSocial->descripcion?></h4> 
              <?php
              if(count($lAutorizacion) > 0):
              ?>
              <ul class="list-group">
                  <?php
                  foreach ($lAutorizacion as $value):
                      if($value->social_id == $oSocial->id):
                  ?>
                  <li class="list-group-item"><?php echo $value->username?>
                  <?php
                  $boolean = true;
                  foreach ($value->aItem->find_all() as $oItem) :
                      if($oItem->aReporte->where("status","=",1)->count_all() >0):
                          $boolean = false;
                          break;
                      endif;
                  endforeach;

                  if($boolean == true):
                  ?>    
                  <span class="pull-right">
                      <a href="eliminar/<?php echo $value->id ?>" class="btn btn-xs btn-danger">
                          <span class="glyphicon glyphicon-trash"></span>
                      </a>
                  </span>
                  <?php
                  endif;
                  ?>   
                  </li>
                  <?php
                      endif;
                  endforeach;
                  ?>
              </ul>
              <?php
              endif;
              ?>
              <p>
                  <a href="<?php echo URL::base(true)?>dashboard/home/agregar/<?php echo $oSocial->id?>" class="btn btn-primary" role="button">Agregar</a> 
              </p>
            </div>
            </div>
            </div>
            <?php
            }
            ?>
            </div>
            </div>
            <a href="<?php echo url::base()?>dashboard/home/saltarintro">Saltar este paso e ir directamente a la administracion de reportes.</a>
        </div>
    </div>
</div>