<?php
echo Message::display();
?>
<div class="page-content">
    <div id="tab-general" class=" text-center">
        <div id="generalTabContent" class="tab-content">
            <h2>Paso2: Selecciona la data a mostrarse</h2>
            <form action="" method="post">
                <div class="form-group">
                <div class="container">
                    <div class="row">
                   <?php
                   foreach ($lAutorizacion as $oAutorizacion):
                   ?>
                       <h3>Cuentas de <?php echo $oAutorizacion->username?>:</h3>
                       <ul class="list-group"> 
                       <?php
                       foreach ($oAutorizacion->aItem->find_all() as $key => $oItem) :
                        ?>
                           <li class="list-group-item"><input type="checkbox" class="items" id="<?php echo $key?>" name="items[]" value="<?php echo $oItem->id?>">&nbsp;&nbsp;<span><?php echo $oItem->descripcion?></span></li>
                        <?php
                        endforeach;
                        ?>
                        </ul> 
                   <?php
                    endforeach;
                   ?>  
                    </div>
                </div>
                </div>
                <button type="submit" style="display:none" class="btn btn-primary">Guardar</button>    
            </form>
             <a href="<?php echo url::base()?>dashboard/home/saltarintro">Saltar este paso e ir directamente a la administracion de reportes.</a>
        </div>
    </div>
</div>