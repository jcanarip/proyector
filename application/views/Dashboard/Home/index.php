<div class="page-content">
    <div id="tab-general" class=" text-center">
        <div id="generalTabContent" class="tab-content">
            <h2>En solo 3 pasos podras configurar tu primer Dashboard</h2>
            <div class="container">
                <div class="row">
                    <div class="timeline-centered">
                        <article class="timeline-entry left-aligned">
                            <div class="timeline-entry-inner">
                                <div class="timeline-icon bg-blue"><i class="icon-bulb"></i></div>
                                <div class="timeline-label bg-blue"><h4 class="timeline-title">1</h4>
                                    <p>Conectar con tus redes.</p>
                                </div>
                            </div>
                        </article>
                        <article class="timeline-entry ">
                            <div class="timeline-entry-inner">
                                <div class="timeline-icon bg-blue"><i class="icon-briefcase"></i></div>
                                <div class="timeline-label bg-blue"><h4 class="timeline-title">2</h4>
                                    <p>Selecciona la data a mostrarse.</p>
                                </div>
                            </div>
                        </article>
                        <article class="timeline-entry left-aligned">
                            <div class="timeline-entry-inner">
                                <div class="timeline-icon bg-blue"><i class="icon-folder-alt"></i></div>
                                <div class="timeline-label bg-blue"><h4 class="timeline-title">3</h4>
                                    <p>Selecciona el Dashboard</p>
                                </div>
                            </div>
                        </article>
                        <article class="timeline-entry ">
                            <div class="timeline-entry-inner">
                                <div class="timeline-icon bg-blue"><i class="icon-like"></i></div>
                                <div class="timeline-label bg-blue"><h4 class="timeline-title">¿Listo?</h4>
                                    <p>Continua <a style="color:#FFF;font-size: 25px" href="<?php echo url::base()?>dashboard/home/paso1">aqui</a> </p>
                                </div
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        <a href="<?php echo url::base()?>dashboard/home/saltarintro">Saltar este paso e ir directamente a la administracion de reportes.</a>
        </div>
    </div>
</div>