<?php
echo Message::display();
?>
<div class="page-content">
    <div id="tab-general">
        <div id="generalTabContent" class="tab-content">
            <h2>Paso3: Completa los datos y selecciona el dashboard</h2>    
            <form action="" method="post">
                <div class="form-group">
                    <label for="empresa_id">Cuenta</label>
                    <div class="input-icon right">
                        <select name="empresa_id" id="empresa_id" class="form-control">
                            <option value=""  >-- Elige una cuenta --</option>
                            <?php
                            foreach ($aListadEmpresas as $value):
                            ?>
                            <option value="<?php echo $value->id?>"   ><?php echo $value->name ?></option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="descripcion">Titulo del dashboard</label>
                    <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="" value="">
                </div>
                <div class="form-group">
                    <label for="tipo">Tipo del Dashboard</label>
                    <select name="tipo" id="tipo" class="form-control">
                        <?php
                        if(count($aTipodashboard) > 0):
                        ?>
                        <option value="" title="">-- Elige un tipo de dashboard --</option>
                        <?php
                        foreach ($aTipodashboard as $value):
                        ?>
                        <option value="<?php echo $value->id?>" title="<?php echo $value->texto?>" ><?php echo $value->name?></option>
                        <?php 
                        endforeach;
                        else:
                        ?>
                        <option value="" title="">-- No existe ningùn tipo de dashboard --</option>
                        <?php 
                        endif;
                        ?>
                    </select>
                    <div class="note note-info" id="info-tipo-plantilla" style="display:none">
                        <h4 class="box-heading">Informaciòn importante:</h4>
                        <p></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="periodo">Periodo</label>
                    <select name="periodo" id="periodo" class="form-control">
                        <option value="" title="">-- No existe ningùn tipo de dashboard --</option>
                    </select>
                </div>

              <button type="submit" class="btn btn-primary">Guardar</button>
            </form>
            <a href="<?php echo url::base()?>dashboard/home/saltarintro">Saltar este paso e ir directamente a la administracion de reportes.</a>
        </div>
    </div>
</div>