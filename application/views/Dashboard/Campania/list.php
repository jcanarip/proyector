<?php

foreach ($datatables->result() as $campania)
{
    $texto = "warning";
    $icon = "ok";
    if($campania->status == 1 ) 
    {
        $texto = "danger";
        $icon = "trash";
    }
    
    if($campania->status == 2 ) 
    {
        $texto = "success";
        $icon = "ok";
    }
    
    $datatables->add_row(array
    (
        $campania->descripcion,
        date("d-m-Y H:i",  strtotime($campania->inicio_campania)) ,
        date("d-m-Y H:i",  strtotime($campania->fin_campania)) ,
        '<a href="'.URL::base(TRUE).'dashboard/campania/editar/'.$campania->id.'" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-repeat"></span></a>&nbsp;&nbsp;<a href="'.URL::base(TRUE).'dashboard/campania/eliminar/'.$campania->id.'" class="btn btn-sm btn-'.$texto.'"><span class="glyphicon glyphicon-'.$icon.'"></span</a>'
    ));
}
?>