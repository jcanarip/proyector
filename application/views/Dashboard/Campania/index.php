<?php
echo Message::display();
?>
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            Campañas
        </div>
    </div>
    <!--<ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">Tables</li>
    </ol>-->
    <div class="clearfix">
    </div>
</div>
<div class="page-content">
    <div id="tab-general">
        <div class="row mbl">
            <div class="col-lg-12">
                <a href="campania/crear" class="btn btn-primary">Crear</a>
            </div>
        </div>
        <div class="row mbl">
           
            <div class="col-lg-12">
                
                <div class="panel panel-grey">
                    <div class="panel-body">
                        <table id="campanialist" class="table table-hover table-bordered" >
                            <thead>
                                <tr>                      
                                    <th>Descripcion</th>
                                    <th>Fecha inicio de campaña</th>
                                    <th>Fecha fin de campaña</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>