<?php
echo Message::display();
?>
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            <?php echo ($oCampania->id)? 'Editar':'Nuevo';?>
        </div>
    </div>
    <!--<ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">Tables</li>
    </ol>-->
    <div class="clearfix">
    </div>
</div>
<div class="page-content">
    <div id="tab-general">
        <div id="generalTabContent" class="tab-content">
        <form action="" method="post">
            <div class="form-group">
                <label for="descripcion">Descripcion de la Campaña</label>
                <div class="input-icon right">
                    <i class="fa fa-envelope"></i>
                    <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Nombre" value="<?php echo $oCampania->descripcion ?>">
                </div>
            </div>
            <!--<div class="form-group">
                <label for="notas">Notas</label>
                <textarea class="form-control" name="notas" id="notas" placeholder="Notas" ><?php echo $oCampania->notas ?></textarea>
            </div>-->
             <div class="form-group">
                <label for="empresa_id">Marca</label>
                <div class="input-icon right">
                    <select name="empresa_id" id="empresa_id" class="form-control">
                        <option value=""  >-- Elige una Marca --</option>
                        <?php
                        foreach ($aListadEmpresas as $value):
                        ?>
                        <option value="<?php echo $value->id?>" <?php echo ($oCampania->empresa_id == $value->id)? "selected" : ""?>  ><?php echo $value->name ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="social_id">Red Social</label>
                <div class="input-icon right">
                    <select name="social_id" id="social_id" class="form-control">
                        <option value=""  >-- Elige una opcion--</option>
                        <?php
                        foreach ($aSocial as $value):
                        ?>
                        <option value="<?php echo $value->id?>" <?php echo ($oCampania->social_id == $value->id)? "selected" : ""?>  ><?php echo $value->descripcion ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            
            <div class="form-group">
                <label for="autorizacion_id">Cuenta</label>
                <div class="input-icon right">
                    <select name="autorizacion_id" id="autorizacion_id" class="form-control">
                        <option value=""  >-- Elige una opcion --</option>
                        <?php
                        foreach ($aCuentas as $value):
                        ?>
                        <option value="<?php echo $value->id?>" <?php echo ($oCampania->autorizacion_id == $value->id)? "selected" : ""?> ><?php echo $value->username ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div> 
            
            <div class="form-group">
                <label for="item_id">Item</label>
                <div class="input-icon right">
                    <select name="item_id" id="item_id" class="form-control">
                        <option value=""  >-- Elige una opcion --</option>
                        <?php
                        foreach ($aItem as $value):
                        ?>
                        <option value="<?php echo $value->id?>" <?php echo ($oCampania->item_id == $value->id)? "selected" : ""?>  ><?php echo $value->descripcion ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="indicador">Indicador</label>
                <div class="input-icon right">
                    <select name="indicador" id="indicador" class="form-control">
                        <option value=""  >-- Elige una opcion --</option>
                        <?php
                        foreach ($aIndicador as $value):
                        ?>
                        <option value="<?php echo $value["id"]?>" data-tipo="<?php echo $value["tipo"]?>" <?php echo ($oCampania->indicador == $value["id"])? "selected" : ""?>  ><?php echo $value["descripcion"] ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            
            <div class="form-group objeto">
                <label for="objeto">Objeto a medir</label>
                <div class="input-icon right">
                    <select name="objeto" id="objeto" class="form-control">
                        <option value=""  >-- Elige una opcion --</option>
                        <?php
                        foreach ($aObjeto as $value):
                        ?>
                        <option value="<?php echo $value["id"]?>" <?php echo ($oCampania->objeto == $value["id"])? "selected" : ""?>  ><?php echo $value["descripcion"] ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
        
            <div class="form-group">
                <label for="inicio_campania">Fecha inicio de la campaña</label>
                 <div class='input-group date' id='datetimepickerinicio'>
                     <input type='text'  class="form-control" name="inicio_campania" id="inicio_campania" placeholder="dd-mm-aaaa" value="<?php echo ($oCampania->inicio_campania)? date("d-m-Y H:i",  strtotime($oCampania->inicio_campania)) : "" ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="fin_campania">Fecha final de la campaña</label>
                 <div class='input-group date' id='datetimepickerfin'>
                     <input type='text'  class="form-control" name="fin_campania" id="fin_campania" placeholder="dd-mm-aaaa" value="<?php echo ($oCampania->fin_campania)? date("d-m-Y H:i",  strtotime($oCampania->fin_campania)) : "" ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="metrica">Objetivo</label>
                <input type="text" class="form-control" name="metrica" id="metrica" placeholder="Metrica" value="<?php echo $oCampania->metrica ?>">
            </div>
          <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
        </div>
    </div>
</div>
