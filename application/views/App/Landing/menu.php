<?php if ($oUser): ?>
<nav id="sidebar" role="navigation" data-step="2" data-intro="Template has &lt;b&gt;many navigation styles&lt;/b&gt;"
    data-position="right" class="navbar-default navbar-static-side">
    <div class="sidebar-collapse menu-scroll">
        <ul id="side-menu" class="nav">
            <div class="clearfix"></div>
            <li data-step="1" data-intro="Aqui puedes crear, editar,activar y anular a todos los usuarios que usen el sistema, dandoles un tipo de perfil y relacionandolos con las marcas ya creadas." 
                <?php echo ($seleccion == 'user/index')? 'class="active"' :'';?> >
                <a href="<?php echo url::base(true)?>app/user" >
                    <i class="fa fa-user fa-fw">
                        <div class="icon-bg bg-orange"></div>
                    </i>
                    <span class="menu-title">Gestión de Usuarios</span>
                </a>
            </li>
            <li data-step="2" data-intro="Aqui puedes crear, editar y anular las marcas que seran relacionadas con los reportes que crees posteriormente."  <?php echo ($seleccion == 'empresa/index')? 'class="active"' :'';?>>
                <a href="<?php echo url::base(true)?>app/empresa" >
                    <i class="fa fa-user fa-fw">
                        <div class="icon-bg bg-orange"></div>
                    </i>
                    <span class="menu-title">Gestión de Marcas</span>
                </a>
            </li>
            <li data-step="3" data-intro="Aqui puedes crear, editar y anular las redes sociales que seran relacionadas con los reportes que crees posteriormente. SOLO PARA DESARROLLADORES"  <?php echo ($seleccion == 'social/index')? 'class="active"' :'';?>>
                <a href="<?php echo url::base(true)?>app/social" >
                    <i class="fa fa-user fa-fw">
                        <div class="icon-bg bg-orange"></div>
                    </i>
                    <span class="menu-title">Gestión de redes</span>
                </a>
            </li>
             <li data-step="4" data-intro="Aqui puedes crear, editar y anula los tipos de dashboards" <?php echo ($seleccion == 'tipodashboard/index')? 'class="active"' :'';?>>
                <a href="<?php echo url::base(true)?>app/tipodashboard" >
                    <i class="fa fa-user fa-fw">
                        <div class="icon-bg bg-orange"></div>
                    </i>
                    <span class="menu-title">Gestion de Tipo de Dashboards</span>
                </a>
            </li>
            <li data-step="5" data-intro="Dirígete al entorno dodne crearas tus modules, desde aqui." >
                <a href="<?php echo url::base(true)?>dashboard" >
                    <i class="fa fa-user fa-fw">
                        <div class="icon-bg bg-orange"></div>
                    </i>
                    <span class="menu-title">Crear Dashboards</span>
                </a>
            </li>
           
        </ul>
    </div>
</nav>
<?php endif ?>