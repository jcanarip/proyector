<?php

foreach ($datatables->result() as $user)
{
    $texto = "warning";
    $icon = "ok";
    $textomenu ="ACTIVAR";
    if($user->status == 1 or $user->status == 3) 
    {
        $texto = "danger";
        $icon = "trash";
        $textomenu ="SUSPENDER";
    }
    
    if($user->status == 4)
    {
        $texto = "success";
        $icon = "thumbs-up";
        $textomenu ="EVALUAR";
    }
    
    $datatables->add_row(array
    (
        $user->first_name,
        $user->last_name,
        $user->email,
        '<a href="'.URL::base(TRUE).'app/user/editar/'.$user->id.'" class="btn btn-sm btn-info">EDITAR</a>&nbsp;&nbsp;<a href="'.URL::base(TRUE).'app/user/eliminar/'.$user->id.'" class="btn btn-sm btn-'.$texto.'">'.$textomenu.'</a>'
    ));
}
?>