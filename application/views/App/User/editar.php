






<?php
echo Message::display();
?>
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            <?php echo ($oUser->id)? 'Editar':'Nuevo';?>
        </div>
    </div>
    <!--<ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">Tables</li>
    </ol>-->
    <div class="clearfix">
    </div>
</div>
<div class="page-content">
    <div id="tab-general">
        <div id="generalTabContent" class="tab-content">
        <form action="" method="post">
            
            <div class="form-group">
                <label for="first_name">Nombres</label>
                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Nombres" value="<?php echo $oUser->first_name ?>">
            </div>
            <div class="form-group">
                <label for="last_name">Apellidos</label>
                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Apellidos" value="<?php echo $oUser->last_name ?>">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <div class="input-icon right">
                    <i class="fa fa-envelope"></i>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $oUser->email ?>">
                </div>
            </div>
            <!--<div class="form-group">
                <label for="birthday">Fecha de Nacimiento</label>
                 <div class='input-group date' id='datetimepickerbirthday'>
                     <input type='text'  class="form-control" name="birthday" id="birthday" placeholder="dd-mm-aaaa" value="<?php echo ($oUser->birthday)? date("d-m-Y",  strtotime($oUser->birthday)) : "" ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>-->
            <div class="form-group">
                <label for="profile_id">Perfil</label>
                <select name="profile_id" id="profile_id" class="form-control">
                    <?php
                    foreach ($aProfile as $value):
                    ?>
                    <option value="<?php echo $value->id?>" <?php echo ($value->id == $oUser->profile_id  )? 'selected':''?> ><?php echo $value->name ?></option>
                    <?php
                    endforeach;
                    ?>
                </select>
            </div>
             <div class="form-group" id="groupempresa" <?php echo ($oUser->id)? (($oUser->profile_id == 1)? "style='display:none'" : "") : "style='display:none'"; ?> >
                <label for="empresa">Cuenta (s)</label>
                <select name="empresa[]" id="empresa" class="form-control" multiple="multiple">
                    <?php 
                    foreach ($aEmpresa as $value):
                    ?>
                    <option value="<?php echo $value->id?>" <?php echo (in_array($value->id,$aUserEmpresa) and $oUser->profile_id != 1)? 'selected':''?> ><?php echo $value->name ?></option>
                    <?php
                    endforeach;
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="genero">Gènero</label>
                <select name="genero" id="genero" class="form-control">
                    <option value="M" <?php echo ($oUser->genero == 'M')? "selected" : ""?>>Hombre</option>
                    <option value="F" <?php echo ($oUser->genero == 'F')? "selected" : ""?>>Mujer</option>
                </select>
            </div>
            <!--<div class="form-group">
                <label for="address">Direcciòn</label>
                <input type="text" class="form-control" name="address" id="address" placeholder="Direcciòn" value="<?php echo $oUser->address ?>">
            </div>-->
           
          <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
        </div>
    </div>
</div>