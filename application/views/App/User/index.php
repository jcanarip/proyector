<?php
echo Message::display();
?>
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title mrm">
            Usuarios
        </div>
        <div class="page-subtitle">Aqui podràs visualizar los usuarios activos y generar nuevos en caso los necesites.</div>
    </div>
    <!--<ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">Tables</li>
    </ol>-->
    <div class="clearfix">
    </div>
</div>
<div class="page-content">
    <div id="tab-general">
        <div class="row mbl">
            <div class="col-lg-12">
                <a href="user/crear" class="btn btn-primary">Crear</a>
            </div>
        </div>
        <div class="row mbl">
           
            <div class="col-lg-12">
                
                <div class="panel panel-grey">
                    <div class="panel-body">
                        <table id="userlist" class="table table-hover table-bordered" >
                            <thead>
                                <tr>                      
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Correo</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>