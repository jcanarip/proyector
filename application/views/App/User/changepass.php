<?php
echo Message::display();
?>
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            Cambio de contraseña
        </div>
    </div>
    <!--<ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">Tables</li>
    </ol>-->
    <div class="clearfix">
    </div>
</div>
<div class="page-content">
    <div id="tab-general">
        <div id="generalTabContent" class="tab-content">
        <form action="" method="post">

            <div class="form-group">
                <label for="password">Nuevo password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Nuevo password" value="" min="8" >
            </div>
            <div class="form-group">
                <label for="npassword">Repite el nuevo password</label>
                <input type="password" class="form-control" name="npassword" id="npassword" placeholder="Repite el nuevo password" value="" min="8">
            </div>

          <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
        </div>
    </div>
</div>