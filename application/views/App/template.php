<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="#">
<link rel="apple-touch-icon" href="#">
<link rel="apple-touch-icon" sizes="72x72" href="#">
<link rel="apple-touch-icon" sizes="114x114" href="#">
<title><?php echo $title ?></title>
<link rel="shortcut icon" href="/media/img/favicon.png" />
<link type='text/css' rel='stylesheet' href='http://fonts.googleapis.com/css?family=Montserrat:400,700' />
<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700" />
<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300" />
<link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/css/jquery-ui-1.10.4.custom.min.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/css/font-awesome.min.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/css/simple-line-icons.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/css/bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/lib/prompt/jquery-impromptu.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/lib/intro/introjs.min.css" />
<?php
foreach ($styles as $css) 
{
?>
    <link rel="stylesheet" type="text/css" href="<?php echo $css."?".time() ?>"  />
<?php
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/css/animate.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/css/main.css<?php echo "?".time() ?>" />
<link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/css/style-responsive.css"/>
<link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/css/zabuto_calendar.min.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/css/pace.css">
<link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/css/jquery.news-ticker.css" />
</head> 
<body>
    <div>
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <?php echo $cabecera ?>
        <div id="wrapper">
        <?php echo $menu ?>
            <div id="page-wrapper" <?php echo $margin;?>>
                <?php echo $content ?>
                <div id="footer">
                    <div class="copyright">
                       <a href="#">© Tribal121 2015. Designed by Tribal121</a>
                    </div>
                </div>
            </div>    
        </div>
    </div>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/jquery.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/bootstrap-hover-dropdown.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/html5shiv.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/respond.min.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/jquery.metisMenu.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/jquery.slimscroll.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/jquery.news-ticker.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/jquery.menu.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/pace.min.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/holder.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/responsive-tabs.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/jquery.flot.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/jquery.flot.categories.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/jquery.flot.pie.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/jquery.flot.tooltip.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/jquery.flot.fillbetween.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/jquery.flot.stack.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/jquery.flot.spline.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/zabuto_calendar.min.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/prompt/jquery-impromptu.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/intro/intro.min.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/jquery.base64.min.js"></script>
<script type="text/javascript" src="<?php echo URL::base(TRUE) ?>media/lib/main.js"></script>
<?php
foreach ($scripts as $js) 
{
    if(!is_null($js))
    {
?>
<script type="text/javascript" src="<?php echo $js."?".time() ?>"></script>
<?php
    }
}
?>
</body>
</html>