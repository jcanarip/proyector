<?php

foreach ($datatables->result() as $empresa)
{
    $texto = "warning";
    $icon = "ok";
    $textomenu ="ACTIVAR";
    if($empresa->status == 1 or $empresa->status == 3) 
    {
        $texto = "danger";
        $icon = "trash";
        $textomenu ="SUSPENDER";
    }
    
    $datatables->add_row(array
    (
        $empresa->name,
        $empresa->contacto,
        $empresa->email,
        '<a href="'.URL::base(TRUE).'app/empresa/editar/'.$empresa->id.'" class="btn btn-sm btn-info">EDITAR</a>&nbsp;&nbsp;<a href="'.URL::base(TRUE).'app/empresa/eliminar/'.$empresa->id.'" class="btn btn-sm btn-'.$texto.'">'.$textomenu.'</a>'
    ));
}
?>