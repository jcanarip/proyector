
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            <?php echo ($oEmpresa->id)? 'Editar':'Nuevo';?>
        </div>
    </div>
    <!--<ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">Tables</li>
    </ol>-->
    <div class="clearfix">
    </div>
</div>
<div class="page-content">
    <div id="tab-general">
        <div id="generalTabContent" class="tab-content">
        <form action="" method="post">
            <div class="form-group">
                <label for="name">Nombre de la Marca</label>
                <div class="input-icon right">
                    <i class="fa fa-envelope"></i>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Nombre" value="<?php echo $oEmpresa->name ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="phone">Telefono </label>
                <input type="number" class="form-control" name="phone" id="phone" placeholder="Telefono" value="<?php echo $oEmpresa->phone ?>">
            </div>
            <div class="form-group">
                <label for="contacto">Nombre del cliente</label>
                <input type="text" class="form-control" name="contacto" id="contacto" placeholder="Nombre del cliente" value="<?php echo $oEmpresa->contacto ?>">
            </div>
             <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" id="email" placeholder="email" value="<?php echo $oEmpresa->email ?>">
            </div>
          <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
        </div>
    </div>
</div>