<?php
echo Message::display();
?>

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            Editar Profile
        </div>
    </div>
    <!--<ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">Tables</li>
    </ol>-->
    <div class="clearfix">
    </div>
</div>
<div class="page-content">
    <div id="tab-general">
        <div id="generalTabContent" class="tab-content">
<form action="" method="post">
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $auxUSer->email ?>">
    </div>
    <div class="form-group">
        <label for="first_name">Nombres</label>
        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Nombres" value="<?php echo $auxUSer->first_name ?>">
    </div>
    <div class="form-group">
        <label for="last_name">Apellidos</label>
        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Apellidos" value="<?php echo $auxUSer->last_name ?>">
    </div>
    <div class="form-group">
        <label for="birthday">Fecha de Nacimiento</label>
         <div class='input-group date' id='datetimepickerbirthday'>
             <input type='text'  class="form-control" name="birthday" id="birthday" placeholder="dd-mm-aaaa" value="<?php echo ($auxUSer->birthday)? date("d-m-Y",  strtotime($auxUSer->birthday)) : "" ?>" />
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
    <div class="form-group">
        <label class="radio-inline">
            <input type="radio" name="genero" id="genero_1" value="M" <?php echo ($auxUSer->genero == 'M')? "checked" : ""?>> Hombre
        </label>
        <label class="radio-inline">
          <input type="radio" name="genero" id="genero_2" value="F" <?php echo ($auxUSer->genero == 'F')? "checked" : ""?>> Mujer
        </label>
    </div>
    <div class="form-group">
        <label for="address">Direcciòn</label>
        <input type="text" class="form-control" name="address" id="address" placeholder="Direcciòn" value="<?php echo $auxUSer->address ?>">
    </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
        </div>
    </div>
</div>

