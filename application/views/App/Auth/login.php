<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>CMS :: LOGIN</title>
    <link rel="shortcut icon" href="/media/img/favicon.png" />
    <link type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,800italic,400,700,800">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/css/animate.css">
    <link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/css/main.css">
    <link type="text/css" rel="stylesheet" href="<?php echo URL::base(TRUE) ?>media/css/style-responsive.css">

</head> 
<body  class="login-block">
    <div class="page-form">
        <div class="panel panel-blue">
            <div class="panel-body pan">
                <?php echo Message::display();?>
                 <form  method="post" class="form-horizontal">
                <div class="form-body login-padding">
                    <div class="text-center">
                        <h1 style="margin-top: 0px; font-size:42px; text-transform:uppercase; letter-spacing:-1px; color:#000; font-weight:bold">
                           <a href="<?php echo URL::base(TRUE) ?>" style="color:#000;">Xperia</a></h1>
                        <br />
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">
                            Email:</label>
                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa fa-user"></i>
                                <input type="text" id="email" name="email"  type="text" placeholder="" class="form-control" /></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">
                            Password:</label>
                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa fa-lock"></i>
                                <input type="password" id="password" name="password" placeholder="" class="form-control" /></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">
                        </label>
                        <div class="col-md-9">
                            <input type="checkbox" name="rememberme" >&nbsp;&nbsp;Remember me
                        </div>
                    </div> 
                    <div class="form-group mbn">
                        <div class="col-lg-12">
                            <div class="form-group mbn">
                                <div class="col-lg-3">
                                    &nbsp;
                                </div>
                                <div class="col-lg-9">
                                    <a href="<?php echo URL::base(TRUE) ?>auth/register" class="btn btn-default back-btn">Registrar</a>&nbsp;&nbsp;
                                    <button type="submit" class="btn btn-default sign-btn">
                                        Sign In</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="col-lg-12 text-center">
            <p>
                <a href="<?php echo URL::base(TRUE) ?>auth/forget" style="color:#d0dbf2">Forgot Something ?</a>            
            </p>
        </div>
    </div>
</body>
</html>
