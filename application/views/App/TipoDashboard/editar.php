
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            <?php echo ($oTipoDashboard->id)? 'Editar':'Nuevo';?>
        </div>
    </div>
    <!--<ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">Tables</li>
    </ol>-->
    <div class="clearfix">
    </div>
</div>
<div class="page-content">
    <div id="tab-general">
        <div id="generalTabContent" class="tab-content">
        <form action="" method="post">
            <div class="form-group">
                <label for="name">Nombre del Tipo de Dashboard</label>
                <div class="input-icon right">
                    <i class="fa fa-envelope"></i>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Nombre del Tipo de Dashboard" value="<?php echo $oTipoDashboard->name ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="texto">Texto</label>
                <div class="input-icon right">
                    <i class="fa fa-envelope"></i>
                    <textarea class="form-control" name="texto" id="texto" placeholder="Texto descriptivo">
                        <?php echo $oTipoDashboard->texto ?>
                    </textarea>
                </div>
            </div>
            
          <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
        </div>
    </div>
</div>