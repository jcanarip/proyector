<?php

foreach ($datatables->result() as $tipodashboard)
{
    $texto = "warning";
    $icon = "ok";
    $textomenu ="ACTIVAR";
    if($tipodashboard->status == 1 or $tipodashboard->status == 3) 
    {
        $texto = "danger";
        $icon = "trash";
        $textomenu ="SUSPENDER";
    }
    
    $datatables->add_row(array
    (
        $tipodashboard->id,
        $tipodashboard->name,
        '<a href="'.URL::base(TRUE).'app/tipodashboard/editar/'.$tipodashboard->id.'" class="btn btn-sm btn-info">EDITAR</a>&nbsp;&nbsp;<a href="'.URL::base(TRUE).'app/tipodashboard/eliminar/'.$tipodashboard->id.'" class="btn btn-sm btn-'.$texto.'">'.$textomenu.'</a>'
    ));
}
?>