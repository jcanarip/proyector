
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            <?php echo ($oSocial->id)? 'Editar':'Nuevo';?>
        </div>
    </div>
    <!--<ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">Tables</li>
    </ol>-->
    <div class="clearfix">
    </div>
</div>
<div class="page-content">
    <div id="tab-general">
        <div id="generalTabContent" class="tab-content">
        <form action="" method="post">
            <div class="form-group">
                <label for="descripcion">Nombre de la Red Social</label>
                <div class="input-icon right">
                    <i class="fa fa-envelope"></i>
                    <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Nombre a mostrar" value="<?php echo $oSocial->descripcion ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="name">Nombre de interno de la Red Social</label>
                <div class="input-icon right">
                    <i class="fa fa-envelope"></i>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Nombre del controlador que contendra las formulas" value="<?php echo $oSocial->name ?>">
                </div>
            </div>
            
          <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
        </div>
    </div>
</div>