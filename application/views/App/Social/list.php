<?php

foreach ($datatables->result() as $social)
{
    $texto = "warning";
    $icon = "ok";
    $textomenu ="ACTIVAR";
    if($social->status == 1 or $social->status == 3) 
    {
        $texto = "danger";
        $icon = "trash";
        $textomenu ="SUSPENDER";
    }
    
    $datatables->add_row(array
    (
        $social->id,
        $social->descripcion,
        '<a href="'.URL::base(TRUE).'app/social/editar/'.$social->id.'" class="btn btn-sm btn-info">EDITAR</a>&nbsp;&nbsp;<a href="'.URL::base(TRUE).'app/social/eliminar/'.$social->id.'" class="btn btn-sm btn-'.$texto.'">'.$textomenu.'</a>'
    ));
}
?>