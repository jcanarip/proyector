<?php defined('SYSPATH') or die('No direct access allowed.');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_Rep_Documenttype extends ORM
{
    protected $_table_name = 'rep_documenttype';
    
    protected $_has_many = array(
        'aUser' => array(
            'model' => 'User_User',
            'foreign_key'	=> 'documenttype_id',
        ),
        'aEmpresa' => array(
            'model' => 'User_Empresa',
            'foreign_key'	=> 'documenttype_id',
        ),
    );
    
    protected $_belongs_to = array(

        
       'oUbigeo' => array(
            'model' => 'Rep_Ubigeo',
            'foreign_key'	=> 'ubigeo_id',
        ),
    );
}