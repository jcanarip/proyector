<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Rep_Tipodashboard extends ORM
{
    protected $_table_name = 'rep_tipodashboard';
    
    protected $_has_many = array(
        'aPlantilla' => array(
            'model' => 'Redes_Plantilla',
            'foreign_key'	=> 'tipodashboard_id',
        ),
  
        'aReporte' => array(
            'model' => 'Redes_Reporte',
            'foreign_key'	=> 'tipo',
        ),
    );
 
}