<?php defined('SYSPATH') or die('No direct access allowed.');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_Rep_Ubigeo extends ORM
{
    protected $_table_name = "rep_ubigeo";
    
    protected $_has_many = array(
        "aUser" => array(
            'model' => "User_User",
            'foreign_key' => "ubigeo_id"
        ),
        "aDcoumentType" => array(
            'model' => "Rep_Documenttype",
            'foreign_key' => "ubigeo_id"
        ),
        "aEmpresa" => array(
            'model' => "User_Empresa",
            'foreign_key' => "ubigeo_id"
        ),
    );
}

