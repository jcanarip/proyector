<?php defined('SYSPATH') or die('No direct access allowed.');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_Rep_Campania extends ORM
{
    protected $_table_name = "rep_campania";
    
    protected $_has_many = array(
       
    );
    
    
    protected $_belongs_to = array(
        'oUser' => array(
            'model' => 'User_User',
            'foreign_key'	=> 'user_id',
        ),
        'oSocial' => array(
            'model' => 'Redes_Social',
            'foreign_key'	=> 'social_id',
        ),
        'oEmpresa' => array(
            'model' => 'User_Empresa',
            'foreign_key'	=> 'empresa_id',
        ),
        'oItem' => array(
            'model' => 'Redes_Item',
            'foreign_key'	=> 'item_id',
        ),
    );
}