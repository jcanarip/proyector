<?php defined('SYSPATH') or die('No direct access allowed.');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_Redes_Reporte extends ORM
{
    protected $_table_name = "redes_reporte";
    protected $_has_many = array(
        'aItem' => array(
            'model'   => 'Redes_Item',
            'through' => 'redes_reportedetalle',
            'foreign_key' => 'redes_reporte_id',
            'far_key' => 'redes_item_id',  
        ),
    );
    protected $_belongs_to = array(
        'oUser' => array(
            'model' => 'User_User',
            'foreign_key'	=> 'user_id',
        ),
        'oEmpresa' => array(
            'model' => 'User_Empresa',
            'foreign_key'	=> 'empresa_id',
        ),
    );
}