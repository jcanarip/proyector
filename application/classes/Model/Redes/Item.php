<?php defined('SYSPATH') or die('No direct access allowed.');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_Redes_Item extends ORM
{
    protected $_table_name = "redes_item";
    protected $_has_many = array(
        'aReporte' => array(
            'model'   => 'Redes_Reporte',
            'through' => 'redes_reportedetalle',
            'foreign_key' => 'redes_item_id',
            'far_key' => 'redes_reporte_id',       // "column name" relating to the Course Model in "students_courses" table  
        ),
        'aCampania' => array(
            'model'	=> 'Rep_Campania',
            'foreign_key'	=> 'item_id',
        ),
    );
    protected $_belongs_to = array(
        'oAutorizacion' => array(
            'model' => 'Redes_Autorizacion',
            'foreign_key'	=> 'autorizacion_id',
        )
    );
}