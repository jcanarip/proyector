<?php defined('SYSPATH') or die('No direct access allowed.');

require_once Kohana::find_file("vendor","facebook/src/facebook/autoload"); 

class Controller_Dashboard_Facebook extends Controller_Dashboard_Template
{
    public function action_index()
    {
        $id = $this->request->param('id');
        $redireccion = URL::base(TRUE)."dashboard/home/complete/".$id;
        $url = "https://www.facebook.com/dialog/oauth?client_id=".$this->fb_app_key."&redirect_uri=".$redireccion."&scope=read_insights,manage_pages&auth_type=reauthenticate";
        $this->redirect($url);
    }
    
    public function action_setdata()
    {
        $id = $this->request->param('id');
        
        $url = "https://graph.facebook.com/v2.4/me/?fields=name,accounts&access_token=".$this->fb_access_token;
        $fUser = json_decode(file_get_contents($url));
        
                
        $oUser =  $this->oUser;

        $aAautorizacion = ORM::factory("Redes_Autorizacion")
                            ->where("social_id","=",$id)
                            ->where("user_id","=",$oUser->id)
                            ->where("uId","=",$fUser->id)
                            ->find();
        
        if($aAautorizacion->loaded())
        {
            $aAautorizacion->access = json_encode($this->request->query()) ;
            $aAautorizacion->update_at = date("Y-m-d H:l:s");

        }else{
            $aAautorizacion->social_id = $id;
            $aAautorizacion->user_id = $oUser->id;
            $aAautorizacion->access = json_encode($this->request->query()) ;
            $aAautorizacion->created_at = date("Y-m-d H:l:s");
            $aAautorizacion->uId = $fUser->id;
        }
        
        $aAautorizacion->username = $fUser->name;  
        $aAautorizacion->save();

        $query = DB::update('redes_item')->set(array('status' => '0'))->where('autorizacion_id', '=', $aAautorizacion->id);
        $query->execute();

        foreach ($fUser->accounts->data as $value)  
        {

            $oItem = ORM::factory("Redes_Item")
                    ->where("uId","=",$value->id)
                    ->where("autorizacion_id","=",$aAautorizacion->id)
                    ->find();
            
            if($oItem->loaded())
            {
                $oItem->username = $value->name;
                $oItem->status = 1;
                $oItem->save();
            }else
            {
                $aItem = ORM::factory("Redes_Item");
                $aItem->uId = $value->id;
                $aItem->username = $value->name;
                $aItem->create_at = date("Y-m-d H:l:s");
                $aItem->autorizacion_id = $aAautorizacion->id;
                $aItem->save();
            }
        }
        
        $this->redirect("/dashboard/editarautorizacion");
    }
}