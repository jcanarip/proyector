<?php defined('SYSPATH') or die('No direct access allowed.');

require_once Kohana::find_file("vendor","facebook/src/facebook/autoload"); 

class Controller_Dashboard_Dashboard extends Controller_Dashboard_Template
{
    public $title = "CMS :: DASHBOARDS"; 
    
    public function action_index()
    {

        $this->title = "CMS :: DASHBOARDS";

        $this->template->styles= array(
            URL::base(TRUE)."media/lib/data-tables/media/css/dataTables.bootstrap.css",
        );

        $this->template->scripts= array(
            URL::base(TRUE)."media/lib/data-tables/media/js/jquery.dataTables.js",
            URL::base(TRUE)."media/lib/data-tables/media/js/dataTables.bootstrap.js",
            URL::base(TRUE)."media/js/dashboard.dashboard.index.js",
        );

        $content = View::factory('Dashboard/Dashboard/index');
        $this->template->content = $content;
    }
    
    public function action_list()
    {
        $oUser = $this->oUser;
        $reportes = ORM::factory('Redes_Reporte')
                    ->where("user_id","=",$oUser->id)
                    ->where("status","=",1);

        $paginate = Paginate::factory($reportes);

        $datatables = DataTables::factory($paginate)
            ->request($this->request)
            ->view('Dashboard/Dashboard/list')
            ->execute()
            ->render();
        
        $this->response->body($datatables);
    }
    
    
    public function action_crear()
    {
        if ($this->request->param('id')) $this->request->redirect('dashboard/dashboard/crear');
        $this->action_editar(NULL);
    }
    
    public function action_editar($editar = TRUE)
    {
        if($editar)
        {
            $this->template->scripts= array(
                URL::base(TRUE)."media/js/dashboard.dashboard.editar.js",
            );
            
           if (!$id = $this->request->param('id')) $this->redirect('dashboard/'); 
        }else{
            $this->template->scripts= array(
                URL::base(TRUE)."media/js/dashboard.dashboard.crear.js",
            );
            $id = NULL;
        }
        
        $oReporte = ORM::factory('Redes_Reporte')
                    ->where("id","=",$id)
                    ->where("user_id","=",$this->oUser->id)
                    ->find();


        $query = DB::select("redes_autorizacion.social_id,redes_social.*")->from('redes_social')
                ->join('redes_autorizacion')->on('redes_social.id','=','redes_autorizacion.social_id')
                ->where("redes_autorizacion.user_id","=",$this->oUser->id)
                ->where("redes_autorizacion.status","=",1)
                ->distinct('redes_autorizacion.social_id')->as_object();
        
        $aSocial =  $query->execute();
        
        $aCuentas = null;
        
        if(count($aSocial) == 1)
        {

            $aCuentas = ORM::factory('Redes_Autorizacion')
                    ->where("social_id","=",$aSocial[0]->id)
                    ->where("user_id","=",$this->oUser->id)
                    ->find_all();
        }

        if($this->request->method() == 'POST')
        {
            if($oReporte->loaded())
            {
               $oReporte->update_at = date('Y-m-d H:i:s'); 
            }else{
               $oReporte->created_at = date('Y-m-d H:i:s');  
            }

           $oReporte->descripcion = $this->request->post("descripcion") ;
           $oReporte->tipo = $this->request->post("tipo") ;
           $oReporte->plantilla = $this->request->post("plantilla") ;
           $oReporte->periodo = $this->request->post("periodo") ;
           $oReporte->user_id = $this->oUser->id ;
           $oReporte->save();
            
           $aItem = $this->request->post("item");
           
           foreach ($aItem as $value) 
           {
              $oReportedetalle = ORM::factory('Redes_Reportedetalle');
              $oReportedetalle->reporte_id = $oReporte->id;
              $oReportedetalle->item_id = $value;
              $oReportedetalle->created_at =  date('Y-m-d H:i:s'); 
              $oReportedetalle->save();
           }
           
           $this->redirect('dashboard/'); 
        }
        
        $this->title = "CMS :: DASHBOARDS - EDITAR ".strtoupper($oReporte->descripcion);

        $this->template->content = View::factory('Dashboard/Dashboard/editar')
                                    ->set(compact("oReporte","aSocial","aCuentas"));
    }
    
    public function action_getCuentasBySocial()
    {
        $aCuentas = ORM::factory('Redes_Autorizacion')
                ->where("social_id","=",$aSocial[0]->id)
                ->where("user_id","=",$this->oUser->id)
                ->find_all();
        

        $this->response->body(View::factory('Dashboard/Dashboard/getcuentasbysocial')->set(compact("aCuentas")));
       
    }
    
    public function action_getItemsByCuenta()
    {
        $id = $this->request->post("id");
        
        $aItems = ORM::factory('Redes_Item')
        ->where("autorizacion_id","=",$id)
        ->where("status","=",1)
        ->find_all();
        
        if(count($aItems) > 0)
        {
            $this->response->body(View::factory('Dashboard/Dashboard/getitembycuenta')->set(compact("aItems")));
        }else{
            Ajax::error("No existen item para esta cuenta");
        }
        

    }
    
    public function action_eliminar()
    {
        $oReporte = ORM::factory('Redes_Reporte')
                ->where("id","=",$this->request->param('id'))
                ->where("user_id","=",$this->oUser->id);

        if ( ! $oReporte->loaded()) $this->redirect('dashboard/');  

        $status = $oReporte->status;

        $oReporte->status = ($status == '1') ? '0' : '1';
        $oReporte->update_at = date('Y-m-d H:i:s');
        $oReporte->save();
        Message::success("Eliminado exitosamente!");
        //if ($status == 'blo' AND $oUser->status == 'cre') $this->active_user($oUser);

        $this->redirect('dashboard/'); 
    }
    
    public function action_view()
    {
        if (!$id = $this->request->param('id')) $this->redirect('dashboard/');
        
        $oReporte = ORM::factory('Redes_Reporte')
                 ->where("id","=",$id)
                 ->find();
        
        $aItem = $oReporte->aItem->find_all();
        
        $oAutorizacion = $aItem[0]->oAutorizacion->find();
        
        $this->title = "CMS :: DASHBOARDS - ".strtoupper($oReporte->descripcion);
        
        $content = View::factory('Dashboard/Dashboard/view');
        $this->template->content = $content;
    }
}