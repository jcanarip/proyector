<?php defined('SYSPATH') or die('No direct access allowed.');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_Redes_Social extends ORM
{
    protected $_table_name = 'redes_social';
    protected $_has_many = array(
        'aAutorizacion' =>  array(
            'model' => 'Redes_Autorizacion',
            'foreign_key'	=> 'social_id',
        ),
        'aCampania' => array(
            'model'	=> 'Rep_Campania',
            'foreign_key'	=> 'social_id',
        ),
    );
}
