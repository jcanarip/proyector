<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_User_Historial extends ORM {

	protected $_table_name = 'user_historial';
	
	protected $_belongs_to = array(
        'oUser' => array(
            'model' => 'User_User',
            'foreign_key'	=> 'user_id',
        ),
        
    );

}