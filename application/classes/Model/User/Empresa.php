<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_User_Empresa extends ORM
{
    protected $_table_name = "user_empresa";
    
    protected $_has_many = array(
        'aUser' => array(
            'model'   => 'User_Empresa',
            'through' => 'user_userempresa',
            'foreign_key' => 'user_empresa_id',
            'far_key' => 'user_user_id',  
        ),
        'aReporte' => array(
            'model'	=> 'Redes_Reporte',
            'foreign_key'	=> 'empresa_id',
        ),
        'aCampania' => array(
            'model'	=> 'Rep_Campania',
            'foreign_key'	=> 'empresa_id',
        ),
    );
    
    protected $_belongs_to = array(
        'oUbigeo' => array(
            'model' => 'Rep_Ubigeo',
            'foreign_key'	=> 'ubigeo_id',
        ),
        'oDocumenttype' => array(
            'model' => 'Rep_Documenttypes',
            'foreign_key'	=> 'documenttype_id',
        ),
    );
}

