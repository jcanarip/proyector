<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_User_User extends ORM
{
    protected $_table_name = 'user_user';
    
    protected $_belongs_to = array(
        'oProfile' => array(
            'model'		=> 'User_Profile',
            'foreign_key'	=> 'profile_id',
        ),
        'oUbigeo' => array(
            'model'		=> 'Rep_Ubigeo',
            'foreign_key'	=> 'ubigeo_id',
        ),
    );
    
    protected $_has_many = array(
        'aToken' => array(
            'model'	=> 'User_Token',
            'foreign_key'	=> 'user_id',
        ),
        'aHistorial' => array(
            'model'	=> 'User_Historial',
            'foreign_key'	=> 'user_id',
        ),
        'aReporte' =>  array(
            'model' => 'Rep_Reporte',
            'foreign_key'	=> 'user_id',
        ),
        'aAutorizacion' =>  array(
            'model' => 'Redes_Autorizacion',
            'foreign_key'	=> 'user_id',
        ),
        'aEmpresa' => array(
            'model'   => 'User_Empresa',
            'through' => 'user_userempresa',
            'foreign_key' => 'user_user_id',
            'far_key' => 'user_empresa_id', 
        ),
        'aCampania' => array(
            'model'	=> 'Rep_Campania',
            'foreign_key'	=> 'user_id',
        ),
    );
    
    
    public static function unique_email_profile($email)
    {
            $oUser = Auth::instance()->get_user();
            $aux = ORM::factory('User_User')
                    ->where('email',"=", $email)
                    ->find();
                    
            return $aux->id == $oUser->id;
    }
    
    public static function unique_email_create($email)
    {
            if (!$id = Request::current()->param('id')) $id = null;
            
            $aux = ORM::factory('User_User')
                    ->where('email',"=", $email)
                    ->find();
                    
            return $aux->id == $id;
    }
}