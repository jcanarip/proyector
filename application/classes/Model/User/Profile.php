<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_User_Profile extends ORM {

	protected $_table_name = 'user_profile';
	
	protected $_has_many = array(
            'aUser' => array(
                'model'		=> 'User_User',
                'foreign_key'	=> 'profile_id',
            ),

	);

}