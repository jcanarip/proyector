<?php defined('SYSPATH') or die('No direct script access.');

class Util {
        
    public static function centralizandoList($lista)
    {
        $group = array();
        $listafinal = array();

        foreach ($lista as $aux) 
        {
            foreach ($aux as $value) 
            {
                $group[$value['id']][] = $value;
            } 
        }

        foreach ($group as $key => $value) 
        {
            $periodo = array();
            $periodo["columna1"] = "-";
            $periodo["columna2"] = "-";
            $periodo["columna3"] = "-";

            foreach ($value as $j => $reg) 
            {
               $periodo["columna".($j+1)] = $reg["cantidad"];
               $periodo["texto"] = $reg["texto"];
            } 

            $periodo["id"] = $key;

            array_push($listafinal, $periodo);
        }

        return array_slice($listafinal,0,12);;
    }

    public static function orderbylistandlimit12($lista)
    {
        $group = array();
        $aux = array();

        foreach ( $lista as $value ) 
        {
            $group[$value['id']][] = $value;
        } 

        foreach ($group as $key => $value) 
        {
            $contador = 0;
            $texto = "";
            foreach ($value as $reg) 
            {
               $contador+= $reg["cantidad"];
               $texto = $reg["texto"];
            } 

            array_push($aux, array(
                "id" => $key,
                "texto" => $texto,
                "cantidad" => $contador
            ));
        }

        $aux = array_slice(self::array_msort($aux,array('cantidad'=>SORT_DESC)),0,12);

        return $aux;     
    }

    public static function array_msort($array, $cols)
    {
        $colarr = array();
        foreach ($cols as $col => $order) {
            $colarr[$col] = array();
            foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
        }
        $eval = 'array_multisort(';
        foreach ($cols as $col => $order) {
            $eval .= '$colarr[\''.$col.'\'],'.$order.',';
        }
        $eval = substr($eval,0,-1).');';
        eval($eval);
        $ret = array();
        foreach ($colarr as $col => $arr) {
            foreach ($arr as $k => $v) {
                $k = substr($k,1);
                if (!isset($ret[$k])) $ret[$k] = $array[$k];
                $ret[$k][$col] = $array[$k][$col];
            }
        }
        return $ret;

    }

    public static function num_format($num)
    {
             return number_format($num, 2, '.', '');
    }

    public static function date_format($date)
    {
             return $date ? date('d M Y @ g.ia', strtotime($date)) : NULL;
    }

    public static function is_month($month = NULL)
    {		
            if (is_null($month)) return Util::is_month(date('F', strtotime(date('Y-m-d H:i:s'))));

            return Arr::get(Util::months(), $month);
    }

    public static function nuevo_formato($fecha = NULL)
    {
        if(is_null($fecha))
        {
            return date("d F Y");
        }


        $dia=date("d", strtotime($fecha));

        if ($dia=="Monday") $dia="Lunes";
        if ($dia=="Tuesday") $dia="Martes";
        if ($dia=="Wednesday") $dia="Miércoles";
        if ($dia=="Thursday") $dia="Jueves";
        if ($dia=="Friday") $dia="Viernes";
        if ($dia=="Saturday") $dia="Sabado";
        if ($dia=="Sunday") $dia="Domingo";

        $mes=date("F", strtotime($fecha));

        if ($mes=="January") $mes="Enero";
        if ($mes=="February") $mes="Febrero";
        if ($mes=="March") $mes="Marzo";
        if ($mes=="April") $mes="Abril";
        if ($mes=="May") $mes="Mayo";
        if ($mes=="June") $mes="Junio";
        if ($mes=="July") $mes="Julio";
        if ($mes=="August") $mes="Agosto";
        if ($mes=="September") $mes="Setiembre";
        if ($mes=="October") $mes="Octubre";
        if ($mes=="November") $mes="Noviembre";
        if ($mes=="December") $mes="Diciembre";

        $anio=date("Y", strtotime($fecha));

        return "$dia de $mes del $anio";
    }

    public static function months()
    {
            return array(
                    'January'	=>  'Enero',
                    'February'	=>  'Febrero',
                    'March'		=>  'Marzo',
                    'April'		=>  'Abril',
                    'May'		=>  'Mayo',
                    'June'		=>  'Junio',
                    'July'		=>  'Julio',
                    'August'	=>  'Agosto',
                    'September'	=>  'Setiembre',
                    'October'	=>  'Octubre',
                    'November'	=>  'Noviembre',
                    'December'	=>  'Diciembre',
            );
    }

    public static function year_now()
    {
            return (int)date('Y');
    }

    public static function parse_signed_request($signed_request, $secret)
    {
            list($encoded_sig, $payload) = explode('.', $signed_request, 2); 

            // decode the data
            $sig = Util::base64_url_decode($encoded_sig);
            $data = json_decode(Util::base64_url_decode($payload), true);

            if (strtoupper($data['algorithm']) !== 'HMAC-SHA256')
            {
                    error_log('Unknown algorithm. Expected HMAC-SHA256');
                    return NULL;
            }

            // check sig
            $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);

            if ($sig !== $expected_sig)
            {
                    error_log('Bad Signed JSON signature!');
                    return NULL;
            }

            return $data;
    }

    public static function base64_url_decode($input)
    {
            return base64_decode(strtr($input, '-_', '+/'));
    }

    public static function valid_date($values, $names)
    {
            foreach ($names as $name) 
            {
                    if ((trim(Arr::get($values, $name))) == NULL) return FALSE;
            }

            return TRUE;
    }

    public static function date_stand($date)
    {
            return date('d', strtotime($date)) . ' de ' . Util::is_month(date('F', strtotime($date))) . ' del ' . date('Y', strtotime($date));
    }

    public static function unique($model, $attribute, $value, $id)
    {
            $val = URL::title($value, '-', TRUE);

            if (URL::title(ORM::factory($model, $id)->$attribute, '-', TRUE) == $val) return TRUE;

            foreach (ORM::factory($model)->find_all()->as_array($attribute) as $key => $name)
            {
                    if (URL::title($key, '-', TRUE) ==  $val) return FALSE;
            }

            return TRUE;
    }
}
