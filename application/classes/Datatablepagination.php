<?php defined('SYSPATH') or die('No direct script access.');

 class Datatablepagination {
	
	public $sLimit;
	public $sIndexColumn;
	public $aColumns;
	public $sOffset;
	public $sOrdert;
	public $sDirection;
	public $sWhere;
	public $iTotal;
	public $iFilteredTotal;
	public $sObjeto;
	public $sModelo;
	public $source;
	public $table;
	
	public function __construct($table,$modelo,$columnas = array())
	{
		$this->table = $table;
		$this->sModelo = $modelo;
		$this->aColumns = $columnas;
		//$this->sDirection = "DESC";

	}
	
	public function inicio() 
	{
	//$this->aColumns = $columnas;
	
	/* Indexed column (used for fast and accurate table cardinality) */
	//$sIndexColumn = $indice;

	/* 
	 * Paging
	 */

	if ( Request::$initial->query('iDisplayLength') != '-1' )
	{
		$this->sOffset = intval(Request::$initial->query('iDisplayStart'));
		$this->sLimit = intval(Request::$initial->query('iDisplayLength'));
	}
	

//	echo $this->sLimit ."---".$this->sOffset;

	/*
	 * Ordering
	 */
	
	$this->sOrdert = array();

	if (Request::$initial->query('iSortCol_0') > -1)
	{
		
		for ( $i=0 ; $i<intval( Request::$initial->query('iSortingCols') ) ; $i++ )
		{
			
			if ( Request::$initial->query( 'bSortable_'.(Request::$initial->query('iSortCol_'.$i)) ) == "true" )
			{
				
				$this->sOrdert[] = ($this->aColumns[ intval( Request::$initial->query('iSortCol_'.$i) )] == null)? "id" : $this->aColumns[ intval( Request::$initial->query('iSortCol_'.$i) )];
				$this->sDirection = (Request::$initial->query('sSortDir_'.$i)==='asc' ? 'ASC' : 'DESC');
			}
		}
		

		
		/*$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}*/
	}
	
	/* 
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */

	if ( Request::$initial->query('sSearch') != "" )
	{
		$this->sWhere = " (";
		for ( $i=0 ; $i<count($this->aColumns) ; $i++ )
		{
			if($this->aColumns[$i] != null)
			{
				$this->sWhere .= "`".$this->aColumns[$i]."` LIKE '%".( Request::$initial->query('sSearch') )."%' OR ";
			}
			

		}
		$this->sWhere = substr_replace( $this->sWhere, "", -3 );
		$this->sWhere .= ')';
	}
	
	/* Individual column filtering */
	for ( $i=0 ; $i<count($this->aColumns) ; $i++ )
	{
		if (  
				Request::$initial->query('bSearchable_'.$i) == "true" && 
				Request::$initial->query('sSearch_'.$i) != '' )
		{

			$this->sWhere .= " AND ";
			$this->sWhere .= "`".$this->aColumns[$i]."` LIKE '%".(Request::$initial->query('sSearch_'.$i))."%' ";
		}
	}
	
	if(!$this->sWhere)
	{
		$this->sWhere = " 1 = 1";
	}


	}
	
	public function datasourde()
	{
		
		$sOutput = '{';
		$sOutput .= '"sEcho": '.intval(Request::$initial->query('sEcho')).', ';
		$sOutput .= '"iTotalRecords": '.$this->iTotal.', ';
		$sOutput .= '"iTotalDisplayRecords": '.$this->iFilteredTotal.', ';
		$sOutput .= '"aaData": [ ';

		$sOutput .= $this->source;

		$sOutput = substr_replace( $sOutput, "", -1 );
		$sOutput .= '] }';

		echo $sOutput;
	}
	
	public function datadb()
	{
		$this->iTotal = ORM::factory($this->sModelo)->count_all();

		$this->aObjeto = DB::select(DB::expr('SQL_CALC_FOUND_ROWS *'))
		->from($this->table)
		->where(DB::expr('1=1') , 'AND', DB::expr($this->sWhere))
		->order_by($this->sOrdert, $this->sDirection)
		->offset($this->sOffset)
		->limit($this->sLimit)
		->as_object('model_'.$this->sModelo)
		->execute();

		$this->iFilteredTotal  = DB::select(array(DB::expr('FOUND_ROWS()'), 'total'))->execute()->get('total'); 

		return $this->aObjeto;
	}
	
	
}
?>