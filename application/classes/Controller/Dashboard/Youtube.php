<?php defined('SYSPATH') or die('No direct access allowed.');

require_once Kohana::find_file("vendor","google/src/Google/autoload"); 

class Controller_Dashboard_Youtube extends Controller_Dashboard_Template
{
    public function action_index()
    {
        $client = new Google_Client();
        $client->setClientId($this->yt_app_key);
        $client->setClientSecret($this->yt_app_secret);
        $urlyt = filter_var($this->urlredireccionapis(),FILTER_SANITIZE_URL);
        $client->addScope(Google_Service_YouTubeAnalytics::YOUTUBE_READONLY);
        $client->addScope(Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
        $client->setRedirectUri($urlyt);
        $client->setAccessType('offline');
        $client->setApprovalPrompt("force");
        
        $state = mt_rand();
        $client->setState($state);
        Session::instance()->set('state',$state);
        $this->redirect($client->createAuthUrl());
    }
    
    public function action_setdata()
    {
        if($this->request->query("error"))
        {
            $this->redirect("/dashboard/home/editarautorizacion/?error=error");
        }
        
        $id = $this->request->param('id');
        $oUser =  $this->oUser;
        
        $client = new Google_Client();
        $client->setClientId($this->yt_app_key);
        $client->setClientSecret($this->yt_app_secret);
        $urlyt = filter_var($this->urlredireccionapis(),FILTER_SANITIZE_URL);
        $client->addScope(Google_Service_YouTubeAnalytics::YOUTUBE_READONLY);
        $client->addScope(Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
        $client->setRedirectUri($urlyt);
        $client->setAccessType('offline');
        $client->setApprovalPrompt("force");
        $client->authenticate($this->request->query("code"));
        $token = json_decode($client->getAccessToken(), true) ;
        $token["app_key"] = $this->yt_app_key;
        $token["app_secret"] = $this->yt_app_secret;
        
        $youtube = new Google_Service_YouTube($client);
        $channelsResponse = $youtube->channels->listChannels('id,snippet,brandingSettings,contentDetails', array('mine' => 'true',));
        $channel = $channelsResponse["items"][0];

        $aAautorizacion = ORM::factory("Redes_Autorizacion")
                            ->where("social_id","=",$id)
                            ->where("user_id","=",$oUser->id)
                            ->where("uId","=",$channel["contentDetails"]["googlePlusUserId"])
                            ->find();
        
        if(!$aAautorizacion->loaded())
        {
            $aAautorizacion->social_id = $id;
            $aAautorizacion->user_id = $oUser->id;
            $aAautorizacion->access = json_encode($token) ;
            $aAautorizacion->created_at = date("Y-m-d H:i:s");
            $aAautorizacion->uId = $channel["contentDetails"]["googlePlusUserId"];
        }
        
        $aAautorizacion->username = $channel["snippet"]["title"];  
        $aAautorizacion->save();
       
        $query = DB::update('redes_item')
                ->set(array('status' => '0'))
                ->where('autorizacion_id', '=', $aAautorizacion->id);
        $query->execute();
        
        $oItem = ORM::factory("Redes_Item")
                ->where("uId","=",$channel["id"])
                ->where("autorizacion_id","=",$aAautorizacion->id)
                ->find();

        if($oItem->loaded())
        {
            $oItem->status = 1;
            $oItem->update_at = date("Y-m-d H:i:s");
        }else
        {
            $oItem->uId =$channel["id"];
            $oItem->autorizacion_id = $aAautorizacion->id;
            $oItem->created_at = date("Y-m-d H:i:s");
        }
        
        $oItem->descripcion = $channel["snippet"]["title"];
        $oItem->save();
        
        $this->redirectbyprofile();
    }
    
    public static function getdata($item,$accesos, $fecha_inicio, $fecha_fin, $funcion) 
    {
        $access_token = json_decode($accesos, true);
        
        $client = new Google_Client();
        $client->setClientId($access_token["app_key"]);
        $client->setClientSecret($access_token["app_secret"]);
        
        $client->addScope(Google_Service_YouTubeAnalytics::YOUTUBE_READONLY);
        $client->addScope(Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
        $client->setAccessType('offline');
        $client->setApprovalPrompt("force");
        $client->setAccessToken($accesos);
        
         if($client->isAccessTokenExpired()) 
        {
            $newToken = json_decode($client->getAccessToken());
            $client->refreshToken($newToken->refresh_token);

            $accesos = json_decode($client->getAccessToken(),true);
            $accesos["app_key"] = $access_token["app_key"];
            $accesos["app_secret"] = $access_token["app_secret"];
            $access_token = $accesos;
            
            $aux = ORM::factory("Redes_Autorizacion",$item->oAutorizacion->id); 
            $aux->access = json_encode($accesos);
            $aux->save();
        }
        
        $youtube = new Google_Service_YouTube($client);
        $youtubeAnalytics = new Google_Service_YouTubeAnalytics($client);

        $channelsResponse = $youtube->channels->listChannels('id,snippet,brandingSettings,contentDetails', array('id' => $item->uId));
        $channel = $channelsResponse["items"][0];
        $channelId = $channel["id"];

        $fecha_inicio = date("Y-m-d",$fecha_inicio);
        $fecha_fin = date("Y-m-d",$fecha_fin);
        
        /* $analyticsResponse_total = $youtubeAnalytics->reports->query(
            'channel=='.$channelId,
            $fecha_inicio,
            $fecha_fin,
            "subscribersGained,subscribersLost"
        );
         
         echo Debug::vars($analyticsResponse_total);
         die();*/
        
        try{
        
            return self::$funcion($channelId,$fecha_inicio,$fecha_fin,$youtubeAnalytics);
        } catch (Exception $exc) {
            Ajax::error($exc->getTraceAsString());
        }

    }
    
    public static function subscriptores($channelId,$fecha_inicio,$fecha_fin,$youtubeAnalytics)
    {
        $analyticsResponse_total = $youtubeAnalytics->reports->query(
            'channel=='.$channelId,
            $fecha_inicio,
            $fecha_fin,
            "subscribersGained,subscribersLost"
        );
        
        return $analyticsResponse_total->rows[0][0] - $analyticsResponse_total->rows[0][1]; 
    }
    
    public static function views($channelId,$fecha_inicio,$fecha_fin,$youtubeAnalytics)
    {
        $analyticsResponse = $youtubeAnalytics->reports->query(
            'channel=='.$channelId,
            $fecha_inicio,
            $fecha_fin,
            "views"
        );
        
        return (is_null($analyticsResponse->rows[0][0]))? 0 : $analyticsResponse->rows[0][0];
    }
    
    public static function estimatedMinutesWatched($channelId,$fecha_inicio,$fecha_fin,$youtubeAnalytics)
    {
        $analyticsResponse = $youtubeAnalytics->reports->query(
            'channel=='.$channelId,
            $fecha_inicio,
            $fecha_fin,
            "estimatedMinutesWatched"
        );
        
        return gmdate("H:i:s", $analyticsResponse->rows[0][0]*60);
    }
    
    public static function likes($channelId,$fecha_inicio,$fecha_fin,$youtubeAnalytics)
    {
        $analyticsResponse = $youtubeAnalytics->reports->query(
            'channel=='.$channelId,
            $fecha_inicio,
            $fecha_fin,
            "likes"
        );
        
        return (is_null($analyticsResponse->rows[0][0]))? 0 : $analyticsResponse->rows[0][0];
    }
    
    public static function dislikes($channelId,$fecha_inicio,$fecha_fin,$youtubeAnalytics)
    {
        $analyticsResponse = $youtubeAnalytics->reports->query(
            'channel=='.$channelId,
            $fecha_inicio,
            $fecha_fin,
            "dislikes"
        );
        
        return (is_null($analyticsResponse->rows[0][0]))? 0 : $analyticsResponse->rows[0][0];
    }
    
    public static function shares($channelId,$fecha_inicio,$fecha_fin,$youtubeAnalytics)
    {
        $analyticsResponse = $youtubeAnalytics->reports->query(
            'channel=='.$channelId,
            $fecha_inicio,
            $fecha_fin,
            "shares"
        );
        
        return (is_null($analyticsResponse->rows[0][0]))? 0 : $analyticsResponse->rows[0][0];
    }
    
    public static function comments($channelId,$fecha_inicio,$fecha_fin,$youtubeAnalytics)
    {
       $analyticsResponse = $youtubeAnalytics->reports->query(
            'channel=='.$channelId,
            $fecha_inicio,
            $fecha_fin,
            "comments"
        );
        
        return (is_null($analyticsResponse->rows[0][0]))? 0 : $analyticsResponse->rows[0][0];
    }
}