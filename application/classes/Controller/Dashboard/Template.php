<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Template extends Controller_Reporte {
   
    public $template = "Dashboard/template";
    public $title ="";
    
    public $tw_app_key = 'eA9tK1qWQJ40mMKlo8aGt94mB';
    public $tw_app_secret = 'XttlQXgLMd4NfvWK0jeR9JC1Yhu9YTm9EsNZhHY3eP7fCxPTYc';
    public $fb_app_key = "1466475013659678";
    public $fb_app_secret = "b0ed2a12618462274d2d40ea13ac0c4b";
    public $fb_access_token = "CAACEdEose0cBAA5frPxw0txTOHjtbGhJhEXANjXRGQ8tLUS3yZAedZAfZAlpaRFZBC7d6Epx6jvLZClSV25ApTWZAkP26ZAoH3fgXF6yvjNhxQCwuZCtwlvv0lZB3iUbzENKkjPFTZC0HZCKfnC1fxMMbST0lsXK8WPFqLZBPWo7zvT51PgYbOZBGtID53VUk28lgj0f9eKIZB69R9rj59SDJMEbM9";
    public $yt_app_key = '726896809443-hraqevr5427n0kn74b4t2sej7ga6touj.apps.googleusercontent.com';
    public $yt_app_secret = '9-ZjVmuAEJlQ1XToul5LqxpB';
    public $an_app_key = "805192384748-e9m4a3sa4i813p43u6vc42njl3st9e67.apps.googleusercontent.com";
    public $an_app_secret = "Uhw-sdpO0OUKPlVxvbw7QkWU";
    public $in_app_key = "4048b468a2284575b4a3380aa9699a0d";
    public $in_app_secret = "54c9d123201b4482bbc93c9f09675f5c";
    public $tw_request_token = null;
    public $twitter = null;
    public $facebook = null;
    public $youtube = null;
  
    
    public function before()
    {  

        parent::before(); 

        if ($this->auto_render)
        {
            
            $margin = "";
            $menu = View::factory('Dashboard/menu');
            if($this->oUser)
            {
                if($this->oUser->profile_id == 3)
                {
                    $margin = "style='margin:0px'";
                    $menu = null;
                }else{
                    $seleccion = strtolower($this->request->controller()."/".$this->request->action());
                    $menu->set('oUser', $this->oUser)->set('seleccion',$seleccion);
                }
            }else{
                $margin = "style='margin:0px'";
                $menu = null;
            }

            View::set_global('margin', $margin);
            
            $this->template->title = $this->title;
            $this->template->scripts = array();
            $this->template->styles = array(); 
            $this->template->menu = $menu;
            $this->template->cabecera = View::factory('Dashboard/cabecera')
                                        ->set('oUser', $this->oUser);
        }
    } 
    
    public function redirectbyprofile()
    {
        if($this->oUser->pasos == 1)
        {
            $this->redirect("/dashboard/home/paso2/");
        }
        else
        {
            $this->redirect("/dashboard/home/editarautorizacion/");
        }
    }
    
    public function urlredireccionapis($id = null)
    {
        $id = ($id)? $id : $this->request->param('id');
        return  URL::base(TRUE)."dashboard/home/complete/".$id;
    }

    public function after()
    {
        parent::after();
    }

}

?>