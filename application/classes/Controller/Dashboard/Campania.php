<?php defined('SYSPATH') or die('No direct access allowed.');

require_once Kohana::find_file("vendor","google/src/Google/autoload"); 
require_once Kohana::find_file("vendor","twitter/TwitterOAuth"); 

class Controller_Dashboard_Campania extends Controller_Dashboard_Template
{
    
    public function action_index()
    {
       $this->title = "CMS :: CAMPAÑAS";

       $this->template->styles= array(
           URL::base(TRUE)."media/lib/data-tables/media/css/dataTables.bootstrap.css",
       );

       $this->template->scripts= array(
           URL::base(TRUE)."media/lib/data-tables/media/js/jquery.dataTables.js",
           URL::base(TRUE)."media/lib/data-tables/media/js/dataTables.bootstrap.js",
           URL::base(TRUE)."media/js/dashboard.campania.index.js",
       );

       $this->template->content = View::factory('Dashboard/Campania/index');

    }

   public function action_list()
   {

       $empresa = ORM::factory('Rep_Campania');

       $paginate = Paginate::factory($empresa)
           ->columns(array('id','descripcion','status'));

       $datatables = DataTables::factory($paginate)
           ->request($this->request)
           ->view('Dashboard/Campania/list')
           ->execute()
           ->render();

       $this->response->body($datatables);
   }
   
    public function action_crear()
    {
        if ($this->request->param('id')) $this->request->redirect('dashboard/campania/crear');
        $this->action_editar(NULL);
    }
    
    public function action_editar($editar = TRUE)
    {
        $this->template->scripts = array(
            URL::base(TRUE)."media/lib/moment-with-locales.js",
            URL::base(TRUE)."media/lib/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js",
            URL::base(TRUE) . "media/js/dashboard.campania.editar.js"
        );
        
        $this->template->styles= array(
            URL::base(TRUE)."media/lib/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css"
        );
        
        if ($editar) {
            if (!$id = $this->request->param('id'))
                $this->redirect('dashboard/campania/');
        } else {
            $id                      = NULL;
        }
        
        $oUser    = $this->oUser;
        $aListadEmpresas = $oUser->aEmpresa->find_all();

        $query    = DB::select("redes_autorizacion.social_id,redes_social.*")
                    ->from('redes_social')
                    ->join('redes_autorizacion')->on('redes_social.id', '=', 'redes_autorizacion.social_id')
                    ->where("redes_autorizacion.user_id", "=", $this->oUser->id)
                    ->where("redes_autorizacion.status", "=", 1)
                    ->distinct('redes_autorizacion.social_id')
                    ->as_object();
        $aSocial  = $query->execute();
        
        $aCuentas = array();
        $aItem =  array();
        $aIndicador = array();
        $aObjeto = array();
        $oCampania = ORM::factory("Rep_Campania",$id);

        if($oCampania->loaded())
        {
            $this->title = "CMS :: EMPRESA - EDITAR";
            $aCuentas =  ORM::factory("Redes_Autorizacion")->where("user_id", "=", $this->oUser->id)->where("social_id", "=", $oCampania->social_id)->find_all();
            $aIndicador = $this->indicadorbysocial($oCampania->social_id);
            $aItem = ORM::factory("Redes_Item")->where("autorizacion_id", "=",  $oCampania->autorizacion_id)->find_all();
            
            
            $aObjeto = $this->objetobyautorizacion($oCampania->item_id, $oCampania->autorizacion_id ,$oCampania->social_id , $oCampania->indicador);
        }else{
            $this->title = "CMS :: EMPRESA - CREAR";
        }

       
        if ($this->request->method() == 'POST') {
            
            $crontab = new Ssh2CrontabManager('tribal121.com', '22', 'tribu121', 'rF,rO?InD;2z');
            
            if ($id) {
                //verificamos si la campaña esta activada
                $inicio_campania = $oCampania->inicio_campania;
                $fin_campania = $oCampania->fin_campania;
                $fecha_actual = date('Y-m-d H:i:s');
                
                if(strtotime($fecha_actual) > strtotime($inicio_campania))
                {
                    Message::danger("La campaña ya se inicio. Solo puede anularla");
                    $this->redirect('dashboard/campania/editar/'.$id);
                }else if(strtotime($fecha_actual) > strtotime($fin_campania))
                {
                    Message::danger("La campaña ya se termino. Solo puede anularla");
                    $this->redirect('dashboard/campania/editar/'.$id);
                }{
                   
                   $crontab->remove_cronjob($oCampania->cronjob_inicio);
                }
                
                $oCampania->update_at = date('Y-m-d H:i:s');
                
            } else {
                $oCampania->created_at = date('Y-m-d H:i:s');
            }
            
            $oCampania->descripcion = $this->request->post("descripcion");
            //$oCampania->notas = $this->request->post("notas");
            $oCampania->social_id = $this->request->post("social_id");
            $oCampania->autorizacion_id = $this->request->post("autorizacion_id");
            $oCampania->item_id = $this->request->post("item_id");
            $oCampania->objeto = $this->request->post("objeto");
            $oCampania->indicador = $this->request->post("indicador");
            $oCampania->inicio_campania = date("Y-m-d H:i:s", strtotime($this->request->post("inicio_campania")));
            $oCampania->fin_campania =  date("Y-m-d H:i:s", strtotime($this->request->post("fin_campania")));
            $oCampania->metrica = $this->request->post("metrica");
            $oCampania->empresa_id  = $this->request->post("empresa_id");
            $oCampania->user_id     = $this->oUser->id;
            $oCampania->save();

            $aux = date("Y-m-d H:i:s", strtotime($this->request->post("inicio_campania")));
            $dia = abs(date("d",  strtotime($aux)));
            $mes = abs(date("m",  strtotime($aux)));
            $anio = date("Y",  strtotime($aux));
            $horas = abs(date("H",  strtotime($aux)));
            $minutos = abs(date("i",  strtotime($aux)));
            
            $cronjob_inicio = $minutos." ".$horas." ".$dia." ".$mes." * curl ".URL::base(TRUE)."dashboard/campania/iniciarcronjob/".$oCampania->id."?user=".$this->oUser->id."&time=".time();
            $crontab->append_cronjob($cronjob_inicio);

            $cronjob = $minutos."* */2 * * * curl ".URL::base(TRUE)."dashboard/campania/cronjob/".$oCampania->id."?"
                    . "user=".$this->oUser->id."&time=".time()
                    ."&social_id=".$oCampania->social_id
                    ."&autorizacion_id=".$oCampania->autorizacion_id
                    ."&item_id=".$oCampania->item_id
                    ."&objeto=".$oCampania->objeto
                    ."&indicador=".$oCampania->indicador;
            
            $objuax = ORM::factory("Rep_Campania",$oCampania->id);
            $objuax->cronjob_inicio = $cronjob_inicio;
            $objuax->cronjob = $cronjob;
            $objuax->save();
            
            Message::success("Guardado exitosamente!");
            $this->redirect('dashboard/campania/');
        }

        
        $this->template->content = View::factory('Dashboard/Campania/editar')->set(compact("minutos","horas","dias","meses","oCampania", "aSocial", "aCuentas", "aItem", "diasdelasemana","aListadEmpresas","aIndicador","aObjeto"));

    }
    
    public function action_iniciarcronjob() 
    {
        $crontab = new Ssh2CrontabManager('tribal121.com', '22', 'tribu121', 'rF,rO?InD;2z');
        
        $id = $this->request->param('id');
       // $user = $this->request->query('user');
        
        $objuax = ORM::factory("Rep_Campania",$id);
        
        //se elimina el cronjob de inicio
        $crontab->remove_cronjob($objuax->cronjob_inicio);
        
        //se cera el cronjob de revision constante (2 hroas)
        $crontab->append_cronjob($objuax->cronjob);
        $this->template->content="";
    }
    
    public function action_cronjob() 
    {
        $crontab = new Ssh2CrontabManager('tribal121.com', '22', 'tribu121', 'rF,rO?InD;2z');
        
        $id = $this->request->param('id');
        $user = $this->request->query('user');
        $social_id = $this->request->query('social_id');
        $autorizacion_id = $this->request->query('autorizacion_id');
        $item_id = $this->request->query('item_id');
        $objeto = $this->request->query('objeto');
        $indicador = $this->request->query('indicador');

        $oAutorizacion = ORM::factory("Redes_Autorizacion")->where("id", "=", $autorizacion_id)->find();
       
        $oItem = ORM::factory("Redes_Item")->where("id", "=", $item_id)->find();
        
        $resultado = 0;

            switch ($indicador) {
                case 1:
                    $url = "https://graph.facebook.com/v2.4/".$objeto."/likes?limit=100&access_token=".$access_token["access_token"];
                    $resultado = json_decode(json_encode(Controller_Dashboard_Facebook::search_next($url)));

                break;
                case 2:
                    $url = "https://graph.facebook.com/v2.4/".$objeto."/comments?limit=100&access_token=".$access_token["access_token"];
                    $resultado = json_decode(json_encode(Controller_Dashboard_Facebook::search_next($url)));

                break;
                case 3:
                    $url = "https://graph.facebook.com/v2.4/".$objeto."/?fields=shares&limit=100&access_token=".$access_token["access_token"];
                    $resultado = json_decode(json_encode(Controller_Dashboard_Facebook::search_next($url)));

                break;
                case 4:
                    $url = "https://graph.facebook.com/v2.4/".$objeto."/?fields=likes&limit=100&access_token=".$access_token["access_token"];
                    $likes = json_decode(json_encode(Controller_Dashboard_Facebook::search_next($url)));

                    $url = "https://graph.facebook.com/v2.4/".$objeto."/?fields=comments&limit=100&access_token=".$access_token["access_token"];
                    $comments = json_decode(json_encode(Controller_Dashboard_Facebook::search_next($url)));

                    $url = "https://graph.facebook.com/v2.4/".$objeto."/?fields=shares&limit=100&access_token=".$access_token["access_token"];
                    $shares = json_decode(json_encode(Controller_Dashboard_Facebook::search_next($url)));

                    $resultado = $likes + ($comments*2) + ($shares*3);
                break;
                case 5:


                break;
                case 6:

                    $twitter = new TwitterOAuth($access_token['app_key'],$access_token['app_secret'],$access_token['oauth_token'],$access_token['oauth_token_secret']); 
                    $aux = $twitter->get("account/verify_credentials");
                    $resultado = $aux->followers_count;
                    
                break;
                case 7:
                    $twitter = new TwitterOAuth($access_token['app_key'],$access_token['app_secret'],$access_token['oauth_token'],$access_token['oauth_token_secret']); 
                    $parametros = array();
                    $parametros["exclude_replies"] = true;
                    $parametros["include_rts"] = false;
                    $parametros["count"] = 200;
                    $tweets = $twitter->get("statuses/mentions_timeline", $parametros);
                    $resultado =  $this->retweetsTotal($tweets,$parametros,$twitter);
                    
                break;
                case 8:

                    $client = new Google_Client();
                    $client->setClientId($access_token["app_key"]);
                    $client->setClientSecret($access_token["app_secret"]);

                    $client->addScope(Google_Service_YouTubeAnalytics::YOUTUBE_READONLY);
                    $client->addScope(Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
                    $client->setAccessType('offline');
                    $client->setApprovalPrompt("force");
                    $client->setAccessToken($oAutorizacion->access);

                    $youtube = new Google_Service_YouTube($client);
                    
                    $listResponse = $youtube->videos->listVideos("id,snippet,statistics",
                          array('id' => $objeto));
                    
                    $resultado = $listResponse["items"][0]["statistics"]["viewCount"];
                break;
                case 9:
                    $client = new Google_Client();
                    $client->setClientId($access_token["app_key"]);
                    $client->setClientSecret($access_token["app_secret"]);

                    $client->addScope(Google_Service_YouTubeAnalytics::YOUTUBE_READONLY);
                    $client->addScope(Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
                    $client->setAccessType('offline');
                    $client->setApprovalPrompt("force");
                    $client->setAccessToken($oAutorizacion->access);

                    $youtube = new Google_Service_YouTube($client);

                    $channelsResponse = $youtube->channels->listChannels('id,snippet,brandingSettings,contentDetails', array('id' => $oItem->uId));
                    $channel = $channelsResponse["items"][0];
                    $channelId = $channel["id"];
                    
                    $listResponse = $youtube->videos->listSubscriptions("id,contentDetails", array('channelId' => $channelId));
                    
                    $resultado = $listResponse["items"][0]["contentDetails"]["totalItemCount"];

                break;
                case 10:
                    $client = new Google_Client();
                    $client->setClientId($access_token["app_key"]);
                    $client->setClientSecret($access_token["app_secret"]);

                    $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
                    $client->setAccessType('offline');
                    $client->setApprovalPrompt("force");
                    $client->setAccessToken($oAutorizacion->access);

                    $igoogle_id = explode(".",$oItem->uId);
                    
                    $analytics = new Google_Service_Analytics($client);
                    $goal = $analytics->management_goals->get($igoogle_id[0],$igoogle_id[1],$igoogle_id[2], $objeto);
                    
                    $results = $analytics->data_ga->get(
                        'ga:' . $igoogle_id[2],
                        date("Y-m-d",  strtotime($goal["created"])),
                        'today',
                        'ga:goal'.$objeto.'Completions'
                    );

                    if (count($results->getRows()) > 0) 
                    {
                        $rows = $results->getRows();
                        $resultado = $rows[0][0];

                    } else {
                        $resultado = 0;
                    }

                break;
                case 11:

                    $client = new Google_Client();
                    $client->setClientId($access_token["app_key"]);
                    $client->setClientSecret($access_token["app_secret"]);

                    $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
                    $client->setAccessType('offline');
                    $client->setApprovalPrompt("force");
                    $client->setAccessToken($oAutorizacion->access);

                    $igoogle_id = explode(".",$oItem->uId);
                    
                    $analytics = new Google_Service_Analytics($client);
                    $profile = $analytics->management_profiles->get($igoogle_id[0],$igoogle_id[1],$igoogle_id[2]);
                    
                    $results = $analytics->data_ga->get(
                        'ga:' . $igoogle_id[2],
                         date("Y-m-d",  strtotime($profile["created"])),
                        'today',
                        'ga:organicSearches'
                    );

                    if (count($results->getRows()) > 0) 
                    {
                        $rows = $results->getRows();
                        $resultado = $rows[0][0];

                    } else {
                        $resultado = 0;
                    }

                break;
                default:
                break;
           }

        
       // 0	0	*	*	*	curl http://www.tribal121.com/proyectotribal121/cron/cron.php
        $objuax = ORM::factory("Rep_Campania",$id);
        
        $fin_campania = $objuax->fin_campania;
        $fecha_actual = date('Y-m-d H:i:s');

        
        if(($resultado >= $objuax->metrica) or (strtotime($fecha_actual) >= strtotime($fin_campania)))
        {
           $crontab->remove_cronjob($objuax->cronjob);
           $objuax->revision_campania =  date('Y-m-d H:i:s');
           $objuax->status = 2;
           $objuax->save();
        }
        
        $this->template->content="";

    }
    
    private function retweetsTotal($tweets,$parametros,$user)
    {
        $total = count($tweets);
        $inicio = 0;
        $suma = 0;

        foreach($tweets as $tweet)
        {
            $inicio ++;

            if(isset($tweet->retweeted_status))
            {
                $suma += (int) $tweet->retweet_count;
            }
            
            if($total == $inicio)
            {
                $parametros["max_id"] = $tweet->id_str;
                $tweets = $user->get("statuses/user_timeline", $parametros); 
                return $suma + (int) $this->retweetsTotal($tweets,$parametros,$user);  
            }
        }          
    }
    
    public function action_getcuentasbysocial()
    {
        if (!$id = $this->request->post('id')) {
            Ajax::error("Existen problemas con la red social seleccionada");
        }
        
        $aCuentas = ORM::factory('Redes_Autorizacion')->where("social_id", "=", $id)->where("status", "=", 1)->where("user_id", "=", $this->oUser->id)->find_all();
        
        $this->response->body(View::factory('Dashboard/Campania/getcuentasbysocial')->set(compact("aCuentas")));
    }
    
    public function action_getobjetobyindicador()
    {
        if (!$item_id = $this->request->post('item_id')) {
            Ajax::error("Existen problemas con el item seleccionada");
        }
        if (!$autorizacion_id = $this->request->post('autorizacion_id')) {
            Ajax::error("Existen problemas con la cuenta seleccionada");
        }
        if (!$social_id = $this->request->post('social_id')) {
            Ajax::error("Existen problemas con la red social seleccionada");
        }
        if (!$indicador = $this->request->post('indicador')) {
            Ajax::error("Existen problemas con indicadorseleccionada");
        }
        
        $aObjeto = $this->objetobyautorizacion($item_id, $autorizacion_id ,$social_id , $indicador);
        
        $this->response->body(View::factory('Dashboard/Campania/getobjetobyindicador')->set(compact("aObjeto")));
    }
    
    private function objetobyautorizacion($item_id = null, $autorizacion_id = null,$social_id = null, $indicador = null)
    {
        
       $oAutorizacion = ORM::factory("Redes_Autorizacion")->where("id", "=", $autorizacion_id)->find();
       
       $oItem = ORM::factory("Redes_Item")->where("id", "=", $item_id)->find();
               
       $posts = array();
       
       if($oAutorizacion->loaded())
       {
            $aIndicador = $this->indicadorbysocial($social_id);
            
            if(!array_key_exists($indicador, $aIndicador))
            {
               return array(); 
            }
            
            $oIndicador = $aIndicador[$indicador];

            $access_token = json_decode($oAutorizacion->access, true);

            if($social_id == 1)
            {
                if($oIndicador["tipo"] == 1)
                {
                   $url = "https://graph.facebook.com/v2.4/".$oItem->uId."/posts?limit=20&access_token=".$access_token["access_token"];
                   $lista = json_decode(file_get_contents($url));
                   
                   if(isset($lista->data))
                   {
                        foreach ($lista->data as $value) {
                            $texto = (isset($value->message)) ? $value->message : $value->story;
                           array_push($posts, array(
                               "id" => $value->id,
                               "descripcion" => strtr(($texto) , 0 , 50),
                          ));
                        }
                   }
                }
            }
            
            if($social_id == 3)
            {
                if($oIndicador["tipo"] == 1)
                {
                    $client = new Google_Client();
                    $client->setClientId($access_token["app_key"]);
                    $client->setClientSecret($access_token["app_secret"]);
                    $client->addScope(Google_Service_YouTubeAnalytics::YOUTUBE_READONLY);
                    $client->addScope(Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
                    $client->setAccessType('offline');
                    $client->setApprovalPrompt("force");
                    $client->setAccessToken($oAutorizacion->access);

                    $youtube = new Google_Service_YouTube($client);
                    $youtubeAnalytics = new Google_Service_YouTubeAnalytics($client);

                    $channelsResponse = $youtube->channels->listChannels('id,snippet,brandingSettings,contentDetails', array('id' => $oItem->uId));

                    $item = $channelsResponse["items"][0];

                    $uploadsListId = $item["contentDetails"]["relatedPlaylists"]["uploads"];
                    
                    $playlistResponse =  $youtube->playlistItems->listPlaylistItems('snippet', array(
                       'playlistId' => $uploadsListId,
                       'maxResults' => 20
                    ));
                     
                    foreach ($playlistResponse as $value) {
                        $listResponse = $youtube->videos->listVideos("id,snippet,statistics",
                          array('id' => $value["snippet"]["resourceId"]["videoId"]));

                       array_push($posts, array(
                            "id" => $value["snippet"]["resourceId"]["videoId"],
                            "descripcion" =>  ($listResponse["items"][0]["snippet"]["title"]) 
                      ));
                    }
                }
            }
            
            if($social_id == 4)
            {
                if($oIndicador["tipo"] == 1)
                {
                    $client = new Google_Client();
                    $client->setClientId($access_token["app_key"]);
                    $client->setClientSecret($access_token["app_secret"]);
                    $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
                    $client->setAccessType('offline');
                    $client->setApprovalPrompt("force");
                    $client->setAccessToken($oAutorizacion->access);

                    $igoogle_id = explode(".",$oItem->uId);
                    
                    $analytics = new Google_Service_Analytics($client);
                    $lista = $analytics->management_goals->listManagementGoals($igoogle_id[0],$igoogle_id[1],$igoogle_id[2], array(
                        "max-results" => 20
                    ));
                    
                    foreach ($lista as $value) {

                       array_push($posts, array(
                            "id" => $value["id"],
                            "descripcion" =>  ($value["name"]) 
                      ));
                    }
                }
            }
       }
       
       return $posts;
    }
    
    public function action_getindicadoresbysocial()
    {
        if (!$id = $this->request->post('id')) {
            Ajax::error("Existen problemas con la red social seleccionada");
        }
        
        $aIndicador = $this->indicadorbysocial($id);
        
        $this->response->body(View::factory('Dashboard/Campania/getindicadorbysocial')->set(compact("aIndicador")));
    }
    
    private function indicadorbysocial($id = null)
    {
        $lista = array(
                1 => array(
                        1 => array(
                            "id" => 1,
                            "descripcion" => "Like",
                            "tipo" => 1,
                        ),
                        2 => array(
                            "id" => 2,
                            "descripcion" => "Comentarios",
                            "tipo" => 1,
                        ),
                        3 => array(
                            "id" => 3,
                            "descripcion" => "Shares",
                            "tipo" => 1,
                        ),
                        4 => array(
                            "id" => 4,
                            "descripcion" => "Indicadores",
                            "tipo" => 1,
                        ),
                        5 => array(
                            "id" => 5,
                            "descripcion" => "Engament rate",
                            "tipo" => 1,
                        )
                ),
                2 => array(
                        6 => array(
                            "id" => 6,
                            "descripcion" => "Followers",
                            "tipo" => 2,
                        ),
                        7 => array(
                            "id" => 7,
                            "descripcion" => "Menciones",
                             "tipo" => 2,
                        )
                ),
                3 => array(
                        8 => array(
                            "id" => 8,
                            "descripcion" => "Reproducciones",
                             "tipo" => 1,
                        ),
                        9 => array(
                            "id" => 9,
                            "descripcion" => "Suscriptores",
                             "tipo" => 2,
                        )
                ),
                4 => array(
                        10 => array(
                            "id" => 10,
                            "descripcion" => "Objetivos",
                             "tipo" => 1,
                        ),
                        11 => array(
                            "id" => 11,
                            "descripcion" => "Traficos",
                            "tipo" => 2,
                        )
                ),
        );
        
        return array_key_exists($id, $lista)? $lista[$id] : array();
    }
    
    public function action_getindicadorbysocial()
    {
        if (!$id = $this->request->post('id')) {
            Ajax::error("Existen problemas con la red social seleccionada");
        }
        
        $aIndicadores = $this->indicadorbysocial($id);
        
        $this->response->body(View::factory('Dashboard/Campania/getindicadorbysocial')->set(compact("aIndicadores")));
    }
    
    public function action_getitembycuenta()
    {
        if (!$id = $this->request->post("id"))
            Ajax::error("No existen items para esta cuenta");
        
        $aItems = ORM::factory('Redes_Item')->where("autorizacion_id", "=", $id)->where("status", "=", 1)->order_by('descripcion', 'asc')->find_all();
        
        if (count($aItems) > 0) {
            $this->response->body(View::factory('Dashboard/Campania/getItemsByCuenta')->set(compact("aItems")));
        } else {
            Ajax::error("No existen items para esta cuenta");
        }
    }
    
    public function action_eliminar()
    {
        $oCampania = ORM::factory('Rep_Campania')->where("id", "=", $this->request->param('id'))->find();

        if (!$oReporte->loaded())
             $this->redirect('dashboard/campania/');
        
        $status = $oReporte->status;
        
        $crontab = new Ssh2CrontabManager('tribal121.com', '22', 'tribu121', 'rF,rO?InD;2z');
        
        $inicio_campania = $oCampania->inicio_campania;
        $fin_campania = $oCampania->fin_campania;
        $fecha_actual = date('Y-m-d H:i:s');

        //verificamos si ya se inicio la campaña, si este es elc aso no puede desactivarse la campaña
        if(strtotime($fecha_actual) > strtotime($inicio_campania))
        {
            Message::danger("La campaña ya se inicio. Solo puede anularla");

        }else if(strtotime($fecha_actual) > strtotime($fin_campania))
        {
         //verificamos si ya se inicio la campaña, si este es elc aso no puede desactivarse la campaña
            Message::danger("La campaña ya se termino. Solo puede anularla");

        }{
            $crontab = new Ssh2CrontabManager('tribal121.com', '22', 'tribu121', 'rF,rO?InD;2z');
                    
            if($status == 0)
            {
                $oReporte->status = 1;
                $crontab->append_cronjob($oCampania->cronjob_inicio);
                 Message::success("Reactivado exitosamente!");
                 
            }else if($status == 1)
            {
                $oReporte->status = 0;
                $crontab->remove_cronjob($oCampania->cronjob_inicio);
                Message::success("Eliminado exitosamente!");
                 
            }

            $oReporte->update_at = date('Y-m-d H:i:s');
            $oReporte->save();

        }

        $this->redirect('dashboard/');
    }
    
    public function action_verificacioncampnias()
    {
        $aCampnias = ORM::factory("Rep_Campania")
                ->where("revision_ajax", "=", 0)
                ->where("status", "=", 1)
                ->where("user_id", "=", $this->oUser->id)
                ->where('fin_campana', '>=', date('Y-m-d H:i:s'))->find_all();
        
        $lista = array();
        foreach ($aCampnias as $value) 
        {
            $value->status = 2;
            $value->revision_ajax = 1;
            $value->save();
            
            array_push($lista, $value);
        }
        
        if(count($lista) > 0)
        {
            $this->response->body(json_encode($lista));
        }else{
            $this->response->body(json_encode(array()));
        }
        
        //$aCamapnias = ORM::factory("Rep_Campania")->where("revision_ajax", "=", 0)->where("status", "=", 2)->where("user_id", "=", $this->oUser->id)->find_all();
    }
}

