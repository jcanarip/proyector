<?php
defined('SYSPATH') or die('No direct access allowed.');

class Controller_Dashboard_Dashboard extends Controller_Dashboard_Template
{
    public $title = "CMS :: DASHBOARDS";
    
    public function before()
    {  

        parent::before(); 

        if($this->oUser->profile_id == 3 and in_array( $this->request->action() ,array("view","editar","crear","eliminar") ))
        {
           $this->request->redirect('dashboard/');
        }
        
    } 
    
    public function action_index()
    {
        
        $this->title = "CMS :: DASHBOARDS";
        
        $this->template->styles = array(
            URL::base(TRUE) . "media/lib/data-tables/media/css/dataTables.bootstrap.css"
        );
        
        $script =  URL::base(TRUE)."media/js/dashboard.dashboard.intro.js";
        
        if($this->oUser->aHistorial->count_all() > 1)
        {
            $script = null;
            unset($_COOKIE['value']);
        }
        
        if(isset($_COOKIE['value']))
        {
            if(base64_decode($_COOKIE['value'])  == 'dashboard' )
            {
                $script = null;
                unset($_COOKIE['value']);
            }
        }
        
        $this->template->scripts = array(
            URL::base(TRUE) . "media/lib/data-tables/media/js/jquery.dataTables.js",
            URL::base(TRUE) . "media/lib/data-tables/media/js/dataTables.bootstrap.js",
            URL::base(TRUE) . "media/js/dashboard.dashboard.index.js",$script
        );
        
        $content                 = View::factory('Dashboard/Dashboard/index');
        $this->template->content = $content;
    }
    
    public function action_list()
    {
        $oUser    = $this->oUser;
        
        if(count($condicional = $oUser->aEmpresa->find_all()->as_array("id")) == 0)
        {
            $condicional = DB::expr('(null)');
        }

        $reportes = ORM::factory('Redes_Reporte')->where("empresa_id", "in", $condicional);
        
        $paginate = Paginate::factory($reportes)->sort('created_at', "DESC");;
        
        $datatables = DataTables::factory($paginate)->request($this->request)->view('Dashboard/Dashboard/list')->execute()->render();
        
        $this->response->body($datatables);
    }
    
    public function action_crear()
    {
        if ($this->request->param('id'))
            $this->request->redirect('dashboard/dashboard/crear');
        $this->action_editar(NULL);
    }
    
    public function action_editar($editar = TRUE)
    {
        if ($editar) {
            $this->template->scripts = array(
                URL::base(TRUE) . "media/js/dashboard.dashboard.editar.js"
            );
            
            if (!$id = $this->request->param('id'))
                $this->redirect('dashboard/');
        } else {
            $this->template->scripts = array(
                URL::base(TRUE) . "media/js/dashboard.dashboard.crear.js"
            );
            $id                      = NULL;
        }
        
        $oUser    = $this->oUser;
        $aListadEmpresas = $oUser->aEmpresa->find_all();
        
        if(count($condicional = $aListadEmpresas->as_array("id")) == 0)
        {
            $condicional = DB::expr('(null)');
        }
        
        $oReporte = ORM::factory('Redes_Reporte')->where("id", "=", $id)->where("empresa_id", "in", $condicional)->find();
        $query    = DB::select("redes_autorizacion.social_id,redes_social.*")
                    ->from('redes_social')
                    ->join('redes_autorizacion')->on('redes_social.id', '=', 'redes_autorizacion.social_id')
                    ->where("redes_autorizacion.user_id", "=", $this->oUser->id)
                    ->where("redes_autorizacion.status", "=", 1)
                    ->distinct('redes_autorizacion.social_id')
                    ->as_object();
        $aSocial  = $query->execute();
        
        $aCuentas = null;
        $aPeriodo = array();
        
        if ($oReporte->loaded()) 
        {
            $aPeriodo = $this->getPeriodoByTipodashboard($oReporte->tipo);
            $this->title             = "CMS :: DASHBOARDS - EDITAR " . strtoupper($oReporte->descripcion);
        }else{
            $this->title             = "CMS :: DASHBOARDS - CREAR ";
        }
        
        if (count($aSocial) == 1) 
        {
            $aCuentas = ORM::factory('Redes_Autorizacion')->where("social_id", "=", $aSocial[0]->id)->where("user_id", "=", $this->oUser->id)->where("status", "=", 1)->find_all();
        }
        
        $aTipodashboard = ORM::factory('Rep_Tipodashboard')->where("status", "=", 1)->find_all();
        
        if ($this->request->method() == 'POST') {
            if ($id) {
                $oReporte->update_at = date('Y-m-d H:i:s');
            } else {
                $oReporte->created_at = date('Y-m-d H:i:s');
            }
            
            $oReporte->descripcion = $this->request->post("descripcion");
            $oReporte->tipo        = $this->request->post("tipo");
            $oReporte->plantilla   = 0;
            //$oReporte->plantilla   = $this->request->post("plantilla");
            $oReporte->periodo     = $this->request->post("periodo");
            $oReporte->empresa_id  = $this->request->post("empresa_id");
            $oReporte->user_id     = $this->oUser->id;
            $oReporte->save();
            
            $aItem = $this->request->post("items");
            
            DB::delete('redes_reportedetalle')->where('redes_reporte_id', '=', $oReporte->id)->execute();
            
            foreach ($aItem as $value) {
                $oReportedetalle                   = ORM::factory('Redes_Reportedetalle');
                $oReportedetalle->redes_reporte_id = $oReporte->id;
                $oReportedetalle->redes_item_id    = $value;
                $oReportedetalle->created_at       = date('Y-m-d H:i:s');
                $oReportedetalle->save();
            }
            
            Message::success("Guardado exitosamente!");
            $this->redirect('dashboard/');
        }
        
        
        $this->template->content = View::factory('Dashboard/Dashboard/editar')->set(compact("oReporte", "aSocial", "aCuentas", "aTipodashboard", "aPeriodo","aListadEmpresas"));
    }
    
    public function action_sendemail()
    {
        if (!$id = $this->request->post("time"))
            Ajax::error("EXiste un problema en el servidor");
        
        if (!$email = $this->request->post("email"))
            Ajax::error("No existen periodos para esta cuenta");
        
        $urlpdf = json_decode($this->generarpdf($id), true);
        $urlexcel = json_decode($this->generarexcel($id), true);
        
        $lista = explode(",", $email);

        foreach ($lista as $value) 
        {
            Email::factory('Reporte', 'Puede descargar los reportes en los siguientes links: <br> <h2>Version PDF: </h2><br> '.$urlpdf["ruta"].'<br> <h2>Version EXCEL: </h2><br> '.$urlexcel["ruta"], 'text/html')
                 ->to($value)
                 ->from('hola@proyectotribal.com', 'Proyecto R')
                 ->send();
        }

        $this->response->body(json_encode(array("success" => 1, "message" => "Email enviado.")));
    }
    
    public function action_showPeriodoByTipodashboard()
    {
        if (!$id = $this->request->post("id"))
            Ajax::error("No existen periodos para esta cuenta");
        
        $aPeriodo = $this->getPeriodoByTipodashboard($id);
        
        $this->response->body(View::factory('Dashboard/Dashboard/showPeriodoByTipodashboard')->set(compact("aPeriodo")));
    }
    
    /**
     * 
     * @param int $id tipo d edashboard
     * @return array lista periodos segun id
     */
    private function getPeriodoByTipodashboard($id)
    {
        $lista = array(
            1 => array(
                array(
                    "id" => 1,
                    "descripcion" => "Diario"
                ),
                array(
                    "id" => 2,
                    "descripcion" => "Semanal (hasta 3 meses atras)"
                ),
                array(
                    "id" => 3,
                    "descripcion" => "Mensual (hasta 1 año atras)"
                ),
                array(
                    "id" => 4,
                    "descripcion" => "Trimestral (hasta 1 año atras)"
                ),
                array(
                    "id" => 5,
                    "descripcion" => "Anual (hasta 1 año atras)"
                )
            ),
            2 => array(
                array(
                    "id" => 6,
                    "descripcion" => "Pequeño (Hcae 1, 7 y 15 dìas)"
                ),
                array(
                    "id" => 7,
                    "descripcion" => "Medio (Hace 7, 15 y 30 dìas)"
                ),
                array(
                    "id" => 8,
                    "descripcion" => "Largo (Hace 7, 30 y 90 dìas)"
                ),
                array(
                    "id" => 9,
                    "descripcion" => "Vista anual (Hace 30, 180 y 360 dìas)"
                )
            ),
            3 => array(
                array(
                    "id" => 10,
                    "descripcion" => "Por intervalos de fechas"
                )
                
            )
        );
        
        return array_key_exists($id, $lista) ? $lista[$id] : Ajax::error("El periodo seleccionada por tipo de dashboard no existe");;
    }
    
    private function getFechasPorSemanas($hoy = null)
    {
        $semanas = array();
        
        if ($hoy == null) {
            $hoy = date('Y-m-d');
        }
        
        $hoy = date('Y-m-d', strtotime('Last Monday', strtotime($hoy)));
        for ($i = 1; $i < 13; $i++) {
            $first_day_of_week = date('Y-m-d', strtotime($hoy));
            $last_day_of_week  = date('Y-m-d', strtotime('Next Sunday', strtotime($hoy)));
            $hoy               = date('Y-m-d', strtotime("-1 week", strtotime($first_day_of_week)));
            $semanas[$i]       = array(
                0 => $first_day_of_week,
                1 => $last_day_of_week
            );
        }
        
        return $semanas;
    }
    
    private function getFechasPorMes($hoy = null)
    {
        $mes = array();
        
        if ($hoy == null) {
            $hoy = date('Y-m-d');
        }
        
        for ($i = 1; $i < 13; $i++) {
            $first_day_of_moth = date('Y-m-d', strtotime('first day of this month', strtotime($hoy)));
            $last_day_of_moth  = date('Y-m-d', strtotime('last day of this month', strtotime($hoy)));
            $hoy               = date('Y-m-d', strtotime("-1 month", strtotime($first_day_of_moth)));
            $mes[$i]           = array(
                0 => $first_day_of_moth,
                1 => $last_day_of_moth
            );
        }
        
        return $mes;
    }
    
    private function getFechasPorTrimestre($hoy = null)
    {
        $trimestre = array();
        
        if ($hoy == null) {
            $hoy = date('Y-m-d');
        }
        
        for ($i = 1; $i < 5; $i++) {
            $last_day_of_moth  = date('Y-m-d', strtotime('last day of this month', strtotime($hoy)));
            $first_day_of_moth = date('Y-m-d', strtotime('first day of this month', strtotime($hoy)));
            $first_day_of_moth = date('Y-m-d', strtotime("-2 month", strtotime($first_day_of_moth)));
            $hoy               = date('Y-m-d', strtotime("-1 month", strtotime($first_day_of_moth)));
            
            $trimestre[$i] = array(
                0 => $first_day_of_moth,
                1 => $last_day_of_moth
            );
        }
        return $trimestre;
    }
    
    private function getFechasPorAnio($hoy = null)
    {
        $anio = array();
        
        if ($hoy == null) {
            $hoy = date('Y');
        }
        
        for ($i = 1; $i < 3; $i++) {
            $first_day_of_year = $hoy . "-01-01";
            $last_day_of_year  = $hoy . "-12-31";
            
            $hoy = date("Y", strtotime("-1 year", strtotime($last_day_of_year)));
            
            $anio[$i] = array(
                0 => $first_day_of_year,
                1 => $last_day_of_year
            );
        }
        return $anio;
    }
    
    private function getPlantillaBySocial($id)
    {
        
        $lista = array(
            1 => array(
                1 => array(
                    
                    "name" => "Facebook",
                    "detalle" => json_encode(array(
                        array(
                            "descripcion" => "PAGE IMPRESSIONS",
                            "funcion" => "page_impressions",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "PAGE IMPRESSIONS ORGANICS",
                            "funcion" => "page_impressions_organic",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "ENGAGED USERS",
                            "funcion" => "page_engaged_users",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "FANS COUNT",
                            "funcion" => "page_fans",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "VIRALITY IMPRESSIONS",
                            "funcion" => "page_impressions_viral",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "PAGE INTERACTIONS",
                            "funcion" => "page_consumptions",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "STORIES ABOUT YOU",
                            "funcion" => "page_story_adds",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "FAN ADDS",
                            "funcion" => "page_fan_adds_unique",
                            "tipo" => 1
                        ),
                       /* array(
                            "descripcion" => "PEOPLE TALKING ABOUT YOU",
                            "funcion" => "page_storytellers",
                            "tipo" => 1
                        ),*/
                        array(
                            "descripcion" => "TOP POSTS BY PEOPLE TALKING ABOUT IT",
                            "funcion" => "post_storytellers",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP POSTS BY STORIES CREATED",
                            "funcion" => "post_stories",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP POSTS BY VIRALITY IMPRESSIONS",
                            "funcion" => "post_impressions_viral",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP POSTS BY IMPRESSIONS",
                            "funcion" => "post_impressions",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP POSTS BY ENGAGED USERS",
                            "funcion" => "post_engaged_users",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP POSTS BY LIKES",
                            "funcion" => "post_likes",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP POSTS BY COMMENTS",
                            "funcion" => "post_comments",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "FIDELIDAD",
                            "funcion" => "post_fidelidad",
                            "tipo" => 2
                        )
                    ))
                )
            ),
            2 => array(
                2 => array(
                    "name" => "Twitter",
                    "detalle" => json_encode(array(
                        array(
                            "descripcion" => "FOLLOWERS",
                            "funcion" => "followers",
                            "tipo" => 3
                        ),
                        /* array(
                        "descripcion" => "MENTIONS",
                        "funcion" => "mentions",
                        "tipo" => 1
                        ),*/
                        array(
                            "descripcion" => "RETWEETS",
                            "funcion" => "retweets",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "LISTED COUNT",
                            "funcion" => "listed_count",
                            "tipo" => 3
                        ),
                        array(
                            "descripcion" => "TOP RETWEETS POSTS",
                            "funcion" => "top_retweeted_posts",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP MENTIONS BY FOLLOWERS COUNT",
                            "funcion" => "top_mentions_by_followers_count",
                            "tipo" => 2
                        )
                    ))
                )
            ),
            3 => array(
                3 => array(
                    "name" => "Youtube",
                    "detalle" => json_encode(array(
                        array(
                            "descripcion" => "YOUTUBE SUBSCRIBERS",
                            "funcion" => "subscriptores",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "VIDEO VIEWS",
                            "funcion" => "views",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "ESTIMATED MINUTES WATCHED",
                            "funcion" => "estimatedMinutesWatched",
                            "tipo" => 1
                        ),
                        /*  array(
                        "descripcion" => "FAVORITES ADDED",
                        "funcion" => "favorities",
                        "tipo" => 1
                        ),*/
                        array(
                            "descripcion" => "LIKES",
                            "funcion" => "likes",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "DISLIKES",
                            "funcion" => "dislikes",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "SHARES",
                            "funcion" => "shares",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "COMMENTS COUNT",
                            "funcion" => "comments",
                            "tipo" => 1
                        )
                    ))
                )
            ),
            4 => array(
                4 => array(
                    "name" => "pADSENSE",
                    "detalle" => json_encode(array(
                        array(
                            "descripcion" => "ADSENSE CTR ",
                            "funcion" => "adsensectr",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "ADSENSE REVENUE",
                            "funcion" => "adsenserevenue",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "ADSENSE REVENUE PER VISIT",
                            "funcion" => "adsenserevenuepervisit",
                            "tipo" => 1
                        )
                    ))
                ),
                5 => array(
                    "name" => "APP TRACKING",
                    "detalle" => json_encode(array(
                        array(
                            "descripcion" => "AVERAGE TIME ON SCREEN",
                            "funcion" => "avgscreenviewduration",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "SCREEN VIEWS",
                            "funcion" => "screenviews",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "SCREEN VIEWS PER VISIT",
                            "funcion" => "screenviewspervisit",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "TOP SCREEN (VIEWS)",
                            "funcion" => "topscreen",
                            "tipo" => 2
                        )
                    ))
                ),
                6 => array(
                    "name" => "CONTENT",
                    "detalle" => json_encode(array(
                        array(
                            "descripcion" => "NUMBER OF PAGEVIEWS",
                            "funcion" => "pageviews",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "PAGES PER VISIT",
                            "funcion" => "pagepervisit",
                            "tipo" => 1
                        ),
                        
                        array(
                            "descripcion" => "TOP EXIT PAGES (VISITS)",
                            "funcion" => "exitpagepath",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP LANDING PAGES",
                            "funcion" => "landingpagepath",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP PAGES (PAGEVIEWS)",
                            "funcion" => "pagepath",
                            "tipo" => 2
                        )
                    ))
                ),
                7 => array(
                    "name" => "E-COMMERCE",
                    "detalle" => json_encode(array(
                        array(
                            "descripcion" => "DAYS TO TRANSACTION",
                            "funcion" => "daystotransaction",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "ITEMS PER TRANSACTION",
                            "funcion" => "itemspertransaction",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "REVENUE",
                            "funcion" => "transactionrevenue",
                            "tipo" => 1
                        ),
                        /*  array(
                        "descripcion" => "REVENUE (GOAL)",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        
                        array(
                        "descripcion" => "REVENUE PER SOURCE",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "REVENUE PER TRANSACTION",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "REVENUE PER VISIT",
                        "funcion" => "",
                        "tipo" => 1
                        ),*/
                        array(
                            "descripcion" => "TRANSACTIONS",
                            "funcion" => "transactions",
                            "tipo" => 1
                        )
                        /* array(
                        "descripcion" => "TRANSACTIONS (GOAL)",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        
                        array(
                        "descripcion" => "TRANSACTIONS PER SOURCE",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "VISITS TO TRANSACTION",
                        "funcion" => "",
                        "tipo" => 1
                        ),*/
                    ))
                ),
                8 => array(
                    "name" => "EVENTS",
                    "detalle" => json_encode(array(
                        array(
                            "descripcion" => "TOP EVENT CATEGORIES (EVENTS)",
                            "funcion" => "eventcategory",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP EVENT CATEGORIES + ACTIONS (EVENTS)",
                            "funcion" => "eventaction",
                            "tipo" => 2
                        )
                    ))
                ),
                9 => array(
                    "name" => "GOALS",
                    "detalle" => json_encode(array(
                        /*array(
                        "descripcion" => "AVERAGE VISIT VALUE",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "AVERAGE VISIT VALUE (GOAL)",
                        "funcion" => "",
                        "tipo" => 1
                        ),*/
                        array(
                            "descripcion" => "CONVERSION RATE",
                            "funcion" => "goalconversionrateall",
                            "tipo" => 1
                        ),
                        
                        array(
                            "descripcion" => "CONVERSIONS",
                            "funcion" => "goalcompletionsall",
                            "tipo" => 1
                        ),
                        
                        array(
                            "descripcion" => "TOP CONVERTING GOALS",
                            "funcion" => "goalcompletionlocation",
                            "tipo" => 2
                        ),
                        /*array(
                        "descripcion" => "TOP CONVERTING GOALS (ORGANIC)",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "TOP CONVERTING GOALS (PAID SEARCH)",
                        "funcion" => "",
                        "tipo" => 1
                        ),*/
                        array(
                            "descripcion" => "TOTAL GOAL VALUE",
                            "funcion" => "goalvalueall",
                            "tipo" => 1
                        )
                    ))
                ),
                10 => array(
                    "name" => "MOBILE",
                    "detalle" => json_encode(array(
                        array(
                            "descripcion" => "MOBILE INTERACTION TYPE",
                            "funcion" => "mobileinputselector",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP MOBILE DEVICES (VISITS)",
                            "funcion" => "mobiledevicemodel",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP MOBILE DEVICES BRANDING (VISITS)",
                            "funcion" => "mobiledevicebranding",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP MOBILE PLATFORM (VISITS)",
                            "funcion" => "mobiledeviceinfo",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "VISITS FROM MOBILE",
                            "funcion" => "visitsfrommobile",
                            "tipo" => 1
                        )
                        
                    ))
                ),
                11 => array(
                    "name" => "MULTI-CHANNEL FUNNELS",
                    "detalle" => json_encode(array(
                        array(
                            "descripcion" => "ASSISTED CONVERSION BY GROUPING",
                            "funcion" => "channelgrouping",
                            "tipo" => 2
                        )
                        /*array(
                        "descripcion" => "ASSISTED CONVERSION BY SOURCE / MEDIUM",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "ASSISTED CONVERSION VALUE",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "ASSISTED GOALS ONLY",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "ASSISTED TRANSACTIONS ONLY",
                        "funcion" => "",
                        "tipo" => 1
                        ),array(
                        "descripcion" => "ASSISTED VALUE BY SOURCE / MEDIUM",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "ORG. ASSISTED CONV. RATE",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "ORGANIC ASSISTED CONVERSIONS",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "ORGANIC ASSISTED VALUE",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "ORGANIC COMBINED CONV. RATE",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "ORGANIC COMBINED CONVERSIONS",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "ORGANIC COMBINED VS OVERALL",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "PAID ASSISTED CONV. RATE",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "PAID COMBINED CONVERSIONS",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "PAID COMBINED VS OVERALL",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "PAID SEARCH ASSISTED CONV.",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "PAID SEARCH ASSISTED VALUE",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "TOP CONVERSION PATHS",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "TOP CONVERSION PATHS W/ ORGANIC",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "TOP CONVERSION PATHS W/ PAID SEARCH",
                        "funcion" => "",
                        "tipo" => 1
                        ),*/
                    ))
                ),
                12 => array(
                    "name" => "SEM / PPC",
                    "detalle" => json_encode(array(
                        array(
                            "descripcion" => "CLICKS",
                            "funcion" => "adclicks",
                            "tipo" => 1
                        ),
                        /*array(
                        "descripcion" => "CONVERSIONS FROM PPC",
                        "funcion" => "",
                        "tipo" => 1
                        ),*/
                        array(
                            "descripcion" => "COST",
                            "funcion" => "adcost",
                            "tipo" => 1
                        ),
                        /*array(
                        "descripcion" => "COST + 15% MARKUP",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "COST PER CLICK (CPC) + 15% MARKUP",
                        "funcion" => "",
                        "tipo" => 1
                        ),*/
                        array(
                            "descripcion" => "COST PER CONVERSION",
                            "funcion" => "costperconversion",
                            "tipo" => 1
                        ),
                        /*array(
                        "descripcion" => "COST PER LEAD. (CPL) + 15% MARKUP",
                        "funcion" => "",
                        "tipo" => 1
                        ),*/
                        array(
                            "descripcion" => "CTR (CLICK-THROUGH-RATE)",
                            "funcion" => "CTR",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "IMPRESSIONS",
                            "funcion" => "impressions",
                            "tipo" => 1
                        )
                        /*array(
                        "descripcion" => "NEW VISITS FROM PAID SEARCH",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "RATIO OF NEW VISITS FROM PAID SEARCH",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "ROI (RETURNS ON INVESTMENT)",
                        "funcion" => "",
                        "tipo" => 1
                        ),
                        array(
                        "descripcion" => "TOP PPC CAMPAIGNS (VISITS)",
                        "funcion" => "",
                        "tipo" => 1
                        ),*/
                    ))
                ),
                /*13 => array(
                "name" => "SEO",
                "detalle" => json_encode(array(
                array(
                "descripcion" => "CONVERSION RATE FROM ORGANIC",
                "funcion" => "",
                "tipo" => 1
                ),
                array(
                "descripcion" => "CONVERSIONS FROM ORGANIC",
                "funcion" => "",
                "tipo" => 1
                ),
                array(
                "descripcion" => "NUMBER OF ORGANIC LANDING PAGES",
                "funcion" => "",
                "tipo" => 1
                ),
                array(
                "descripcion" => "ORGANIC VISITS RATE",
                "funcion" => "",
                "tipo" => 1
                ),
                array(
                "descripcion" => "TOP KEYWORDS FROM ORGANIC (VISITS)",
                "funcion" => "",
                "tipo" => 1
                ),
                array(
                "descripcion" => "TOP LANDING PAGE FROM ORGANIC (VISITS)",
                "funcion" => "",
                "tipo" => 1
                ),
                array(
                "descripcion" => "TOP SEARCH ENGINES",
                "funcion" => "",
                "tipo" => 1
                ),
                array(
                "descripcion" => "VISITS FROM ORGANIC",
                "funcion" => "",
                "tipo" => 1
                ),
                
                ))
                ),
                
                14 => array(
                "name" => "SOCIAL MEDIA",
                "detalle" => json_encode(array(
                array(
                "descripcion" => "ONSITE FACEBOOK LIKES",
                "funcion" => "",
                "tipo" => 1
                ),
                array(
                "descripcion" => "ONSITE TWEETS",
                "funcion" => "",
                "tipo" => 1
                ),
                array(
                "descripcion" => "SOCIAL MEDIA INTERACTIONS",
                "funcion" => "",
                "tipo" => 1
                ),
                array(
                "descripcion" => "SOCIAL MEDIA INTERACTIONS PER VISIT",
                "funcion" => "",
                "tipo" => 1
                ),
                
                array(
                "descripcion" => "SOCIAL NETWORKS",
                "funcion" => "",
                "tipo" => 1
                ),
                array(
                "descripcion" => "TOP SOCIAL MEDIA ACTIONS (INTERACTIONS)",
                "funcion" => "",
                "tipo" => 1
                ),
                array(
                "descripcion" => "TOP SOCIAL MEDIA SOURCES (INTERACTIONS)",
                "funcion" => "",
                "tipo" => 1
                ),
                ))
                ),*/
                15 => array(
                    "name" => "STANDARD",
                    "detalle" => json_encode(array(
                        array(
                            "descripcion" => "AVERAGE VISIT TIME",
                            "funcion" => "avgSessionDuration",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "BOUNCE RATE",
                            "funcion" => "bouncerate",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "DAYS SINCE LAST VISIT",
                            "funcion" => "dayssincelastsession",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "NEW VISIT RATE",
                            "funcion" => "percentnewsessions",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "NEW VISITORS",
                            "funcion" => "newusers",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "NEW VS RETURNING",
                            "funcion" => "newvsreturning",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "RETURNING VISITORS",
                            "funcion" => "returningvisitors",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "TOTAL TIME ON SITE",
                            "funcion" => "totaltimeonsite",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "TOTAL VISITS",
                            "funcion" => "sessions",
                            "tipo" => 1
                        ),
                        
                        array(
                            "descripcion" => "UNIQUE VISITORS",
                            "funcion" => "users",
                            "tipo" => 1
                        ),
                        
                        array(
                            "descripcion" => "VISIT COUNT",
                            "funcion" => "sessioncount",
                            "tipo" => 2
                        )
                    ))
                ),
                16 => array(
                    "name" => "TRAFFIC SOURCES",
                    "detalle" => json_encode(array(
                        array(
                            "descripcion" => "SOURCE / MEDIUM (VISITS)",
                            "funcion" => "sourcemedium",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP CITIES (VISITS)",
                            "funcion" => "city",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP CONTINENTS (VISITS)",
                            "funcion" => "ontinent",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP COUNTRIES (VISITS)",
                            "funcion" => "country",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP REFERERS",
                            "funcion" => "referralpath",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP REGIONS (VISITS)",
                            "funcion" => "region",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP SOURCES (VISITS)",
                            "funcion" => "source",
                            "tipo" => 2
                        )
                    ))
                ),
                17 => array(
                    "name" => "TEGNOLOGY",
                    "detalle" => json_encode(array(
                        array(
                            "descripcion" => "AVG. LOAD TIME (in MS)",
                            "funcion" => "pageloadtime",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "TOP BROWSERS (VISITS)",
                            "funcion" => "browser",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP BROWSERS + VERSION (VISITS)",
                            "funcion" => "browserversion",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP LANGUAGES (VISITS)",
                            "funcion" => "language",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP OPERATING SYSTEMS (VISITS)",
                            "funcion" => "operatingsystem",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP OPERATING SYSTEMS + VERSION (VISITS)",
                            "funcion" => "operatingsystemversion",
                            "tipo" => 2
                        ),
                        array(
                            "descripcion" => "TOP SCREEN RESOLUTIONS (VISITS)",
                            "funcion" => "screenresolution",
                            "tipo" => 2
                        )
                    ))
                )
            ),
            /*7 => array(
                18 => array(
                    "name" => "Instagram",
                    "detalle" => json_encode(array(
                        array(
                            "descripcion" => "INSTAGRAM COMMENTS COUNT",
                            "funcion" => "comments",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "INSTAGRAM LIKES",
                            "funcion" => "likes",
                            "tipo" => 1
                        ),
                        array(
                            "descripcion" => "INSTAGRAM MEDIA COUNT",
                            "funcion" => "media",
                            "tipo" => 1
                        ),
                    ))
                )
            ),*/
        );
        
        return array_key_exists($id, $lista) ? $lista[$id] :  Ajax::error("La plantilla seleccionada por la red social no existe");
    }
    
    public function action_savedashboard()
    {
        if (!$id = $this->request->post('dashboard'))
            Ajax::error("Este Dashboard no existe");
        if (!$tipo_dhasboard = $this->request->post('tipo_dhasboard'))
            Ajax::error("Tipo de dashboard no existe");
        
        $lista                   = array();
        $lista["fechainicio"]    = $this->request->post('fechainicio');
        $lista["tipo_dhasboard"] = $tipo_dhasboard;
        $lista["data"]           = array();
        
        if ($this->request->post('date-table')) {
            foreach ($this->request->post('date-table') as $value) {
                array_push($lista["data"], json_decode($value, true));
            }
        }
        
        
        $oReporte = ORM::factory('Redes_Reporte', $id);
        
        if ($oReporte->loaded()) {
            $oReporte->datetime  = json_encode($lista);
            $oReporte->update_at = date('Y-m-d H:i:s');
            $oReporte->save();
            $this->response->body(json_encode(array(
                "success" => true,
                "message" => "Dashboard grabado exitosamente."
            )));
        } else {
            Ajax::error("Este Dashboard no existe");
        }
    }
    
    public function action_getCuentasBySocial()
    {
        if (!$id = $this->request->post('id')) {
            Ajax::error("Existen problemas con la red social seleccionada");
        }
        
        $aCuentas = ORM::factory('Redes_Autorizacion')->where("social_id", "=", $id)->where("user_id", "=", $this->oUser->id)->find_all();
        
        $this->response->body(View::factory('Dashboard/Dashboard/getcuentasbysocial')->set(compact("aCuentas")));
    }
    
    public function action_getItemsByCuenta()
    {
        if (!$id = $this->request->post("id"))
            Ajax::error("No existen items para esta cuenta");
        
        $aItems = ORM::factory('Redes_Item')->where("autorizacion_id", "=", $id)->where("status", "=", 1)->order_by('descripcion', 'asc')->find_all();
        
        if (count($aItems) > 0) {
            $data = array();
            
            foreach ($aItems as $key => $value) {
                array_push($data, array(
                    "id" => "item-" . strtolower($value->oAutorizacion->oSocial->name) . "-" . $value->id,
                    "value" => $value->id,
                    "descripcion" => $value->descripcion
                ));
            }
            
            $this->response->body(json_encode($data));
        } else {
            Ajax::error("No existen items para esta cuenta");
        }
    }
    
    public function action_eliminar()
    {

        $oReporte = ORM::factory('Redes_Reporte')->where("id", "=", $this->request->param('id'))->where("user_id", "=", $this->oUser->id)->find();

        if (!$oReporte->loaded())
            $this->redirect('dashboard/');
        
        $status = $oReporte->status;
        
        $oReporte->status    = ($status == '1') ? '0' : '1';
        $oReporte->update_at = date('Y-m-d H:i:s');
        $oReporte->save();
        
        Message::success("Eliminado exitosamente!");
        
        $this->redirect('dashboard/');
    }
    
    public function action_view()
    {
        if (!$id = $this->request->param('id'))
            $this->redirect('dashboard/');
        
        $this->template->styles = array(
            URL::base(TRUE) . "media/lib/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css",
            URL::base(TRUE) . "media/lib/gridster/src/jquery.gridster.css",
        );
        
        $script =  URL::base(TRUE)."media/js/dashboard.dashboard.introview.js";

        if($this->oUser->aHistorial->count_all() > 1)
        {
            $script = null;
            unset($_COOKIE['value']);
        }
        
        
        if(isset($_COOKIE['value']))
        {
            if(base64_decode($_COOKIE['value'])  == 'introview' )
            {
                $script = null;
                unset($_COOKIE['value']);
            }
        }
        
        $this->template->scripts = array(
            "https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['corechart']}]}",
            URL::base(TRUE) . "media/lib/moment-with-locales.js",
            URL::base(TRUE) . "media/lib/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js",
            URL::base(TRUE) . "media/js/dashboard.dashboard.view.js",$script
        );
        
        $aPlantillas = array();
        $oReporte    = ORM::factory('Redes_Reporte')->where("id", "=", $id)->find();
        $aItem       = $oReporte->aItem->find_all();
        
        foreach ($aItem as $value) {
            $aPlantillas[$value->oAutorizacion->social_id] = array(
                "id" => $value->oAutorizacion->social_id,
                "detalle" => $this->getPlantillaBySocial($value->oAutorizacion->social_id)
            );
        }
        
        $this->title = "CMS :: DASHBOARDS - " . strtoupper($oReporte->descripcion);
        
        $content = View::factory('Dashboard/Dashboard/view')->set(compact("aItem", 'oReporte', "aPlantillas"));
        
        if ($oReporte->periodo == 2) {
            $content->set("aTiempo", $this->getFechasPorSemanas());
        }
        
        if ($oReporte->periodo == 3) {
            $content->set("aTiempo", $this->getFechasPorMes());
        }
        
        if ($oReporte->periodo == 4) {
            $content->set("aTiempo", $this->getFechasPorTrimestre());
        }
        
        if ($oReporte->periodo == 5) {
            $content->set("aTiempo", $this->getFechasPorAnio());
        }
        
        
        $this->template->content = $content;
    }
    
    public function action_show()
    {
        if (!$id = $this->request->param('id'))
            $this->redirect('dashboard/');
        
        $this->template->styles = array(
            URL::base(TRUE) . "media/lib/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css"
        );
        
        $this->template->scripts = array(
            "https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['corechart']}]}",
            URL::base(TRUE) . "media/lib/moment-with-locales.js",
            URL::base(TRUE) . "media/lib/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js",
            URL::base(TRUE) . "media/js/dashboard.dashboard.show.js",

        );
        
        $oReporte    = ORM::factory('Redes_Reporte')->where("id", "=", $id)->find();
        $oUser = $this->oUser;
        
        $this->title = "CMS :: DASHBOARDS - " . strtoupper($oReporte->descripcion);
        
        $content = View::factory('Dashboard/Dashboard/show')->set(compact('oReporte','oUser'));
        
        if ($oReporte->periodo == 2) {
            $content->set("aTiempo", $this->getFechasPorSemanas());
        }
        
        if ($oReporte->periodo == 3) {
            $content->set("aTiempo", $this->getFechasPorMes());
        }
        
        if ($oReporte->periodo == 4) {
            $content->set("aTiempo", $this->getFechasPorTrimestre());
        }
        
        if ($oReporte->periodo == 5) {
            $content->set("aTiempo", $this->getFechasPorAnio());
        }

        $this->template->content = $content;
    }
    
    private function generarpdf($id) 
    {
        $oReporte = ORM::factory('Redes_Reporte', $id);
        
        if(!$oReporte->loaded())
        {
           Ajax::error("No existen item para este dashboard"); 
        }

        $html = View::factory("Dashboard/Dashboard/pdf")
                    ->set("oReporte", $oReporte);

        $document = HTML2PDF::document($html);
        $nombre =  time()."-".$id;
               
        $ruta = "media/pdf/".$nombre.".pdf";
        $document->margins(6,6,6,6);
        //$document->orientation("Landscape");
        $document->save($_SERVER['DOCUMENT_ROOT']."/proyectotribal121/".$ruta);    
        
        return json_encode(array("ruta" => URL::base(TRUE).$ruta));
    }
    
    private function generarexcel($id) 
    {
        $oReporte = ORM::factory('Redes_Reporte', $id);
        
        if(!$oReporte->loaded())
        {
           Ajax::error("No existen item para este dashboard"); 
        }

        $html = View::factory("Dashboard/Dashboard/pdf")
                    ->set("oReporte", $oReporte);

        $ws = new Spreadsheet(array(
        'author'       => 'Proyecto R',
        'title'        => 'Report'
        ));

        $ws->get_active_sheet(0);
        $as = $ws->get_active_sheet();
        $as->setTitle('Report');
        $as->getDefaultStyle()->getFont()->setSize(10);


        if(isset($oReporte->datetime) and !empty($oReporte->datetime))
        {
            $json = json_decode($oReporte->datetime, true);

            if(count($json["data"]) >0)
            {
               // $sh = array();
                $fila = 0;
                foreach ($json["data"] as $value) 
                {
                    $fila++;
                    $cabecera = array();
                    $mes = array();
                    
                    $ws->get_active_sheet()->mergeCells('A'.$fila.':I'.$fila);
                    $ws->get_active_sheet()->setCellValue('A'.$fila, $value["descripcion"]);
                    $ws->get_active_sheet()->getStyle('A'.$fila)->getFont()->setSize(14)->setBold(true);
                    
                   // array_push($sh, array());
                    //array_push($sh, array($value["descripcion"]));
                    
                    if($json["tipo_dhasboard"] == 1)
                    {
                         if($value["tipo"] == 1 or $value["tipo"] == 3)
                         {
                             $fila++;
                             foreach ($value["chart"] as $indice => $value2) 
                             {
                                $ws->get_active_sheet()->setCellValue(chr(65 + $indice).$fila,$value2["c"][0]["v"]);
                                $ws->get_active_sheet()->getStyle(chr(65 + $indice).$fila)->getFont()->setSize(12)->setBold(true);
                                
                                // array_push($cabecera, $value2["c"][0]["v"]);
                             }
                             
                             $fila++;
                             foreach ($value["chart"] as $indice => $value2) 
                             {
                                 $ws->get_active_sheet()->setCellValue(chr(65 + $indice).$fila,$value2["c"][1]["v"]);
                                 //array_push($mes, $value2["c"][1]["v"]);
                             }
                             
                             $fila++;
                             $ws->get_active_sheet()->mergeCells('A'.$fila.':I'.$fila);
                             $fila++;
                             //array_push($sh, $cabecera);
                             //array_push($sh, $mes);

                         }else{
                             foreach ($value["table"] as $value2) 
                             {
                                 $fila++;
                                 $ws->get_active_sheet()->mergeCells('A'.$fila.':I'.$fila);
                                 $ws->get_active_sheet()->setCellValue('A'.$fila, $value2["texto"]);
                                 $ws->get_active_sheet()->setCellValue('J'.$fila, $value2["cantidad"]);
                             }
                             
                             $fila++;
                             $ws->get_active_sheet()->mergeCells('A'.$fila.':J'.$fila);
                             $fila++;
                         }

                    }else if($json["tipo_dhasboard"] == 2){
                        if($value["tipo"] == 1 or $value["tipo"] == 3)
                         {
                                $fila++;
                                foreach ($value["chart"] as  $indice => $value2) 
                                {
                                    //array_push($cabecera, $value2["c"][0]["v"]);
                                    $ws->get_active_sheet()->setCellValue(chr(65 + $indice).$fila,$value2["c"][0]["v"]);
                                    $ws->get_active_sheet()->getStyle(chr(65 + $indice).$fila)->getFont()->setSize(12)->setBold(true);
                                }
                                
                                $fila++;
                                foreach ($value["chart"] as $indice => $value2) 
                                {
                                    $ws->getActiveSheet()->setCellValue(chr(65 + $indice).$fila,$value2["c"][1]["v"]);
                                    //array_push($mes, $value2["c"][1]["v"]);
                                }

                               /* array_push($sh, $cabecera);
                                array_push($sh, $mes);*/
                                //array_push($sh, array());
                                
                                $fila++;
                                $ws->get_active_sheet()->mergeCells('A'.$fila.':I'.$fila);
                                
                                $fila++;
                                $ws->get_active_sheet()->setCellValue("A".$fila,$value["dias"]["dia1"]);
                                $ws->get_active_sheet()->getStyle("A".$fila)->getFont()->setSize(12)->setBold(true);
                                $ws->get_active_sheet()->setCellValue("B".$fila,$value["dias"]["dia2"]);
                                $ws->get_active_sheet()->getStyle("B".$fila)->getFont()->setSize(12)->setBold(true);
                                $ws->get_active_sheet()->setCellValue("C".$fila,$value["dias"]["dia3"]);
                                $ws->get_active_sheet()->getStyle("C".$fila)->getFont()->setSize(12)->setBold(true);
                                
                                $fila++;
                                $ws->get_active_sheet()->setCellValue("A".$fila,$value["periodos"]["columna1"]);
                                $ws->get_active_sheet()->setCellValue("B".$fila,$value["periodos"]["columna2"]);
                                $ws->get_active_sheet()->setCellValue("C".$fila,$value["periodos"]["columna3"]);
                                
                                $fila++;
                                $ws->get_active_sheet()->mergeCells('A'.$fila.':I'.$fila);
                                $fila++;
                                
                                //array_push($sh,array($value["dias"]["dia1"],$value["dias"]["dia2"],$value["dias"]["dia3"]));
                               // array_push($sh,array($value["periodos"]["columna1"],$value["periodos"]["columna2"],$value["periodos"]["columna3"]));

                         }else{
                             $lista = Util::centralizandoList($value["periodos"]);
                            // array_push($sh,array("",$value["dias"]["dia1"],$value["dias"]["dia2"],$value["dias"]["dia3"]));
                             
                             
                            $fila++;
                            $ws->get_active_sheet()->setCellValue("A".$fila,"");
                            $ws->get_active_sheet()->getStyle("A".$fila)->getFont()->setSize(12)->setBold(true);
                            $ws->get_active_sheet()->setCellValue("B".$fila,$value["dias"]["dia1"]);
                            $ws->get_active_sheet()->getStyle("B".$fila)->getFont()->setSize(12)->setBold(true);
                            $ws->get_active_sheet()->setCellValue("C".$fila,$value["dias"]["dia2"]);
                            $ws->get_active_sheet()->getStyle("C".$fila)->getFont()->setSize(12)->setBold(true);
                            $ws->get_active_sheet()->setCellValue("D".$fila,$value["dias"]["dia3"]);
                            $ws->get_active_sheet()->getStyle("D".$fila)->getFont()->setSize(12)->setBold(true);
                            
                           

                             
                             foreach ($lista as $value2) {
                                  //array_push($sh,array($value2["texto"],$value2["columna1"],$value2["columna2"],$value2["columna3"]));
                                $fila++;
                                $ws->get_active_sheet()->setCellValue("A".$fila,$value["texto"]);
                                $ws->get_active_sheet()->setCellValue("B".$fila,$value["columna2"]);
                                $ws->get_active_sheet()->setCellValue("C".$fila,$value["columna3"]);
                                $ws->get_active_sheet()->setCellValue("D".$fila,$value["columna3"]);
                             }
                             
                             $fila++;
                             $ws->get_active_sheet()->mergeCells('A'.$fila.':I'.$fila);
                             $fila++;
                             
                         }
                    }else if($json["tipo_dhasboard"] == 3){
                        if($value["tipo"] == 1 or $value["tipo"] == 3)
                         {
                                //array_push($sh,array("Fecha de reporte","Fecha a comparar","Porcentaje"));
                                //array_push($sh,array($value[0],(!is_null($value[1]))? $value[1] : 0,$value["porcentaje"]));
                                
                                
                                $fila++;
                                $ws->get_active_sheet()->setCellValue("A".$fila,"Fecha de reporte");
                                $ws->get_active_sheet()->getStyle("A".$fila)->getFont()->setSize(12)->setBold(true);
                                $ws->get_active_sheet()->setCellValue("B".$fila,"Fecha a comparar");
                                $ws->get_active_sheet()->getStyle("B".$fila)->getFont()->setSize(12)->setBold(true);
                                $ws->get_active_sheet()->setCellValue("C".$fila,"Porcentaje");
                                $ws->get_active_sheet()->getStyle("C".$fila)->getFont()->setSize(12)->setBold(true);
                                
                                $fila++;
                                $ws->get_active_sheet()->setCellValue("A".$fila,$value[0]);
                                $ws->get_active_sheet()->setCellValue("B".$fila,(!is_null($value[1]))? $value[1] : 0);
                                $ws->get_active_sheet()->setCellValue("C".$fila,$value["porcentaje"]);
                                
                                $fila++;
                                $ws->set_active_sheet()->mergeCells('A'.$fila.':I'.$fila);
                                $fila++;
                         }else{
                             foreach ($value[0] as $value2) 
                             {
                                 //array_push($sh,array($value2["texto"], $value2["cantidad"]));
                                 $fila++;
                                 $ws->get_active_sheet()->mergeCells('A'.$fila.':I'.$fila);
                                 $ws->get_active_sheet()->setCellValue('A'.$fila, $value2["texto"]);
                                 $ws->get_active_sheet()->setCellValue('J'.$fila, $value2["cantidad"]);
                                 
                              
                             }
                             
                            $fila++;
                            $ws->get_active_sheet()->mergeCells('A'.$fila.':J'.$fila);
                            $fila++;
                         }
                    }
                    
                    
                    //array_push($sh, array());
                }
            }
        }

        //$ws->set_data($sh, false);
        //$ws->send(array('name'=>'report', 'format'=>'Excel5'));
        //$document->orientation("Landscape");
        $nombre =  time()."-".$id;
        $ruta = "media/excel/";

        $auxruta = $ws->save(array(
            "path" => $_SERVER['DOCUMENT_ROOT']."/proyectotribal121/".$ruta,
            "name" => $nombre));

        $nombrefinal = explode("/", $auxruta);
        
       return json_encode(array("ruta" => URL::base(TRUE).$ruta.$nombrefinal[count($nombrefinal)-1]));
    }
    
    public function action_getpdf()
    {
        if (!$id = $this->request->post('t'))
            Ajax::error("No existen item para este dashboard");

        $this->response->body($this->generarpdf($id));
    }
    
    public function action_getexcel()
    {
        if (!$id = $this->request->post('t'))
            Ajax::error("No existen item para este dashboard");
 
        $this->response->body($this->generarexcel($id));
    }
    
    public function action_showframe()
    {
        if (!$id = $this->request->post('id'))
            Ajax::error("No existen item para este dashboard");
        if (!$dashboard_id = $this->request->post('dashboard'))
            Ajax::error("Este Dashboard no existe");
        if (is_null($this->request->post('fc')))
            Ajax::error("No existe funcion a buscar.");
        if (!$plantilla = $this->request->post('pl'))
            Ajax::error("No existe plantilla a buscar.");
        
        $funcion = $this->request->post('fc');
        
        $oReporte = ORM::factory('Redes_Reporte')->where("id", "=", $dashboard_id)->find();
        
        if (!$oReporte->loaded())
            Ajax::error("No existen dashboard para mostrar");
        
        $oItem = ORM::factory('Redes_Item')->where("id", "=", $id)->find();
        
        if (!$oItem->loaded())
            Ajax::error("No existen item para este dashboard");
        
        $oAutorizacion = $oItem->oAutorizacion;
        $oSocial       = $oAutorizacion->oSocial;
        
        //obtenemos nombre de la funcion de la tabla plantilla, del detalle que esta en json 
        $aPlantillas = $this->getPlantillaBySocial($oSocial->id);
        $oPlantilla  = $aPlantillas[$plantilla];
        $aFuncion    = json_decode($oPlantilla["detalle"], true);
        $oFuncion    = $aFuncion[$funcion];
        
        $resultado["success"] = true;
        switch ($oReporte->periodo) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                $resultado["html"] = View::factory("Dashboard/Dashboard/tipodashboard")
                    ->set("tipo", $oFuncion["tipo"])
                    ->set("descripcion", $oFuncion["descripcion"])
                    ->set("dashboard", $oReporte->tipo)
                    ->set("pl", $plantilla)
                    ->set("fc", $funcion)
                    ->set("estado", true)->render();
                break;
            default:
                $resultado["html"]    = "";
                $resultado["success"] = false;
        }
        
        $this->response->body(json_encode($resultado));
    }
    
    public function action_getReportByItem()
    {
        //validaciones
        if (!$id = $this->request->post('id'))
            Ajax::error("No existen item para este dashboard");
        if (!$dashboard_id = $this->request->post('dashboard'))
            Ajax::error("Este Dashboard no existe");
        if (is_null($this->request->post('fc')))
            Ajax::error("No existe funcion a buscar.");
        if (!$plantilla = $this->request->post('pl'))
            Ajax::error("No existe plantilla a buscar.");
        
        $funcion = $this->request->post('fc');
        
        $oReporte = ORM::factory('Redes_Reporte')->where("id", "=", $dashboard_id)->find();
        
        if (!$oReporte->loaded())
            Ajax::error("No existen dashboard para mostrar");
        
        $oItem = ORM::factory('Redes_Item')->where("id", "=", $id)->find();
        
        if (!$oItem->loaded())
            Ajax::error("No existen item para este dashboard");
        
        $oAutorizacion = $oItem->oAutorizacion;
        $oSocial       = $oAutorizacion->oSocial;
        
        //obtenemos nombre de la funcion de la tabla plantilla, del detalle que esta en json 
        $aPlantillas = $this->getPlantillaBySocial($oSocial->id);
        $oPlantilla  = $aPlantillas[$plantilla];
        $aFuncion    = json_decode($oPlantilla["detalle"], true);
        $oFuncion    = $aFuncion[$funcion];
        
        $class = "Controller_Dashboard_" . $oSocial->name;
        
        $resultado         = array();
        $resultado["tipo"] = $oFuncion["tipo"];
        $resultado["data"] = View::factory("Dashboard/Dashboard/tipodashboard")
                ->set("tipo", $oFuncion["tipo"])
                ->set("descripcion", $oFuncion["descripcion"])
                ->set("dashboard", $oReporte->tipo)
                ->set("pl", $plantilla)->set("fc", $funcion)
                ->set("data", $this->getDataByPeriodo($oReporte->periodo, $oItem, $oAutorizacion, $oFuncion, $class, $funcion, $plantilla))
                ->set("estado", true)->render();
        
        $this->response->body(json_encode($resultado));
    }
    
    
    /**
     * obtenenos la data segun periodo 
     * @param int $periodo periodo de tiempo del dashboard
     * @param obj $item de la cuenta
     * @param obj $oAutorizacion  cuenta
     * @param obj $oFuncion  lista de plantillas y funciones
     * @param string $class nombre de la clase a llamar en el getdata de cada redsocial
     * @param int $funcion indice de la funcion a llamar
     * @param int $plantilla indice de la plantilla con funciones
     * @return array
     */
    private function getDataByPeriodo($periodo, $oItem, $oAutorizacion, $oFuncion, $class, $funcion, $plantilla)
    {
        $data = array();

            switch ($periodo) {
                case 1:
                    $data = $this->periodo_diario($oItem, $oAutorizacion->access, $oFuncion, $class);
                    break;
                case 2:
                    $data = $this->periodo_semanal($oItem, $oAutorizacion->access, $oFuncion, $class);
                    break;
                case 3:
                    $data = $this->periodo_mensual($oItem, $oAutorizacion->access, $oFuncion, $class);
                    break;
                case 4:
                    $data = $this->periodo_trimestral($oItem, $oAutorizacion->access, $oFuncion, $class);
                    break;
                case 5:
                    $data = $this->periodo_anual($oItem, $oAutorizacion->access, $oFuncion, $class);
                    break;
                case 6:
                    $data = $this->periodo_pequeno($oItem, $oAutorizacion->access, $oFuncion, $class);
                    break;
                case 7:
                    $data = $this->periodo_medio($oItem, $oAutorizacion->access, $oFuncion, $class);
                    break;
                case 8:
                    $data = $this->periodo_largo($oItem, $oAutorizacion->access, $oFuncion, $class);
                    break;
                case 9:
                    $data = $this->periodo_vistaanual($oItem, $oAutorizacion->access, $oFuncion, $class);
                    break;
                case 10:
                    $data = $this->periodo_fechas($oItem, $oAutorizacion->access, $oFuncion, $class);
                    break;
            }

        $data["descripcion"] = $oFuncion["descripcion"];
        $data["tipo"]        = $oFuncion["tipo"];
        $data["fc"]          = $funcion;
        $data["pl"]          = $plantilla;
        
        return $data;
    }
    
    /**
     * 
     * @param object $item de la cuenta
     * @param json $accessos accesos de la red social
     * @param string $funcion nombre d ela funcion que se llamara en el api
     * @param string $class nombre de la clase d ela red social
     * @return array
     * 
     */
    private function periodo_diario($item, $accessos, $funcion, $class)
    {
        $lista = array();
        if (!$fecha_inicio = $this->request->post('fechainicio'))
            $fecha_inicio = date("Y-m-d");
        
        if ($funcion["tipo"] == 1) {
            for ($i = date('t', strtotime($fecha_inicio)); $i >= 1; $i--) {
                //creamos los intervalos de tiempo
                $fecha_fin    = strtotime($fecha_inicio . " 23:59:59");
                $fecha_inicio = strtotime($fecha_inicio . " 00:00:00");
                
                $textox = date("M", $fecha_inicio) . " " . date("d", $fecha_inicio);
                array_unshift($lista, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"])
                        )
                    )
                ));
                
                $fecha_inicio = date('Y-m-d', strtotime('-1 day', $fecha_inicio));
            }
            
            return array(
                "totalultimo" => $lista[count($lista) - 1]["c"][1]["v"],
                "chart" => $lista
            );
        } else if ($funcion["tipo"] == 2) {
            $fecha_fin    = strtotime($fecha_inicio . " 23:59:59");
            $fecha_inicio = strtotime($fecha_inicio . " 00:00:00");
            return array(
                "table" => Util::orderbylistandlimit12($class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]))
            );
        } else {
            
            $mes = date('t', strtotime($fecha_inicio));
            
            for ($i = $mes; $i >= 1; $i--) {
                //creamos los intervalos de tiempo
                $fecha_fin    = strtotime($fecha_inicio . " 23:59:59");
                $fecha_inicio = strtotime($fecha_inicio . " 00:00:00");
                
                $textox = date("M", $fecha_inicio) . " " . date("d", $fecha_inicio);
                $x      = 0;
                
                if ($i == 1) {
                    $x = $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin,  $funcion["funcion"]);
                }
                
                array_unshift($lista, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $x
                        )
                    )
                ));
                
                $fecha_inicio = date('Y-m-d', strtotime('-1 day', $fecha_inicio));
            }
            
            return array(
                "totalultimo" => $lista[count($lista) - 1]["c"][1]["v"],
                "chart" => $lista
            );
        }
    }
    
    /**
     * 
     * @param object $item de la cuenta
     * @param json $accessos accesos de la red social
     * @param string $funcion nombre d ela funcion que se llamara en el api
     * @param string $class nombre de la clase d ela red social
     * @return array
     * 
     */
    private function periodo_semanal($item, $accessos, $funcion, $class)
    {
        $lista = array();
        if (!$indice = $this->request->post('fechainicio'))
            $indice = 1;
        
        $fechasemanas = $this->getFechasPorSemanas();
        if (!array_key_exists($indice, $fechasemanas))
            Ajax::error("No existe semana para generar este dahboard");
        
        if ($funcion["tipo"] == 1) {
            for ($i = count($fechasemanas); $i >= $indice; $i--) {
                //creamos los intervalos de tiempo
                $fecha_inicio = strtotime($fechasemanas[$i][0] . " 00:00:00");
                $fecha_fin    = strtotime($fechasemanas[$i][1] . " 23:59:59");
                
                $textox = date("M", $fecha_inicio) . " " . date("d", $fecha_inicio) . " - " . date("M", $fecha_fin) . " " . date("d", $fecha_fin);
                array_push($lista, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"])
                        )
                    )
                ));
                
            }
            return array(
                "totalultimo" => $lista[count($lista) - 1]["c"][1]["v"],
                "chart" => $lista
            );
        } else if ($funcion["tipo"] == 2) {
            $fecha_inicio = strtotime($fechasemanas[$indice][0] . " 00:00:00");
            $fecha_fin    = strtotime($fechasemanas[$indice][1] . " 23:59:59");
            return array(
                "table" => Util::orderbylistandlimit12($class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]))
            );
        } else {
            $semanas = count($fechasemanas);
            
            for ($i = $semanas; $i >= $indice; $i--) {
                //creamos los intervalos de tiempo
                $fecha_inicio = strtotime($fechasemanas[$i][0] . " 00:00:00");
                $fecha_fin    = strtotime($fechasemanas[$i][1] . " 23:59:59");
                
                $textox = date("M", $fecha_inicio) . " " . date("d", $fecha_inicio) . " - " . date("M", $fecha_fin) . " " . date("d", $fecha_fin);
                
                $x = 0;
                if ($i == $indice) {
                    $x = $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]);
                }
                
                array_push($lista, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $x
                        )
                    )
                ));
            }
            return array(
                "totalultimo" => $lista[count($lista) - 1]["c"][1]["v"],
                "chart" => $lista
            );
        }
    }
    
    /**
     * 
     * @param object $item de la cuenta
     * @param json $accessos accesos de la red social
     * @param string $funcion nombre d ela funcion que se llamara en el api
     * @param string $class nombre de la clase d ela red social
     * @return array
     * 
     */
    private function periodo_mensual($item, $accessos, $funcion, $class)
    {
        $lista = array();
        if (!$indice = $this->request->post('fechainicio'))
            $indice = 1;
        
        $fechames = $this->getFechasPorMes();
        if (!array_key_exists($indice, $fechames))
            Ajax::error("No existe mes para generar este dahboard");
        
        if ($funcion["tipo"] == 1) {
            for ($i = count($fechames); $i >= $indice; $i--) //creamos los intervalos de tiempo
            {
                $fecha_inicio = strtotime($fechames[$i][0] . " 00:00:00");
                $fecha_fin    = strtotime($fechames[$i][1] . " 23:59:59");
                
                $textox = date("M", $fecha_inicio) . " " . date("d", $fecha_inicio) . " - " . date("M", $fecha_fin) . " " . date("d", $fecha_fin);
                array_push($lista, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"])
                        )
                    )
                ));
            }
            return array(
                "totalultimo" => $lista[count($lista) - 1]["c"][1]["v"],
                "chart" => $lista
            );
        } else if ($funcion["tipo"] == 2) {
            $fecha_inicio = strtotime($fechames[$indice][0] . " 00:00:00");
            $fecha_fin    = strtotime($fechames[$indice][1] . " 23:59:59");
            return array(
                "table" => Util::orderbylistandlimit12($class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]))
            );
        } else {
            
            $mes = count($fechames);
            
            for ($i = $mes; $i >= $indice; $i--) //creamos los intervalos de tiempo
                {
                $fecha_inicio = strtotime($fechames[$i][0] . " 00:00:00");
                $fecha_fin    = strtotime($fechames[$i][1] . " 23:59:59");
                
                $textox = date("M", $fecha_inicio) . " " . date("d", $fecha_inicio) . " - " . date("M", $fecha_fin) . " " . date("d", $fecha_fin);
                
                $x = 0;
                
                if ($i == $indice) {
                    $x = $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]);
                }
                
                array_push($lista, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $x
                        )
                    )
                ));
            }
            return array(
                "totalultimo" => $lista[count($lista) - 1]["c"][1]["v"],
                "chart" => $lista
            );
        }
    }
    
    /**
     * 
     * @param object $item de la cuenta
     * @param json $accessos accesos de la red social
     * @param string $funcion nombre d ela funcion que se llamara en el api
     * @param string $class nombre de la clase d ela red social
     * @return array
     * 
     */
    private function periodo_trimestral($item, $accessos, $funcion, $class)
    {
        $lista = array();
        if (!$indice = $this->request->post('fechainicio'))
            $indice = 1;
        
        $fechatrimestre = $this->getFechasPorTrimestre();
        if (!array_key_exists($indice, $fechatrimestre))
            Ajax::error("No existe trimestre para generar este dahboard");
        
        
        
        if ($funcion["tipo"] == 1) {
            
            
            for ($i = count($fechatrimestre); $i >= $indice; $i--) {
                //creamos los intervalos de tiempo
                
                
                
                $fecha_inicio = strtotime($fechatrimestre[$i][0] . " 00:00:00");
                $fecha_fin    = strtotime($fechatrimestre[$i][1] . " 23:59:59");
                
                $textox = date("M", $fecha_inicio) . " " . date("y", $fecha_inicio) . " - " . date("M", $fecha_fin) . " " . date("y", $fecha_fin);
                array_push($lista, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"])
                        )
                    )
                ));
            }
            
            return array(
                "totalultimo" => $lista[count($lista) - 1]["c"][1]["v"],
                "chart" => $lista
            );
        } else if ($funcion["tipo"] == 2) {
            $fecha_inicio = strtotime($fechatrimestre[$indice][0] . " 00:00:00");
            $fecha_fin    = strtotime($fechatrimestre[$indice][1] . " 23:59:59");
            return array(
                "table" => Util::orderbylistandlimit12($class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]))
            );
        } else {
            
            $trimestre = count($fechatrimestre);
            
            for ($i = $trimestre; $i >= $indice; $i--) {
                //creamos los intervalos de tiempo
                $fecha_inicio = strtotime($fechatrimestre[$i][0] . " 00:00:00");
                $fecha_fin    = strtotime($fechatrimestre[$i][1] . " 23:59:59");
                
                $textox = date("M", $fecha_inicio) . " " . date("y", $fecha_inicio) . " - " . date("M", $fecha_fin) . " " . date("y", $fecha_fin);
                
                $x = 0;
                if ($i == $indice) {
                    $x = $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]);
                }
                
                array_push($lista, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $x
                        )
                    )
                ));
            }
            
            return array(
                "totalultimo" => $lista[count($lista) - 1]["c"][1]["v"],
                "chart" => $lista
            );
        }
    }
    
    /**
     * 
     * @param object $item de la cuenta
     * @param json $accessos accesos de la red social
     * @param string $funcion nombre d ela funcion que se llamara en el api
     * @param string $class nombre de la clase d ela red social
     * @return array
     * 
     */
    private function periodo_anual($item, $accessos, $funcion, $class)
    {
        $lista = array();
        if (!$indice = $this->request->post('fechainicio'))
            $indice = 1;
        
        $fechaanio = $this->getFechasPorAnio();
        if (!array_key_exists($indice, $fechaanio))
            Ajax::error("No existe año para generar este dahboard");
        
        // if($funcion["tipo"] ==1)
        // {
        //creamos los intervalos de tiempo
        $mes      = $fechaanio[$indice][1];
        $listames = $this->getFechasPorMes($mes);
        $anio     = count($listames);
        
        for ($j = $anio; $j >= 1; $j--) {
            $fecha_inicio = strtotime($listames[$j][0] . " 00:00:00");
            $fecha_fin    = strtotime($listames[$j][1] . " 23:59:59");
            
            $textox = date("M", $fecha_inicio) . " " . date("y", $fecha_inicio);
            
            if ($funcion["tipo"] == 1) {
                array_push($lista, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"])
                        )
                    )
                ));
            } else if ($funcion["tipo"] == 2) {
                $lista = array_merge($lista, $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]));
            } else {
                
                $x = 0;
                if ($j == 1) {
                    $x = $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]);
                }
                
                array_push($lista, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $x
                        )
                    )
                ));
            }
        }
        
        
        if ($funcion["tipo"] == 1 or $funcion["tipo"] == 3) {
            $sumatoria = 0;
            
            foreach ($lista as $value) {
                $sumatoria += $value["c"][1]["v"];
            }
            
            return array(
                "totalultimo" => $sumatoria,
                "chart" => $lista
            );
        } else {
            return array(
                "table" => Util::orderbylistandlimit12($lista)
            );
        }
    }
    
    /**
     * 
     * @param int $canti dias
     * @param date $hoy fecha actual
     * @param boolean $literal
     * @return array intervalo fecha inicio y fecha fin
     */
    private function getFechaByCantiDias($canti = null, $hoy = null, $literal = FALSE)
    {
        $lista = array();
        
        if ($hoy == null) {
            $hoy = date("Y-m-d");
        }
        
        if ($canti == null) {
            $texto = "-1 day";
        } else if ($canti == 7) {
            $texto = "-1 week";
        } else if ($canti == 15) {
            $texto = "-2 week";
        } else if (($canti == 31 or $canti == 30 or $canti == 29 or $canti == 28) and $literal == FALSE) {
            $texto = "-1 month";
        } else if ($canti == 90) {
            $texto = "-3 month";
        } else {
            $texto = "-" . $canti . " day";
        }
        
        return array(
            0 => date('Y-m-d', strtotime($texto, strtotime($hoy))),
            1 => $hoy
        );
    }
    
    /**
     * 
     * @param object $item de la cuenta
     * @param json $accessos accesos de la red social
     * @param string $funcion nombre d ela funcion que se llamara en el api
     * @param string $class nombre de la clase d ela red social
     * @return array
     * 
     */
    private function periodo_pequeno($item, $accessos, $funcion, $class)
    {
        $lista                         = array();
        $tipo1                         = $this->getFechaByCantiDias(15);
        $lista["dias"]["dia1"]         = 15;
        $lista["periodos"]["columna1"] = $class::getdata($item, $accessos, strtotime($tipo1[0] . " 00:00:00"), strtotime($tipo1[1] . " 00:00:00"), $funcion["funcion"]);
        
        $tipo2                         = $this->getFechaByCantiDias(7);
        $lista["dias"]["dia2"]         = 7;
        $lista["periodos"]["columna2"] = $class::getdata($item, $accessos, strtotime($tipo2[0] . " 00:00:00"), strtotime($tipo2[1] . " 00:00:00"), $funcion["funcion"]);
        
        $tipo3                         = $this->getFechaByCantiDias();
        $lista["dias"]["dia3"]         = 1;
        $lista["periodos"]["columna3"] = $class::getdata($item, $accessos, strtotime($tipo3[0] . " 00:00:00"), strtotime($tipo3[1] . " 00:00:00"), $funcion["funcion"]);
        
        if ($funcion["tipo"] == 1) {
            $chart = array();
            for ($i = 1; $i < 16; $i++) {
                $fecha_inicio = strtotime($tipo3[0] . " 00:00:00");
                $fecha_fin    = strtotime($tipo3[1] . " 00:00:00");
                
                $textox = date("M", $fecha_inicio) . " " . date("d", $fecha_inicio);
                
                array_unshift($chart, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"])
                        )
                    )
                ));
                
                $tipo3 = $this->getFechaByCantiDias(null, $tipo3[0]);
            }
            
            $lista["chart"] = $chart;
        } else if ($funcion["tipo"] == 2) {
            $lista["periodos"]["columna1"] = array_slice(Util::array_msort($lista["periodos"]["columna1"], array(
                'cantidad' => SORT_DESC
            )), 0, 12);
            $lista["periodos"]["columna2"] = array_slice(Util::array_msort($lista["periodos"]["columna2"], array(
                'cantidad' => SORT_DESC
            )), 0, 12);
            $lista["periodos"]["columna3"] = array_slice(Util::array_msort($lista["periodos"]["columna3"], array(
                'cantidad' => SORT_DESC
            )), 0, 12);
            
        } else {
            $chart = array();
            for ($i = 1; $i < 16; $i++) {
                $fecha_inicio = strtotime($tipo3[0] . " 00:00:00");
                $fecha_fin    = strtotime($tipo3[1] . " 00:00:00");
                
                $textox = date("M", $fecha_inicio) . " " . date("d", $fecha_inicio);
                
                $x = 0;
                
                if ($i == 15) {
                    $x = $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]);
                }
                
                array_unshift($chart, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $x
                        )
                    )
                ));
                
                $tipo3 = $this->getFechaByCantiDias(null, $tipo3[0]);
            }
            
            $lista["chart"] = $chart;
        }
        
        return $lista;
    }
    
    
    /**
     * 
     * @param object $item de la cuenta
     * @param json $accessos accesos de la red social
     * @param string $funcion nombre d ela funcion que se llamara en el api
     * @param string $class nombre de la clase d ela red social
     * @return array
     * 
     */
    private function periodo_medio($item, $accessos, $funcion, $class)
    {
        $lista                         = array();
        $tipo1                         = $this->getFechaByCantiDias(30);
        $lista["dias"]["dia1"]         = 30;
        $lista["periodos"]["columna1"] = $class::getdata($item, $accessos, strtotime($tipo1[0] . " 00:00:00"), strtotime($tipo1[1] . " 00:00:00"), $funcion["funcion"]);
        
        $tipo2                         = $this->getFechaByCantiDias(15);
        $lista["dias"]["dia2"]         = 15;
        $lista["periodos"]["columna2"] = $class::getdata($item, $accessos, strtotime($tipo2[0] . " 00:00:00"), strtotime($tipo2[1] . " 00:00:00"), $funcion["funcion"]);
        
        $tipo3                         = $this->getFechaByCantiDias(7);
        $lista["dias"]["dia3"]         = 7;
        $lista["periodos"]["columna3"] = $class::getdata($item, $accessos, strtotime($tipo3[0] . " 00:00:00"), strtotime($tipo3[1] . " 00:00:00"), $funcion["funcion"]);
        
        
        $tipo3 = $this->getFechaByCantiDias();
        if ($funcion["tipo"] == 1) {
            $chart        = array();
            $cantidiasmes = date('t', strtotime($tipo3[1]));
            
            for ($i = 1; $i < $cantidiasmes + 1; $i++) {
                $fecha_inicio = strtotime($tipo3[0] . " 00:00:00");
                $fecha_fin    = strtotime($tipo3[1] . " 00:00:00");
                
                $textox = date("M", $fecha_inicio) . " " . date("d", $fecha_inicio);
                
                array_unshift($chart, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"])
                        )
                    )
                ));
                
                $tipo3 = $this->getFechaByCantiDias(null, $tipo3[0]);
            }
            
            $lista["chart"] = $chart;
        } else if ($funcion["tipo"] == 2) {
            $lista["periodos"]["columna1"] = array_slice(Util::array_msort($lista["periodos"]["columna1"], array(
                'cantidad' => SORT_DESC
            )), 0, 12);
            $lista["periodos"]["columna2"] = array_slice(Util::array_msort($lista["periodos"]["columna2"], array(
                'cantidad' => SORT_DESC
            )), 0, 12);
            $lista["periodos"]["columna3"] = array_slice(Util::array_msort($lista["periodos"]["columna3"], array(
                'cantidad' => SORT_DESC
            )), 0, 12);
            
        } else {
            
            $chart        = array();
            $cantidiasmes = date('t', strtotime($tipo3[1]));
            
            for ($i = 1; $i < $cantidiasmes + 1; $i++) {
                $fecha_inicio = strtotime($tipo3[0] . " 00:00:00");
                $fecha_fin    = strtotime($tipo3[1] . " 00:00:00");
                
                $textox = date("M", $fecha_inicio) . " " . date("d", $fecha_inicio);
                
                $x = 0;
                
                if ($i == $cantidiasmes) {
                    $x = $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]);
                }
                
                array_unshift($chart, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $x
                        )
                    )
                ));
                
                $tipo3 = $this->getFechaByCantiDias(null, $tipo3[0]);
            }
            
            $lista["chart"] = $chart;
        }
        
        return $lista;
    }
    
    /**
     * 
     * @param object $item de la cuenta
     * @param json $accessos accesos de la red social
     * @param string $funcion nombre d ela funcion que se llamara en el api
     * @param string $class nombre de la clase d ela red social
     * @return array
     * 
     */
    private function periodo_largo($item, $accessos, $funcion, $class)
    {
        $lista                         = array();
        $tipo1                         = $this->getFechaByCantiDias(90);
        $lista["dias"]["dia1"]         = 90;
        $lista["periodos"]["columna1"] = $class::getdata($item, $accessos, strtotime($tipo1[0] . " 00:00:00"), strtotime($tipo1[1] . " 00:00:00"), $funcion["funcion"]);
        
        $tipo2                         = $this->getFechaByCantiDias(30);
        $lista["dias"]["dia2"]         = 30;
        $lista["periodos"]["columna2"] = $class::getdata($item, $accessos, strtotime($tipo2[0] . " 00:00:00"), strtotime($tipo2[1] . " 00:00:00"), $funcion["funcion"]);
        
        $tipo3                         = $this->getFechaByCantiDias(7);
        $lista["dias"]["dia3"]         = 7;
        $lista["periodos"]["columna3"] = $class::getdata($item, $accessos, strtotime($tipo3[0] . " 00:00:00"), strtotime($tipo3[1] . " 00:00:00"), $funcion["funcion"]);
        
        if ($funcion["tipo"] == 1) {
            $chart = array();
            
            for ($i = 1; $i < 13; $i++) {
                $fecha_inicio = strtotime($tipo3[0] . " 00:00:00");
                $fecha_fin    = strtotime($tipo3[1] . " 00:00:00");
                
                $textox = date("M", $fecha_inicio) . " " . date("d", $fecha_inicio);
                
                array_unshift($chart, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"])
                        )
                    )
                ));
                
                $tipo3 = $this->getFechaByCantiDias("7", $tipo3[0]);
            }
            
            $lista["chart"] = $chart;
        } else if ($funcion["tipo"] == 2) {
            $lista["periodos"]["columna1"] = array_slice(Util::array_msort($lista["periodos"]["columna1"], array(
                'cantidad' => SORT_DESC
            )), 0, 12);
            $lista["periodos"]["columna2"] = array_slice(Util::array_msort($lista["periodos"]["columna2"], array(
                'cantidad' => SORT_DESC
            )), 0, 12);
            $lista["periodos"]["columna3"] = array_slice(Util::array_msort($lista["periodos"]["columna3"], array(
                'cantidad' => SORT_DESC
            )), 0, 12);
        } else {
            
            $chart = array();
            
            for ($i = 1; $i < 13; $i++) {
                $fecha_inicio = strtotime($tipo3[0] . " 00:00:00");
                $fecha_fin    = strtotime($tipo3[1] . " 00:00:00");
                
                $textox = date("M", $fecha_inicio) . " " . date("d", $fecha_inicio);
                
                $x = 0;
                
                if ($i == 12) {
                    $x = $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]);
                }
                
                array_unshift($chart, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $x
                        )
                    )
                ));
                
                $tipo3 = $this->getFechaByCantiDias("7", $tipo3[0]);
            }
            
            $lista["chart"] = $chart;
        }
        
        return $lista;
    }
    
    /**
     * 
     * @param object $item de la cuenta
     * @param json $accessos accesos de la red social
     * @param string $funcion nombre d ela funcion que se llamara en el api
     * @param string $class nombre de la clase d ela red social
     * @return array
     * 
     */
    private function periodo_vistaanual($item, $accessos, $funcion, $class)
    {
        $lista = array();
        
        $tipo1       = $this->getFechaByCantiDias(90);
        $canti360    = 0;
        $canti180    = 0;
        $lista360    = array();
        $lista180    = array();
        $auxarray360 = array();
        $auxarray180 = array();
        
        for ($i = 1; $i < 5; $i++) {
            $fecha_inicio = strtotime($tipo1[0] . " 00:00:00");
            $fecha_fin    = strtotime($tipo1[1] . " 00:00:00");
            if ($funcion["tipo"] == 1) {
                if ($funcion["funcion"] == "page_fans") {
                    array_unshift($auxarray360, $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]));
                } else {
                    $canti360 += $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]);
                }
            } else {
                $lista360 = array_merge($lista360, $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]));
            }
            
            $tipo1 = $this->getFechaByCantiDias(90, $tipo1[0]);
        }
        
        if ($funcion["funcion"] == "page_fans") {
            $canti360 = $auxarray360[0];
        }
        
        $tipo2 = $this->getFechaByCantiDias(90);
        for ($i = 1; $i < 3; $i++) {
            $fecha_inicio = strtotime($tipo2[0] . " 00:00:00");
            $fecha_fin    = strtotime($tipo2[1] . " 00:00:00");
            if ($funcion["tipo"] == 1) {
                if ($funcion["funcion"] == "page_fans") {
                    array_unshift($auxarray180, $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]));
                } else {
                    $canti180 += $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]);
                }
            } else {
                $lista180 = array_merge($lista180, $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]));
            }
            
            $tipo2 = $this->getFechaByCantiDias(90, $tipo2[0]);
        }
        
        if ($funcion["funcion"] == "page_fans") {
            $canti180 = $auxarray180[0];
        }
        
        $lista["dias"]["dia1"] = 360;
        $lista["dias"]["dia2"] = 180;
        $lista["dias"]["dia3"] = 30;
        if ($funcion["tipo"] == 1) {
            $lista["periodos"]["columna1"] = $canti360;
            $lista["periodos"]["columna2"] = $canti180;
            
            $tipo3                         = $this->getFechaByCantiDias(30);
            $lista["periodos"]["columna3"] = $class::getdata($item, $accessos, strtotime($tipo3[0] . " 00:00:00"), strtotime($tipo3[1] . " 00:00:00"), $funcion["funcion"]);
            
            $chart = array();
            
            for ($i = 1; $i < 13; $i++) {
                $fecha_inicio = strtotime($tipo3[0] . " 00:00:00");
                $fecha_fin    = strtotime($tipo3[1] . " 00:00:00");
                
                $textox = date("M", $fecha_inicio) . " " . date("d", $fecha_inicio);
                array_unshift($chart, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"])
                        )
                    )
                ));
                
                $tipo3 = $this->getFechaByCantiDias(30, $tipo3[0]);
            }
            
            $lista["chart"] = $chart;
        } else if ($funcion["tipo"] == 2) {
            $lista["periodos"]["columna1"] = array_slice(Util::array_msort($lista360, array(
                'cantidad' => SORT_DESC
            )), 0, 12);
            $lista["periodos"]["columna2"] = array_slice(Util::array_msort($lista180, array(
                'cantidad' => SORT_DESC
            )), 0, 12);
            
            $tipo3                         = $this->getFechaByCantiDias(30);
            $lista["periodos"]["columna3"] = ($class::getdata($item, $accessos, strtotime($tipo3[0] . " 00:00:00"), strtotime($tipo3[1] . " 00:00:00"), $funcion["funcion"]));
            $lista["periodos"]["columna3"] = array_slice(Util::array_msort($lista["periodos"]["columna3"], array(
                'cantidad' => SORT_DESC
            )), 0, 12);
            
        } else {
            $lista["periodos"]["columna1"] = $canti360;
            $lista["periodos"]["columna2"] = $canti180;
            
            $tipo3                         = $this->getFechaByCantiDias(30);
            $lista["periodos"]["columna3"] = $class::getdata($item, $accessos, strtotime($tipo3[0] . " 00:00:00"), strtotime($tipo3[1] . " 00:00:00"), $funcion["funcion"]);
            
            $chart = array();
            
            for ($i = 1; $i < 13; $i++) {
                $fecha_inicio = strtotime($tipo3[0] . " 00:00:00");
                $fecha_fin    = strtotime($tipo3[1] . " 00:00:00");
                
                $textox = date("M", $fecha_inicio) . " " . date("d", $fecha_inicio);
                
                $X = 0;
                
                if ($i == 12) {
                    $X = $class::getdata($item, $accessos, $fecha_inicio, $fecha_fin, $funcion["funcion"]);
                }
                
                array_unshift($chart, array(
                    "c" => array(
                        array(
                            "v" => $textox
                        ),
                        array(
                            "v" => $X
                        )
                    )
                ));
                
                $tipo3 = $this->getFechaByCantiDias(30, $tipo3[0]);
            }
            
            $lista["chart"] = $chart;
        }
        
        return $lista;
    }
    
    /**
     * 
     * @param object $item de la cuenta
     * @param json $accessos accesos de la red social
     * @param string $funcion nombre d ela funcion que se llamara en el api
     * @param string $class nombre de la clase d ela red social
     * @return array
     * 
     */
    private function periodo_fechas($item, $accessos, $funcion, $class)
    {
        $lista = array();
        if (!$fechainicio = $this->request->post('fechainicio'))
            Ajax::error("No existe fecha inicio para generar este dahboard");
        if (!$fechafin = $this->request->post('fechafin'))
            Ajax::error("No existe fecha fin para generar este dahboard");
        
        if ($fechainicioopcional = $this->request->post('fechainicioopcional')) {
            if (!strtotime($fechainicioopcional))
                Ajax::error("Fecha de inicio opcional no valida");
        }
        
        if ($fechafinopcional = $this->request->post('fechafinopcional')) {
            
            if (!strtotime($fechafinopcional))
                Ajax::error("Fecha de fin opcional no valida");
        }
        
        $lista[0] = $this->getListByFecha($fechainicio, $fechafin, $item, $accessos, $funcion, $class);
        $lista[1] = null;
        if ((isset($fechainicioopcional) and !empty($fechafinopcional) and isset($fechafinopcional) and !empty($fechafinopcional)) and $funcion["tipo"] == 1) {
            $lista[1] = $this->getListByFecha($fechainicioopcional, $fechafinopcional, $item, $accessos, $funcion, $class);
        }
        
        if ($funcion["tipo"] == 1) {
            if (!is_null($lista[1])) {
                $aux = 0;
                if ($lista[1] > 0) {
                    $aux = ($lista[0] * 100 / $lista[1]) - 100;
                }
                
                $lista["porcentaje"] = floor($aux) . "%";
            } else {
                $lista["porcentaje"] = "N/A";
            }
        }
        
        return $lista;
    }
    
    /**
     * 
     * @param date $fechainicio
     * @param date $fechafin
     * @param object $item de la cuenta
     * @param json $accessos accesos de la red social
     * @param string $funcion nombre d ela funcion que se llamara en el api
     * @param string $class nombre de la clase d ela red social
     * @return array data
     */
    private function getListByFecha($fechainicio, $fechafin, $item, $accessos, $funcion, $class)
    {
        $segundos_diferencia = strtotime($fechafin) - strtotime($fechainicio);
        $dias_diferencia     = abs(floor($segundos_diferencia / (60 * 60 * 24)));
        
        //obtengo el valor absoulto de los días (quito el posible signo negativo) 
        return $this->getListByFechaRecursive($dias_diferencia, array(), array(), 0, $fechafin, $item, $accessos, $funcion, $class);
    }
    
    /**
     * funcion recursiva para obtener data en fechas especificas
     * @param int $dias_diferencia
     * @param array $auxarray lista auxiliar para caso de cantidad de fans
     * @param array $listaanio lista  total de data
     * @param int $canti
     * @param date $fechafin
     * @param object $item
     * @param json $accessos
     * @param string $funcion nombre d ela funcion que se llamara en el api
     * @param string $class nombre de la clase d ela red social
     * @return array data
     */
    private function getListByFechaRecursive($dias_diferencia, $auxarray = array(), $listaanio = array(), $canti = 0, $fechafin, $item, $accessos, $funcion, $class)
    {
        $cociente    = 1;
        $residuo     = 0;
        $fechainicio = null;
        
        if ($dias_diferencia >= 90) {
            $dias     = 90;
            $cociente = floor($dias_diferencia / 90);
            $residuo  = $dias_diferencia % 90;
        } else if ($dias_diferencia >= 30 and $dias_diferencia < 90) {
            $dias     = 30;
            $cociente = floor($dias_diferencia / 30);
            $residuo  = $dias_diferencia % 30;
        } else if ($dias_diferencia >= 15 and $dias_diferencia < 30) {
            $dias     = 15;
            $cociente = floor($dias_diferencia / 15);
            $residuo  = $dias_diferencia % 15;
        } else if ($dias_diferencia >= 7 and $dias_diferencia < 15) {
            $dias     = 7;
            $cociente = floor($dias_diferencia / 7);
            $residuo  = $dias_diferencia % 7;
        } else {
            $dias     = 1;
            $cociente = $dias_diferencia;
        }
        
        $tipo1 = $this->getFechaByCantiDias($dias, date('Y-m-d', strtotime($fechafin)), TRUE);
        
        for ($i = 1; $i < ($cociente + 1); $i++) {
            $fechainicio = strtotime($tipo1[0] . " 00:00:00");
            $fechafin    = strtotime($tipo1[1] . " 00:00:00");
            
            if ($funcion["tipo"] == 1) {
                if ($funcion["funcion"] == "page_fans") {
                    array_unshift($auxarray, $class::getdata($item, $accessos, $fechainicio, $fechafin, $funcion["funcion"]));
                } else {
                    $canti += $class::getdata($item, $accessos, $fechainicio, $fechafin, $funcion["funcion"]);
                }
            } else if ($funcion["tipo"] == 2) {
                $listaanio = array_merge($listaanio, $class::getdata($item, $accessos, $fechainicio, $fechafin, $funcion["funcion"]));
            } else {
                if ($i = $cociente) {
                    array_unshift($auxarray, $class::getdata($item, $accessos, $fechainicio, $fechafin, $funcion["funcion"]));
                } else {
                    array_unshift($auxarray, 0);
                }
            }
            
            $tipo1 = $this->getFechaByCantiDias($dias, date('Y-m-d', strtotime($tipo1[0])));
        }
        
        
        if ($residuo == 0) {
            if ($funcion["funcion"] == "page_fans") {
                $canti = $auxarray[0];
            }
            
            if ($funcion["tipo"] == 1) {
                return $canti;
            } else if ($funcion["tipo"] == 2) {
                return Util::orderbylistandlimit12($listaanio);
            } else {
                return $auxarray[0];
            }
        } else {
            return $this->getLIstByFechaRecursive($residuo, $auxarray, $listaanio, $canti, date('Y-m-d', $fechainicio), $item, $accessos, $funcion, $class);
        }
    }
}