<?php defined('SYSPATH') or die('No direct access allowed.');

require_once Kohana::find_file("vendor","instagram/Instagram"); 

class Controller_Dashboard_Instagram extends Controller_Dashboard_Template
{
    public function action_index()
    {
        $instagram =  new Instagram(array(
            'apiKey' => $this->in_app_key,
            'apiSecret' => $this->in_app_secret,
            'apiCallback' => $this->urlredireccionapis() // must point to success.php
        ));
        
        $this->redirect($instagram->getLoginUrl());
    }
    
    public function action_setdata()
    {

        if($this->request->query("error"))
        {
            $this->redirect("/dashboard/home/editarautorizacion/?error=error");
        }
        
        $id = $this->request->param('id');

        $instagram =  new Instagram(array(
            'apiKey' => $this->in_app_key,
            'apiSecret' => $this->in_app_secret,
            'apiCallback' => $this->urlredireccionapis() // must point to success.php
        ));
        
        $inst = $instagram->getOAuthToken($this->request->query("code"));
       
        $oUser =  $this->oUser;

        $aAautorizacion = ORM::factory("Redes_Autorizacion")
                            ->where("social_id","=",$id)
                            ->where("user_id","=",$oUser->id)
                            ->where("uId","=",$inst->user->id)
                            ->find();
        
        if($aAautorizacion->loaded())
        {
            if($aAautorizacion->status == 0)
            {
               $aAautorizacion->status = 1; 
            }
            
            $aAautorizacion->access = json_encode($inst) ;
            $aAautorizacion->update_at = date("Y-m-d H:i:s");

        }else{
            $aAautorizacion->social_id = $id;
            $aAautorizacion->user_id = $oUser->id;
            $aAautorizacion->access = json_encode($inst) ;
            $aAautorizacion->created_at = date("Y-m-d H:i:s");
            $aAautorizacion->uId = $inst->user->id;
        }
        
        $aAautorizacion->username = $inst->user->username;  
        $aAautorizacion->save();
        
        $query = DB::update('redes_item')
                ->set(array('status' => '0'))
                ->where('autorizacion_id', '=', $aAautorizacion->id);
        $query->execute();


        $oItem = ORM::factory("Redes_Item")
                ->where("uId","=", $inst->user->id)
                ->where("autorizacion_id","=",$aAautorizacion->id)
                ->find();

        if($oItem->loaded())
        {
            $oItem->update_at = date("Y-m-d H:i:s");
            $oItem->status = 1;
        }else
        {
            $oItem->uId = $inst->user->id;
            $oItem->created_at = date("Y-m-d H:i:s");
            $oItem->autorizacion_id = $aAautorizacion->id;
        }
            
        $oItem->descripcion = $inst->user->username;
        $oItem->save();
        
        $this->redirectbyprofile();
    }
    
    public static function getdata($item,$accesos, $fecha_inicio, $fecha_fin, $funcion) 
    {
        $access_token = json_decode($accesos, true);
        $access_token = $access_token["access_token"];
        
        try{
        
            return self::$funcion($item->uId,$fecha_inicio,$fecha_fin,$access_token);
        } catch (Exception $exc) {
            Ajax::error($exc->getTraceAsString());
        }

    }
    
    public static function likes($item,$fecha_inicio,$fecha_fin,$access_token)
    {
       $obj =  file_get_contents("https://api.instagram.com/v1/users/".$item."/media/recent/?max_timestamp=".$fecha_fin."&min_timestamp=".$fecha_inicio."&access_token=".$access_token);
    
       print_r(json_decode($obj, true));
       die();
       
    }
    
    public static function comments($item,$fecha_inicio,$fecha_fin,$access_token)
    {
        $url = "https://api.instagram.com/v1/users/".$item."/media/recent/?max_timestamp=".$fecha_fin."&count=100&min_timestamp=".$fecha_inicio."&access_token=".$access_token;

        die($url);
        
        $obj =  file_get_contents($url);
    
       print_r(json_decode($obj, true));
       die();
    }
    
    public static function media($item,$fecha_inicio,$fecha_fin,$access_token)
    {
       $obj =  file_get_contents("https://api.instagram.com/v1/users/".$item."/media/recent/?max_timestamp=".$fecha_fin."&min_timestamp=".$fecha_inicio."&access_token=".$access_token);

       print_r(json_decode($obj, true));
       die();
    }
}