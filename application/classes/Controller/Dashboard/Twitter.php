<?php defined('SYSPATH') or die('No direct access allowed.');

require_once Kohana::find_file("vendor","twitter/TwitterOAuth"); 

class Controller_Dashboard_Twitter extends Controller_Dashboard_Template
{
    public function action_index()
    {
        $this->twitter = new TwitterOAuth($this->tw_app_key,$this->tw_app_secret);
        $this->tw_request_token =  $this->twitter->oauth('oauth/request_token',array('oauth_callback' => $this->urlredireccionapis()));
        $url = $this->twitter->url('oauth/authorize', array('oauth_token' => $this->tw_request_token['oauth_token']));
        Session::instance()->set('oauth_token',$this->tw_request_token['oauth_token']);
        Session::instance()->set('oauth_token_secret',  $this->tw_request_token['oauth_token_secret']); 
        $this->redirect($url);
    }
    
    public function action_setdata()
    {
        if($this->request->query("denied"))
        {
            $this->redirect("/dashboard/home/editarautorizacion/?error=error");
        }

        $id = $this->request->param('id');
        $oUser =  $this->oUser;
        
        if(!Session::instance()->get('oauth_token_secret'))
        {
            $this->redirect("/dashboard/home/editarautorizacion/?error=error");
        }
        
        $this->twitter = new TwitterOAuth($this->tw_app_key,$this->tw_app_secret,$this->request->query("oauth_token"),Session::instance()->get('oauth_token_secret')); 
        $this->tw_access_token = $this->twitter->oauth("oauth/access_token", array("oauth_verifier" => trim($this->request->query("oauth_verifier"))));  
        $this->tw_access_token["app_key"] = $this->tw_app_key;
        $this->tw_access_token["app_secret"] = $this->tw_app_secret;
        
        $aAautorizacion = ORM::factory("Redes_Autorizacion")
                            ->where("social_id","=",$id)
                            ->where("user_id","=",$oUser->id)
                            ->where("uId","=",$this->tw_access_token["user_id"])

                            ->find();
        
        if($aAautorizacion->loaded())
        {
            
            if($aAautorizacion->status == 0)
            {
               $aAautorizacion->status = 1; 
            }
            $aAautorizacion->access = json_encode($this->tw_access_token) ;
            $aAautorizacion->update_at = date("Y-m-d H:i:s");

        }else{
            $aAautorizacion->social_id = $id;
            $aAautorizacion->user_id = $oUser->id;
            $aAautorizacion->access = json_encode($this->tw_access_token) ;
            $aAautorizacion->created_at = date("Y-m-d H:i:s");
            $aAautorizacion->uId = $this->tw_access_token["user_id"];
        }

        $aAautorizacion->username = $this->tw_access_token["screen_name"];  
        $aAautorizacion->save();

        $query = DB::update('redes_item')
                ->set(array('status' => '0'))
                ->where('autorizacion_id', '=', $aAautorizacion->id);
        $query->execute();


        $oItem = ORM::factory("Redes_Item")
                ->where("uId","=",$this->tw_access_token["user_id"])
                ->where("autorizacion_id","=",$aAautorizacion->id)
                ->find();

        if($oItem->loaded())
        {
            $oItem->update_at = date("Y-m-d H:i:s");
            $oItem->status = 1;
        }else
        {
            $oItem->uId = $this->tw_access_token["user_id"];
            $oItem->created_at = date("Y-m-d H:i:s");
            $oItem->autorizacion_id = $aAautorizacion->id;
        }
            
        $oItem->descripcion = $this->tw_access_token["screen_name"];
        $oItem->save();
        
        $this->redirectbyprofile();
        
    }
    
    public static function getdata($item,$accesos, $fecha_inicio, $fecha_fin, $funcion) 
    {
      //  $access_token = json_decode($accesos, true);

      //  $twitter = new TwitterOAuth($access_token['app_key'],$access_token['app_secret'],$access_token['oauth_token'],$access_token['oauth_token_secret']); 
        try{
            return self::$funcion($item->date_time,$fecha_inicio,$fecha_fin);
        } catch (Exception $exc) {
            Ajax::error($exc->getTraceAsString());
        }

    }
    
    /*public static function check_in_range($start_ts, $end_ts, $date_from_user)
    {
      // Convert to timestamp
      date_default_timezone_set('America/Lima');
      $user_ts = strtotime($date_from_user);

      // Check that user date is between start & end
      if(($user_ts >= $start_ts) and ($user_ts <= $end_ts)) 
      {
          return 1;
      }else if ($user_ts > $end_ts)
      {
          return 2;
      }else {
          return 3;
      }
    }
    
    private static function twrecursivorango($tweets,$parametros,$fecha_desde, $fecha_hasta, $user)
    {
        $total = count($tweets);
        $inicio = 0;
        $aux = array();

        foreach($tweets as $tweet)
        {
            $inicio ++;
            $fecha_tweet = date('Y-m-d H:i:s', strtotime($tweet->created_at));
            
            if(self::check_in_range($fecha_desde,$fecha_hasta,$fecha_tweet) == 1)
            {
                array_push($this->lista_fechas, array("cantidad" => $tweet->retweet_count, "texto" => $tweet->text)); 
            }else if(self::check_in_range($fecha_desde,$fecha_hasta,$fecha_tweet) == 3){
                return $this->lista_fechas;
            }

            if($total == $inicio)
            {
                $parametros["max_id"] = $tweet->id_str;
                $tweets = $user->get("statuses/user_timeline", $parametros); 
                
                return array_merge($this->lista_fechas,self::twrecursivorango($tweets,$parametros,$fecha_desde, $fecha_hasta,$user));  
            }
        }          
    }*/
    
    /*private static function top_retweets($retweets,$parametros,$fecha_desde, $fecha_hasta, $user, $lista_tweets = array())
    {
        $total = count($retweets);
        $inicio = 0;

        foreach($retweets as $tweet)
        {
            $inicio ++;
            $fecha_tweet = date('Y-m-d H:i:s', strtotime($tweet->created_at));
            
            if(self::check_in_range($fecha_desde,$fecha_hasta,$fecha_tweet) == 1)
            {
                if(isset($tweet->retweeted_status))
                {
                   array_push($lista_tweets, array("cantidad" => $tweet->retweet_count, "texto" => $tweet->text, "id" =>$tweet->id_str)); 
                }
            }else if(self::check_in_range($fecha_desde,$fecha_hasta,$fecha_tweet) == 3){
                
                return $lista_tweets;
            }

            if($total == $inicio)
            {
                $parametros["max_id"] = $tweet->id_str;
                $retweets = $user->get("statuses/retweets_of_me", $parametros); 
                
                return array_merge($lista_tweets,self::top_retweets($retweets,$parametros,$fecha_desde, $fecha_hasta,$user,$lista_tweets));  
            }
        }          
    }
    
    private static function top_mentions($menciones,$parametros,$fecha_desde, $fecha_hasta, $user, $lista_menciones = array())
    {
        $total = count($menciones);
        $inicio = 0;

        foreach($menciones as $tweet)
        {
            $inicio ++;
            $fecha_tweet = date('Y-m-d H:i:s', strtotime($tweet->created_at));
            
            if(self::check_in_range($fecha_desde,$fecha_hasta,$fecha_tweet) == 1)
            {
                array_push($lista_menciones, array("cantidad" => $tweet->retweet_count, "texto" => $tweet->text, "id" =>$tweet->id_str)); 
            }else if(self::check_in_range($fecha_desde,$fecha_hasta,$fecha_tweet) == 3){
                
                return $lista_menciones;
            }
            
            if($total == $inicio)
            {
                $parametros["max_id"] = $tweet->id_str;
                $menciones = $user->get("statuses/mentions_timeline", $parametros); 
                
                return array_merge($lista_menciones,self::top_mentions($menciones,$parametros,$fecha_desde, $fecha_hasta,$user,$lista_menciones));  
            }
        }          
    }

    private static function retweetsTotal($tweets,$parametros,$fecha_desde, $fecha_hasta,$user)
    {
        $total = count($tweets);
        $inicio = 0;
        $suma = 0;

        foreach($tweets as $tweet)
        {
            $inicio ++;
            $fecha_tweet = date('Y-m-d H:i:s', strtotime($tweet->created_at));
            
            if(self::check_in_range($fecha_desde,$fecha_hasta,$fecha_tweet) == 1)
            {   
                if(isset($tweet->retweeted_status))
                {
                $suma += (int) $tweet->retweet_count;
                }
            }else if(self::check_in_range($fecha_desde,$fecha_hasta,$fecha_tweet) == 3){
                return $suma;
            }
            
            if($total == $inicio)
            {
                $parametros["max_id"] = $tweet->id_str;
                $tweets = $user->get("statuses/user_timeline", $parametros); 
                return $suma + (int)self::retweetsTotal($tweets,$parametros,$fecha_desde, $fecha_hasta,$user);  
            }
        }          
    }*/
    
    public static function getinformation($date_time,$fecha_inicio,$fecha_fin,$tipo, $indicador)
    {
        $aux = array();
        $sum = 0;
        
        $lista = json_decode($date_time, true);
        
        $indice_ini = date("Y-m-d",$fecha_inicio);
        $indice_fin = date("Y-m-d",$fecha_fin);
        
        if(count($lista) > 0)
        {

            if(array_key_exists($indice_ini, $lista))
            {
                                
            /*     $primer_indice = key(current($lista));
            if(strtotime($indice_ini) < strtotime($primer_indice))
            {
                $indice_ini = $primer_indice;
            }*/
                
                
                end($lista); 
                $ultimo_indice = key($lista);
                reset($lista);

                if(strtotime($indice_fin) > strtotime($ultimo_indice))
                {
                    $indice_fin = $ultimo_indice;
                }

                
                foreach ($lista as $key => $value) 
                {
                    if(strtotime($key) >= strtotime($indice_ini))
                    {
                        if($tipo == 2)
                        {
                           array_push($aux, $value[$indicador]) ;
                        }else{
                           $sum += $value[$indicador] ; 
                        }
                    }
                    
                    if(strtotime($key) == strtotime($indice_fin))
                    {
                       break; 
                    }
                }

                
                if($tipo == 2)
                {
                    if($indicador == 0 or $indicador == 3)
                    {
                       return $aux[0]; 
                    }
                    
                    return $aux;
                }else{
                   return $sum;
                } 
                
            }else{
                if($tipo == 2)
                {
                    if($indicador == 0 or $indicador == 3)
                    {
                       return 0; 
                    }
                    return array();
                }else{
                   return 0;
                }
            }
        }else{
            if($tipo == 2)
            {
                if($indicador == 0 or $indicador == 3)
                {
                   return 0; 
                }
                return array();
            }else{
               return 0;
            }
        } 
    }
    
    
    public static function followers($date_time,$fecha_inicio,$fecha_fin)
    {      
      /*  $aux = $user->get("account/verify_credentials");
        return $aux->followers_count;*/
        
      return self::getinformation($date_time,$fecha_inicio,$fecha_fin,2,0);
    }

    public static function mentions($date_time,$fecha_inicio,$fecha_fin)
    {
       
    }

    public static function retweets($date_time,$fecha_inicio,$fecha_fin)
    {
       return self::getinformation($date_time,$fecha_inicio,$fecha_fin,1,1);
       /* $parametros = array();
        $parametros["exclude_replies"] = true;
        $parametros["include_rts"] = false;
        $parametros["count"] = 200;
        $tweets = $user->get("statuses/mentions_timeline", $parametros);
        return self::retweetsTotal($tweets,$parametros,$fecha_inicio, $fecha_fin, $user);*/
    }

    public static function listed_count($date_time,$fecha_inicio,$fecha_fin)
    {
        return self::getinformation($date_time,$fecha_inicio,$fecha_fin,2,3);
       /* $user = $user->get("account/verify_credentials");
        return $user->listed_count;*/
    }

    public static function top_retweeted_posts($date_time,$fecha_inicio,$fecha_fin)
    {
       return self::getinformation($date_time,$fecha_inicio,$fecha_fin,2,4);
       /* $parametros = array();
        $parametros["exclude_replies"] = true;
        $parametros["include_rts"] = true;
        $parametros["count"] = 200;

        $tweets = $user->get("statuses/retweets_of_me", $parametros);
        
        return self::top_retweets($tweets,$parametros,$fecha_inicio, $fecha_fin, $user);*/
    }

    public static function top_mentions_by_followers_count($date_time,$fecha_inicio,$fecha_fin)
    {
        return self::getinformation($date_time,$fecha_inicio,$fecha_fin,2,5);
        /*$parametros = array();
        $parametros["exclude_replies"] = true;
        $parametros["include_rts"] = false;
        $parametros["count"] = 200;

        $tweets = $user->get("statuses/mentions_timeline", $parametros);
        return self::top_mentions($tweets,$parametros, $fecha_inicio, $fecha_fin, $user);*/
    }
}
