<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Dashboard_Home extends Controller_Dashboard_Template{
  
    public $title = "CMS :: INICIO"; 

    public function before()
    {  
        parent::before();
        
        $this->template->cabecera = null;
        $this->template->menu = null;
        View::set_global('margin', "style='margin:0px'");

        if($this->oUser->pasos == 3 )
        {
            if(!in_array( $this->request->action() ,array("editarautorizacion","eliminar","agregar","complete") ))
            {
               $this->redirect("/dashboard/dashboard"); 
            }
        }
    }
    
    public function action_index()
    {
        $this->template->content = View::factory('Dashboard/Home/index');
    }

    public function action_paso1()
    {
        $oUser = ORM::factory("User_User")
            ->where("id","=",$this->oUser->id)
            ->find();

        $oUser->pasos = 1;
        $oUser->save();

        Auth::instance()->complete_login($oUser);

        $lSocial = ORM::factory("Redes_Social")
            ->where("status","=",1)
            ->find_all();

        $lAutorizacion = ORM::factory("Redes_Autorizacion")
               ->where("user_id","=",$this->oUser->id)
               ->where("status","=",1)
               ->find_all();

        $this->template->content = View::factory("Dashboard/Home/paso1")->set(compact("lSocial","lAutorizacion"));          
    }
    
    public function action_paso2()
    {
        if($this->oUser->pasos == 0)
        {
           $this->redirect("/dashboard/home/paso1");  
        }
        
        $this->template->scripts = array(
            URL::base(TRUE) . "media/js/dashboard.home.paso2.js"
        );

        $oUser = ORM::factory("User_User")
            ->where("id","=",$this->oUser->id)
            ->find();

        $id_social = Session::instance()->get('id_social');

        $lAutorizacion = ORM::factory("Redes_Autorizacion")
                ->where("social_id","=",$id_social)
                ->where("user_id","=",$this->oUser->id)
                ->find_all();

        if ($this->request->method() == 'POST') 
        {
            if(!$aItem = $this->request->post("items"))
            {
                $oUser->pasos = 1;
                $oUser->save();

                Auth::instance()->complete_login($oUser);

                Message::danger("Debe seleccionar una o mas opciones");
                $this->redirect('dashboard/home/paso2'); 
            }

            $oUser->pasos = 2;
            $oUser->save();

            Auth::instance()->complete_login($oUser);

            Session::instance()->set('aItem',$aItem);
            $this->redirect('dashboard/home/paso3');
        }

        $this->template->content = View::factory("Dashboard/Home/paso2")->set(compact("lAutorizacion"));
    }
    
    public function action_paso3()
    {
        if($this->oUser->pasos == 0 or $this->oUser->pasos == 1)
        {
           $this->redirect("/dashboard/home/paso1");  
        }
        
        
        $this->template->scripts = array(
            URL::base(TRUE) . "media/js/dashboard.home.paso3.js"
        );

        $oUser = ORM::factory("User_User")
            ->where("id","=",$this->oUser->id)
            ->find();

        $aListadEmpresas = $oUser->aEmpresa->find_all();
        $aTipodashboard = ORM::factory('Rep_Tipodashboard')->where("status", "=", 1)->find_all();

        if ($this->request->method() == 'POST') 
        {
            $message = null;
            if(!$descripcion = $this->request->post("descripcion"))
            {
                $message = "descripcion";
            }

            if(!$tipo = $this->request->post("tipo"))
            {
                $message = "tipo de dashboard";
            }

            if(!$periodo = $this->request->post("periodo"))
            {
                $message = "periodo";
            }

            if(!$empresa_id = $this->request->post("empresa_id"))
            {
                $message = "cuenta";
            }

            if(!is_null($message))
            {  
                $oUser->pasos = 2;
                $oUser->save();

                Auth::instance()->complete_login($oUser);

                Message::danger("El campo ".$message." es un campo obligatorio para la creacion del dashboard");
                $this->redirect('dashboard/home/paso3');
            }

            $oUser = ORM::factory("User_User")
                    ->where("id","=",$this->oUser->id)
                    ->find();
            $oUser->pasos = 3;;
            $oUser->save();

            $oReporte = ORM::factory('Redes_Reporte');
            $oReporte->created_at = date('Y-m-d H:i:s');
            $oReporte->descripcion = $descripcion;
            $oReporte->tipo        = $tipo;
            $oReporte->plantilla   = 0;
            $oReporte->periodo     = $periodo;
            $oReporte->empresa_id  = $empresa_id;
            $oReporte->user_id     = $this->oUser->id;
            $oReporte->save();

            $aItem = Session::instance()->get('aItem');

            DB::delete('redes_reportedetalle')->where('redes_reporte_id', '=', $oReporte->id)->execute();

            foreach ($aItem as $value) 
            {
                $oReportedetalle                   = ORM::factory('Redes_Reportedetalle');
                $oReportedetalle->redes_reporte_id = $oReporte->id;
                $oReportedetalle->redes_item_id    = $value;
                $oReportedetalle->created_at       = date('Y-m-d H:i:s');
                $oReportedetalle->save();
            }

            Auth::instance()->complete_login($oUser);

            Session::instance()->delete('id_social');
            Session::instance()->delete('aItem');

            $this->redirect('dashboard/dashboard/view/'.$oReporte->id);
        }

        $this->template->content = View::factory("Dashboard/Home/paso3")->set(compact("aListadEmpresas","aTipodashboard"));

    }
    
    public function action_saltarintro()
    {
        $oUser = ORM::factory("User_User")
                        ->where("id","=",$this->oUser->id)
                        ->find();
        $oUser->pasos = 3;
        $oUser->save();
        Auth::instance()->complete_login($oUser);

        Session::instance()->delete('id_social');
        Session::instance()->delete('aItem');

        
        $this->redirect("/dashboard/dashboard");
    }
    
    public function action_editarautorizacion()
    {
        if($this->oUser->pasos != 3 )
        {
               $this->redirect("/dashboard/home"); 
        }
        
        $lSocial = ORM::factory("Redes_Social")
                    ->where("status","=",1)
                    ->find_all();
        
        $lAutorizacion = ORM::factory("Redes_Autorizacion")
                    ->where("user_id","=",$this->oUser->id)
                    ->where("status","=",1)
                    ->find_all();
        
        $margin = "";
        $menu = View::factory('Dashboard/menu');

        if($this->oUser->profile_id == 3)
        {
            $margin = "style='margin:0px'";
            $menu = null;
        }else{
            $seleccion = strtolower($this->request->controller()."/".$this->request->action());
            $menu->set('oUser', $this->oUser)->set('seleccion',$seleccion);
        }

        View::set_global('margin', $margin);
        
        $this->template->menu = $menu;
        $this->template->cabecera = View::factory('Dashboard/cabecera')->set('oUser', $this->oUser);
        $this->template->content = View::factory("Dashboard/Home/editarautorizacion")->set(compact("lSocial","lAutorizacion"));
    }
    
    public function action_eliminar()
    {
        $id = $this->request->param('id');
        
        $oAutorizacion = ORM::factory("Redes_Autorizacion",$id);
        
        if($oAutorizacion->loaded())
        {
           $oAutorizacion->status = 0;
           $oAutorizacion->save();
        }
        
        $this->redirect("dashboard/home/editarautorizacion");
    }
    
    public function action_agregar()
    {
        $id = $this->request->param('id');
        
        $oSocial = ORM::factory("Redes_Social")
                    ->where("id","=",$id)
                    ->where("status","=",1)
                    ->find();
        
        //Session::instance()->set('id_social', $id);
        
        $this->redirect("/dashboard/".strtolower($oSocial->name)."/index/".$id);
    }
    
    public function action_complete()
    {
        $id = $this->request->param('id');

        $oSocial = ORM::factory("Redes_Social")
            ->where("id","=",$id)
            ->where("status","=",1)
            ->find();

        $this->redirect("/dashboard/".strtolower($oSocial->name)."/setdata/".$id.URL::query());
    }
    

}
