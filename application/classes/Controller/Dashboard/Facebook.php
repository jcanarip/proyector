<?php defined('SYSPATH') or die('No direct access allowed.');

require_once Kohana::find_file("vendor","facebook/src/facebook/autoload"); 

class Controller_Dashboard_Facebook extends Controller_Dashboard_Template
{
    public function action_index()
    {
        $url = "https://www.facebook.com/dialog/oauth?client_id=".$this->fb_app_key."&redirect_uri=".$this->urlredireccionapis()."&scope=read_insights,manage_pages&auth_type=reauthenticate";
        $this->redirect($url);
    }
    
    public function action_setdata()
    {
        $id = $this->request->param('id');
        
        
        $auxtoken = file_get_contents("https://graph.facebook.com/v2.4/oauth/access_token?client_id=".$this->fb_app_key."&client_secret=".$this->fb_app_secret."&code=".$this->request->query("code")."&redirect_uri=".$this->urlredireccionapis());
        $auxtoken = json_decode($auxtoken); 
        
        $access_token = file_get_contents("https://graph.facebook.com/v2.4/oauth/access_token?grant_type=fb_exchange_token&client_id=".$this->fb_app_key."&client_secret=".$this->fb_app_secret."&fb_exchange_token=".$auxtoken->access_token);

        $access_token = json_decode($access_token); 
        
        //$url = "https://graph.facebook.com/v2.4/me/?fields=name,accounts&access_token=".$this->fb_access_token;
        
        //$url = "https://graph.facebook.com/v2.4/me/?fields=name,accounts&access_token=".$this->request->query("access_token_fb");
        
        $url = "https://graph.facebook.com/v2.4/me/?fields=name,accounts&access_token=".$access_token->access_token;
        $fUser = json_decode(file_get_contents($url));

        $oUser =  $this->oUser;

        $aAautorizacion = ORM::factory("Redes_Autorizacion")
                            ->where("social_id","=",$id)
                            ->where("user_id","=",$oUser->id)
                            ->where("uId","=",$fUser->id)
                            ->find();
        
        if($aAautorizacion->loaded())
        {
            if($aAautorizacion->status == 0)
            {
               $aAautorizacion->status = 1; 
            }
            
            $aAautorizacion->access = json_encode($access_token) ;
            $aAautorizacion->update_at = date("Y-m-d H:i:s");

        }else{
            $aAautorizacion->social_id = $id;
            $aAautorizacion->user_id = $oUser->id;
            $aAautorizacion->access = json_encode($access_token) ;
            $aAautorizacion->created_at = date("Y-m-d H:i:s");
            $aAautorizacion->uId = $fUser->id;
        }
        
        $aAautorizacion->username = $fUser->name;  
        $aAautorizacion->save();

        $query = DB::update('redes_item')
                ->set(array('status' => '0'))
                ->where('autorizacion_id', '=', $aAautorizacion->id);
        $query->execute();

        foreach ($fUser->accounts->data as $value)  
        {
            $oItem = ORM::factory("Redes_Item")
                    ->where("uId","=",$value->id)
                    ->where("autorizacion_id","=",$aAautorizacion->id)
                    ->find();

            if($oItem->loaded())
            {
                $oItem->update_at = date("Y-m-d H:i:s");
                $oItem->status = 1;
            }
            else
            {
                $oItem->uId = $value->id;
                $oItem->created_at = date("Y-m-d H:i:s");
                $oItem->autorizacion_id = $aAautorizacion->id;
            }
                
            $oItem->descripcion = $value->name;
            $oItem->save();
        }
        
        $this->redirectbyprofile();
    }
    
    public static function getdata($item,$accesos, $fecha_inicio, $fecha_fin, $funcion) 
    {
        $access_token = json_decode($accesos, true);
        $access_token =  $access_token["access_token"];
        
        try{
            return self::$funcion($item->uId,$fecha_inicio,$fecha_fin,$access_token,$funcion);
        } catch (Exception $exc) {
            Ajax::error($exc->getTraceAsString());
        }
    }
    
    //funciones de insights de posts del fanpage
    public static function post_insights($id,$fecha_inicio,$fecha_fin,$access_token,$funcion)
    {
        $url = "https://graph.facebook.com/v2.4/".$id."/posts?limit=100&access_token=".$access_token."&since=".$fecha_inicio."&until=".$fecha_fin;
        
        $lPost = json_decode(json_encode(self::search_next($url)));
        
        $lista = array();

        foreach ($lPost as $post) 
        {
            $url = "https://graph.facebook.com/v2.4/".$post->id."/insights/".$funcion."?fields=values&access_token=".$access_token;
            $lPost_detale = json_decode(file_get_contents($url));
            
            array_push($lista, array(
                "id" => $post->id,
                "texto" => (isset($post->message)) ? $post->message : $post->story,
                "cantidad" => (count($lPost_detale->data) > 0) ? $lPost_detale->data[0]->values[0]->value : "0" ,
            ));
        }
        return $lista;
    }
    
    public static function post_storytellers($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        return self::post_insights($id,$fecha_inicio,$fecha_fin,$access_token,"post_storytellers");
    }
    
    public static function post_stories($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        return self::post_insights($id,$fecha_inicio,$fecha_fin,$access_token,"post_stories");
    }
    
    public static function post_impressions_viral($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        return self::post_insights($id,$fecha_inicio,$fecha_fin,$access_token,"post_impressions_viral");
    }

    public static function post_impressions($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        return self::post_insights($id,$fecha_inicio,$fecha_fin,$access_token,"post_impressions");
    }
    
    public static function post_engaged_users($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        return self::post_insights($id,$fecha_inicio,$fecha_fin,$access_token,"post_engaged_users");
    }
    
    public static function post_likes($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        $url = "https://graph.facebook.com/v2.4/".$id."/posts?limit=100&access_token=".$access_token."&since=".$fecha_inicio."&until=".$fecha_fin;
        $lPost = json_decode(json_encode(self::search_next($url)));
        
        $lista = array();

        foreach ($lPost as $post) 
        {
            $url = "https://graph.facebook.com/v2.4/".$post->id."/likes?limit=100&access_token=".$access_token;
            $lPostDetalles = json_decode(json_encode(self::search_next($url)));

            array_push($lista, array(
                "id" => $post->id,
                "texto" => (isset($post->message)) ? $post->message : $post->story,
                "cantidad" => (count($lPostDetalles) > 0)? count($lPostDetalles) : "0",
            ));
        }
        return $lista;
    }
    
    public static function post_comments($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        $url = "https://graph.facebook.com/v2.4/".$id."/posts?limit=100&access_token=".$access_token."&since=".$fecha_inicio."&until=".$fecha_fin;
        $lPost = json_decode(json_encode(self::search_next($url)));
        
        $lista = array();

        foreach ($lPost as $post) 
        {
            $url = "https://graph.facebook.com/v2.4/".$post->id."/comments?limit=100&access_token=".$access_token;
            $lPostDetalles = json_decode(json_encode(self::search_next($url)));

            array_push($lista, array(
                "id" => $post->id,
                "texto" => (isset($post->message)) ? $post->message : $post->story,
                "cantidad" => (count($lPostDetalles) > 0)? count($lPostDetalles) : "0",
            ));
        }
        return $lista;
    }
    
    public static function post_fidelidad($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        $url = "https://graph.facebook.com/v2.4/".$id."/posts?limit=100&access_token=".$access_token."&since=".$fecha_inicio."&until=".$fecha_fin;
        $lPost = json_decode(json_encode(self::search_next($url)));
        
        $lista = array();
        
        foreach ($lPost as $post) 
        {
            $url = "https://graph.facebook.com/v2.4/".$post->id."/?fields=likes,comments,shares&limit=100&access_token=".$access_token;
            
            $aux = json_decode(file_get_contents($url),true);

            $lPostDetallesLika = 0;
            
            if(array_key_exists("likes",$aux))
            {
                if(array_key_exists("paging",$aux["likes"]))
                {

                    if(array_key_exists("next",$aux["likes"]["paging"]))
                    {
                        $lPostDetallesLika = json_decode(json_encode(self::search_next($aux["likes"]["paging"]["next"])));
                    }
                }
            }
            
            $lPostDetallesComments = 0;
            
            if(array_key_exists("comments",$aux))
            {
                if(array_key_exists("paging",$aux["comments"]))
                {
                    if(array_key_exists("next",$aux["comments"]["paging"]))
                    {
                        $lPostDetallesComments = json_decode(json_encode(self::search_next($aux["comments"]["paging"]["next"])));
                    }
                }
            }

            $lPostDetallesSharess = 0;
            
            if(array_key_exists("shares",$aux))
            {
                $lPostDetallesSharess = $aux["shares"]["count"];
            }

            
            array_push($lista, array(
                "id" => $post->id,
                "texto" => (isset($post->message)) ? $post->message : $post->story,
                "cantidad" => ((count($lPostDetallesLika)*1) + (count($lPostDetallesComments)*2) + ($lPostDetallesSharess*3)),
            ));
        }
        return $lista;
    }
    //funciones recursiva de paginacion
    public static function search_next($url)
    {
        $lPost = json_decode(file_get_contents($url),true);
        
        $listacompelta = array();
        $listacompelta = array_merge($listacompelta, $lPost["data"]); 

        if(isset($lPost["paging"]))
        {
           if(isset($lPost["paging"]["next"]))
           {
              return array_merge($listacompelta,self::search_next($lPost["paging"]["next"])); 
           }
        }
        return $listacompelta;
    }
    
    //funciones de insights del fanpage
    
    public static function page_insights($id,$fecha_inicio,$fecha_fin,$access_token,$funcion)
    {
        $url = "https://graph.facebook.com/v2.4/".$id."/insights/".$funcion."?fields=values,period&since=".$fecha_inicio."&until=".$fecha_fin."&access_token=".$access_token;
        $page_detalles = json_decode(file_get_contents($url));
        
        if(count($page_detalles->data))
        {

            $contador = 0;
            //print_r($page_detalles->data[0]->values);
            
            if($page_detalles->data[0]->period == "day")
            {
                foreach ($page_detalles->data[0]->values as $value) 
                {
                    $contador+= $value->value;
                }  
            }else{
                $contador = $page_detalles->data[0]->values[0]->value;
            }
            return $contador;
        }else{
            return 0;
        }

    }
    
    public static function page_impressions($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        return self::page_insights($id,$fecha_inicio,$fecha_fin,$access_token,"page_impressions");
    }
    
    public static function page_impressions_organic($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        return self::page_insights($id,$fecha_inicio,$fecha_fin,$access_token,"page_impressions_organic");
    }
    
    public static function page_engaged_users($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        return self::page_insights($id,$fecha_inicio,$fecha_fin,$access_token,"page_engaged_users");
    }
    
    public static function page_fans($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        return self::page_insights($id,$fecha_inicio,$fecha_fin,$access_token,"page_fans");
    }
    
    public static function page_impressions_viral($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        return self::page_insights($id,$fecha_inicio,$fecha_fin,$access_token,"page_impressions_viral");
    }
    
    public static function page_consumptions($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        return self::page_insights($id,$fecha_inicio,$fecha_fin,$access_token,"page_consumptions");
    }
    
    public static function page_story_adds($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        return self::page_insights($id,$fecha_inicio,$fecha_fin,$access_token,"page_story_adds");
    }
    
    public static function page_fan_adds_unique($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        return self::page_insights($id,$fecha_inicio,$fecha_fin,$access_token,"page_fan_adds_unique");
    }
    
    public static function page_storytellers($id,$fecha_inicio,$fecha_fin,$access_token)
    {
        return self::page_insights($id,$fecha_inicio,$fecha_fin,$access_token,"page_storytellers");
    }
}