<?php defined('SYSPATH') or die('No direct access allowed.');

require_once Kohana::find_file("vendor","google/src/Google/autoload"); 

class Controller_Dashboard_Ganalytic extends Controller_Dashboard_Template
{
    public function action_index()
    {
        $client = new Google_Client();
        $client->setApplicationName("Reportes Automatizados Analytic");
        $client->setClientId($this->an_app_key);
        $client->setClientSecret($this->an_app_secret);
        $url = filter_var($this->urlredireccionapis(),FILTER_SANITIZE_URL);
        $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
        $client->setRedirectUri($url);
        $client->setAccessType('offline');
        $client->setApprovalPrompt("force");
        $state = mt_rand();
        $client->setState($state);
        Session::instance()->set('state',$state);
        $this->redirect($client->createAuthUrl());
    }
    
    public function action_setdata()
    {
        if($this->request->query("error"))
        {
            $this->redirect("/dashboard/home/editarautorizacion/?error=error");
        }
        
        $id = $this->request->param('id');
        $oUser =  $this->oUser;

        $client = new Google_Client();
        $client->setApplicationName("Reportes Automatizados Analytic");
        $client->setClientId($this->an_app_key);
        $client->setClientSecret($this->an_app_secret);
        $url = filter_var($this->urlredireccionapis(),FILTER_SANITIZE_URL);
        $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
        $client->setRedirectUri($url);
        $client->setAccessType('offline');
        $client->setApprovalPrompt("force");

        $client->authenticate($this->request->query("code"));
        $token = json_decode($client->getAccessToken(), true) ;
        $token["app_key"] = $this->an_app_key;
        $token["app_secret"] = $this->an_app_secret;

        $analytics = new Google_Service_Analytics($client);
        
        $accounts = $analytics->management_accounts->listManagementAccounts();

        $aAautorizacion = ORM::factory("Redes_Autorizacion")
                            ->where("social_id","=",$id)
                            ->where("user_id","=",$oUser->id)
                            ->where("uId","=",$accounts->getUserName())

                            ->find();

        if(!$aAautorizacion->loaded())
        {
            $aAautorizacion->social_id = $id;
            $aAautorizacion->user_id = $oUser->id;
            $aAautorizacion->access = json_encode($token) ;
            $aAautorizacion->created_at = date("Y-m-d H:i:s");
            $aAautorizacion->uId = $accounts->getUserName();
        }
        
        $aAautorizacion->username = $accounts->getUserName();  
        $aAautorizacion->save();
       
        $query = DB::update('redes_item')
                ->set(array('status' => '0'))
                ->where('autorizacion_id', '=', $aAautorizacion->id);
        $query->execute();

        $views = array();
        foreach ($accounts->getItems() as $account) 
        {
            $propertys = $analytics->management_webproperties->listManagementWebproperties($account->getId());

            foreach ($propertys as $property) 
            {
                $views = $analytics->management_profiles->listManagementProfiles($account->getId(), $property["id"])->getItems(); 
                
                foreach ($views as $value) 
                {
                        
                    $oItem = ORM::factory("Redes_Item")
                            ->where("uId","=",$account->getId().".".$value->getWebPropertyId().".".$value->getId())
                            ->where("autorizacion_id","=",$aAautorizacion->id)
                            ->find();

                    if($oItem->loaded())
                    {
                        $oItem->status = 1;
                        $oItem->update_at = date("Y-m-d H:i:s");
                    }else
                    {
                        $oItem->uId =$account->getId().".".$value->getWebPropertyId().".".$value->getId();
                        $oItem->created_at = date("Y-m-d H:i:s");
                        $oItem->autorizacion_id = $aAautorizacion->id;
                    }

                    $oItem->descripcion = $value->getName()." (Profile ID:".$value->getId()."; Tracking ID: ".$value->getWebPropertyId()."; URL: ".$value->getWebsiteUrl().")";
                    $oItem->save();

                }
            }
        }

        $this->redirectbyprofile();
    }
    
    public static function getdata($item,$accesos, $fecha_inicio, $fecha_fin, $funcion) 
    {
        $access_token = json_decode($accesos, true);
        
        $client = new Google_Client();
        $client->setApplicationName("Reportes Automatizados Analytic");
        $client->setClientId($access_token["app_key"]);
        $client->setClientSecret($access_token["app_secret"]);

        $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
        //$client->setRedirectUri($url);
        $client->setAccessType('offline');
        $client->setApprovalPrompt("force");
        $client->setAccessToken($accesos);
        
         if($client->isAccessTokenExpired()) 
        {
            $newToken = json_decode($client->getAccessToken());
            $client->refreshToken($newToken->refresh_token);

            $accesos = json_decode($client->getAccessToken(),true);
            $accesos["app_key"] = $access_token["app_key"];
            $accesos["app_secret"] = $access_token["app_secret"];
            $access_token = $accesos;
            
            $aux = ORM::factory("Redes_Autorizacion",$item->oAutorizacion->id); 
            $aux->access = json_encode($accesos);
            $aux->save();
        }
        
        $analytics = new Google_Service_Analytics($client);

        $fecha_inicio = date("Y-m-d",$fecha_inicio);
        $fecha_fin = date("Y-m-d",$fecha_fin);

        /* $analyticsResponse_total = $youtubeAnalytics->reports->query(
            'channel=='.$channelId,
            $fecha_inicio,
            $fecha_fin,
            "subscribersGained,subscribersLost"
        );
         
         echo Debug::vars($analyticsResponse_total);
         die();*/
        
       try{
            return self::$funcion($item,$fecha_inicio,$fecha_fin,$analytics);
        } catch (Exception $exc) {
            Ajax::error($exc->getTraceAsString());
        }
        
    }
    
    public static function onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,$funcion)
    {
        $array = explode(".",$item->uId);
        
        $results = $analytics->data_ga->get(
              'ga:' . $array[2],
              $fecha_inicio,
              $fecha_fin,
              'ga:'.$funcion);

        if (count($results->getRows()) > 0) 
        {
            $rows = $results->getRows();
            return $rows[0][0];

        } else {
            return 0;
        }
    }
    
    public static function table($item,$fecha_inicio,$fecha_fin,$analytics,$funcion)
    {
        $array = explode(".",$item->uId);
        
        $results = $analytics->data_ga->get(
              'ga:' . $array[2],
              $fecha_inicio,
              $fecha_fin,
              'ga:sessions',array(
                  'dimensions' => 'ga:'.$funcion,
              ));

        $lista = array();
        
        if (count($results->getRows()) > 0) 
        {
            foreach ($results->getRows() as $key => $value) 
            {
                array_push($lista, array(
                               "id" => $key,
                               "texto" => $value[0],
                               "cantidad" => $value[1],
                           ));
            }
            
            return $lista;

        } else {
            return array();
        }
    }
    
    public static function sessions($item,$fecha_inicio,$fecha_fin,$analytics)
    { 
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'sessions');
    }
    
    public static function avgSessionDuration($item,$fecha_inicio,$fecha_fin,$analytics)
    { 
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'avgSessionDuration') ;
    }
    
    public static function adsensectr($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'adsenseCTR');
    }
    
    public static function impressions($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'impressions') ;
    }
    
    public static function costperconversion($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'costperconversion') ;
    }
    
    public static function adsenserevenue($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:adsenserevenue
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'adsenserevenue');
    }
    
    public static function adsenserevenuepervisit($item,$fecha_inicio,$fecha_fin,$analytics)
    {

        $cociente = self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'pageviews');
        
        if($cociente == 0)
        {
           return 0; 
        }
        
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'adsenserevenue') / $cociente ;
    }
    
    public static function avgscreenviewduration($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:avgscreenviewduration
        
        return gmdate("H:i:s", self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'avgscreenviewduration')*60); ;
    }
    
    public static function screenviews($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:screenviews
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'screenviews');
    }
    
    public static function screenviewspervisit($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:screenviews/ga:pageviews
        
        $cociente = self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'pageviews');
        
        if($cociente == 0)
        {
           return 0; 
        }
        
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'screenviews') / $cociente ;
    }
    
    public static function topscreen($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:screenName
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'screenName');
    }
    
    public static function pageviews($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:pageviews
       return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'pageviews') ;
    }
    
    public static function pagepervisit($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:pageviews/ga:uniquePageviews
        
         $cociente = self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'uniquePageviews');
        
        if($cociente == 0)
        {
           return 0; 
        }
        
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'pageviews') / $cociente ;
    }   
    
    public static function exitpagepath($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:exitpagepath
        
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'exitpagepath');
    }   
    
    public static function landingpagepath($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:landingpagepath
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'landingpagepath');
    }   
    
    public static function pagepath($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:pagepath
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'pagepath');
    }  
    
    public static function daystotransaction($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:daystotransaction
        
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'daystotransaction');
    }
    
    public static function itemspertransaction($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:itemQuantity/ga:transactions
        $cociente = self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'transactions');
        
        if($cociente == 0)
        {
           return 0; 
        }
        
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'itemQuantity') / $cociente ;
    }
    
    public static function transactionrevenue($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:transactionrevenue
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'transactionrevenue');
    }
    
    public static function transactions($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:transactions
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'transactions');
    }
    
    public static function eventcategory($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:eventcategory
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'eventcategory');
    }
    
    public static function eventaction($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:eventcategory+ga:eventaction
        
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'eventaction');
    } 
    
    public static function mobiledevicemodel($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:mobiledevicemodel
        
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'mobiledevicemodel');
    } 
    
    public static function mobiledevicebranding($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:mobiledevicebranding
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'mobiledevicebranding');
    } 
    
    public static function mobiledeviceinfo($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:mobiledeviceinfo
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'mobiledeviceinfo');
    } 
    
    public static function visitsfrommobile($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //suma de todos los modelos
        //return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'mobiledevicemodel');
        
        $lista = self::mobileinputselector($item,$fecha_inicio,$fecha_fin,$analytics);
        $sumatoria = 0;
        
        foreach ($lista as $value) 
        {
            $sumatoria+= $value["cantidad"];
        }
        
        return $sumatoria;        
    }
    
    public static function mobileinputselector($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:mobileinputselector
        
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'mobileinputselector');
    }
    
    
    public static function channelgrouping($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:channelGrouping
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'channelGrouping');
    } 
    
    public static function adcost($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:adCost
        
       return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'adCost');
    } 
    
    public static function goalconversionrateall($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:goalCompletionsAll / ga:sessions
        
        $cociente = self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'sessions');
        
        if($cociente == 0)
        {
           return 0; 
        }
        
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'goalCompletionsAll') / $cociente ;
    } 
    
    public static function goalcompletionsall($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:goalcompletionsall
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'goalCompletionsAll');
    } 
    
    public static function goalcompletionlocation($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:goalCompletionLocation
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'goalCompletionLocation');
    } 
    
    public static function goalValueAll($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:goalValueAll
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'goalValueAll');
    } 
    
    public static function adclicks($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:adclicks
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'adclicks');
    }
    
    public static function CTR($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:CTR
       return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'CTR');
    }
    
    public static function users($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:users
       return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'users');
    }
    
    public static function newUsers($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:newUsers
       return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'newUsers');
    }
    
    public static function newvsreturning($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //nuevas visitas / retornos de visitantes
        $lista = array();
        $returningporcentaje = 0;
        $returningvisitors = self::returningvisitors($item,$fecha_inicio,$fecha_fin,$analytics);
        $newvisitorsporcentaje = 0;
        $newUsers = self::newUsers($item,$fecha_inicio,$fecha_fin,$analytics);
        $cociente = self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'sessions');
        
        if($cociente > 0)
        {
            $returningporcentaje =  $returningvisitors*100 / self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'sessions');
            $newvisitorsporcentaje = $newUsers *100 / self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'sessions');
        }

        array_push($lista,array(
            "id" => 1,
            "texto" => "Returning Visitor (".round($returningporcentaje,2)."%)",
            "cantidad" => $returningvisitors));
        
        array_push($lista,array(
            "id" => 2,
            "texto" => "New Visitor (".round($newvisitorsporcentaje,2)."%)",
            "cantidad" => $newUsers));

        return $lista;
    }
    
    public static function returningvisitors($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //sessions - new users
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'sessions') - self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'newUsers');
    }
    
    public static function percentnewsessions($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:percentNewSessions
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'percentNewSessions');
    }
    
    public static function bouncerate($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:bouncerate
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'bouncerate');
    }
    
    public static function dayssincelastsession($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:dayssincelastsession
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'dayssincelastsession');
    }
    
    public static function  totaltimeonsite($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:sessions * ga:avgscreenviewduration
        $tiempo = self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'sessions') * self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'avgscreenviewduration');
        
        return gmdate("H:i:s", $tiempo*60);
    }
    
    public static function sessioncount($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:sessionCount
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'sessionCount');
    }
    
    public static function sourcemedium($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:sourcemedium
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'sourcemedium');
    }
    
    public static function source($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:source
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'source');
    }
    
    public static function referralpath($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:referralPath
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'referralPath');
    }
    
    public static function pageloadtime($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:pageloadtime
        return self::onlyresult($item,$fecha_inicio,$fecha_fin,$analytics,'pageloadtime');
    }
    
    public static function browser($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:browser
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'browser');
    }
    
    public static function browserversion($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:browserversion
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'browserversion');
    }
    
    public static function language($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:language
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'language');
    }
    
    public static function operatingsystem($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:operatingsystem
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'operatingsystem');
    }
    
    public static function operatingsystemversion($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:operatingsystemversion
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'operatingsystemversion');
    }
    
    public static function screenresolution($item,$fecha_inicio,$fecha_fin,$analytics)
    {
        //ga:screenresolution
        return self::table($item,$fecha_inicio,$fecha_fin,$analytics,'screenresolution');
    }
}