<?php defined('SYSPATH') or die('No direct script access.');

class Controller_App_Auth extends Controller_App_Template
{
    public $title = ""; 
    
    public function action_index()
    {
        $this->redirect("/app/auth/login"); 
    }
    
    public function action_login()
    {
        $this->check_user();

        //$this->title = "CMS :: LOGIN";

        if ($this->request->method() == 'POST')
        {
            $oUser = ORM::factory('User_User', array(
                    'email' => $this->request->post('email'),
                    'password' => $this->hashpass($this->request->post('password')),
            ));	

            if ($oUser->loaded())
            {
                
                    $oHistorial = ORM::factory('User_Historial');
                    $oHistorial->user_id = $oUser->id;
                    $oHistorial->action = "App/Auth/login";
                    $oHistorial->created_at = date('Y-m-d H:i:s');
                    $oHistorial->save();
                    
                    $this->complete_login($oUser);
                    

            }
            else
            {
                //Ajax::error('Email o Contraseña inválidos');
                $this->auto_render = FALSE;
                Message::danger('Email o Contraseña inválidos');
                $this->response->body(View::factory('App/Auth/login'));
            }
        }
        else
        {
            $this->auto_render = FALSE;
            $this->response->body(View::factory('App/Auth/login'));
        }
    }
    
    public function action_forget()
    {
        $this->check_user();
        $this->auto_render = FALSE;
        
        if ($this->request->method() == 'POST')
        {

            $email = $this->request->post('email');
            $oUser = ORM::factory('User_User', array(
                    'email' => $email
            ));

            if (!$oUser->loaded())
            {
                Message::danger('Este correo no existe.');
            }else{

                if($oUser->status == 4)
                {
                    Message::danger('Su cuenta no ha sido activada aùn.');
                }else if($oUser->status == 0)
                {
                    Message::danger('Su cuenta esta inactiva.');
                }else{
                    $nuevopass = $this->generarCodigo(8);

                    $oUser->password = $this->hashpass($nuevopass);
                    $oUser->save();
                    Message::success('Se ha enviado un email con el nuevo password.');

                    Email::factory('Cambio de contraseña ', 'Su nuevo password es '.$nuevopass)
                            ->to($oUser->email)
                            ->from('hola@proyectotribal.com', 'Proyecto R')
                            ->send();

                }

            }
                $this->response->body(View::factory('App/Auth/forget'));

        }
        else
        {
            $this->response->body(View::factory('App/Auth/forget'));
        }
    }
    
    private function generarCodigo($longitud) {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($pattern)-1;
        for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
        return $key;
    }
    
    public function action_register()
    {
        $this->check_user();

        //$this->title = "CMS :: LOGIN";
        
        $this->auto_render = FALSE;
        
        $captcha= Captcha::instance();

        if ($this->request->method() == 'POST')
        {
            if (Captcha::valid($this->request->post('captcha')))
            {
                $email = $this->request->post('email');
                $oUser = ORM::factory('User_User', array(
                        'email' => $email
                ));
                
                if ($oUser->loaded())
                {
                    Message::success('Este correo ya esta registrado.');
                }else{
                    $oUser->email = $email;
                    $oUser->profile_id = 2;
                    $oUser->status = 4;
                    $oUser->save();
                    Message::success('Se ha enviado la peticiòn de registro. En poco tiempo nos estaremos comunicando contigo.');
                    
                    $aUser = ORM::factory('User_User')
                            ->where("profile_id", "=", 1)
                            ->where("status","<>",0)
                            ->find_all();
                    
                    foreach($aUser as $oUser)
                    {
                        Email::factory('Cambio de contraseña ', 'El usuario con correo '.$email.' ha enviado una solicitud de registro.')
                                ->to($oUser->email)
                                ->from('hola@proyectotribal.com', 'Proyecto R')
                                ->send();
                    }
                    
                }
                    $this->response->body(View::factory('App/Auth/register')->set("captcha",$captcha));
            }
            else
            {
                Message::danger('Texto captcha incorrecto.');
                $this->response->body(View::factory('App/Auth/register')->set("captcha",$captcha));
            }
        }
        else
        {
            $this->response->body(View::factory('App/Auth/register')->set("captcha",$captcha));
        }
    }
    
    private function complete_login($oUser)
    {
        $remember = (bool) ($this->request->post('rememberme') OR $this->request->query('rememberme'));

        Auth::instance()->login($oUser, $remember);
        
        if($oUser->profile_id == 1)
        {
            $this->redirect("/app"); 
        }else{
            $this->redirect("/dashboard");  
        }
    }
    
    /*private function redirect($url = NULL)
    {
            if ( ! $redirect = $this->request->query('r'))
                    $redirect = '/';

            //$this->request->redirect($redirect ? $redirect : '');
            $this->response->body($url ? $url : $redirect);
    }*/
    	
    public static function hashpass($pass)
    {
            return sha1(md5($pass));
    }
	
    
    private function check_user()
    {
            if ($this->oUser)
                    $this->redirect("/dashboard");
    }
        
    public function action_logout()
    {
        Auth::instance()->logout();

        $this->redirect();
    }
}