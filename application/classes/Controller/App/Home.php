<?php defined('SYSPATH') or die('No direct script access.');
 /*
require_once Kohana::find_file("vendor","twitter/TwitterOAuth"); 
require_once Kohana::find_file("vendor","facebook/src/facebook/autoload"); 
require_once Kohana::find_file("vendor","google/src/Google/autoload"); */

class Controller_App_Home extends Controller_App_Template{
  
    public $title = "HOME :: REPORTE"; 
    public $lista_fechas = array();
    public $lista_horas = array();
    public $lista_tweets = array();
    public $fechahorainicio = null;
    public $fechahorafin = null;
    public $lista_menciones = array();
     
    public function before()
    {  
        parent::before();
    }
    
    public function action_index()
    {

        $content = View::factory('App/Landing/index'); 
      

        $this->template->content = $content;
    }


}
