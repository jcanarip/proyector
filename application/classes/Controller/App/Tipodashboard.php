<?php defined('SYSPATH') or die('No direct script access.');


class Controller_App_Tipodashboard extends Controller_App_Template
{
    
     public $title = ""; 
    
    public function action_index()
    {
        $this->title = "CMS :: TIPO DE DASHBOARD";
        
        $this->template->styles= array(
            URL::base(TRUE)."media/lib/data-tables/media/css/dataTables.bootstrap.css",
        );

        
        $this->template->scripts= array(
            URL::base(TRUE)."media/lib/data-tables/media/js/jquery.dataTables.js",
            URL::base(TRUE)."media/lib/data-tables/media/js/dataTables.bootstrap.js",
            URL::base(TRUE)."media/js/app.tipodashboard.index.js",
        );

        $this->template->content = View::factory('App/TipoDashboard/index');
    }
    
    public function action_list()
    {
        $empresa = ORM::factory('Rep_Tipodashboard');

        $paginate = Paginate::factory($empresa)
            ->columns(array('id','name','status'));

        $datatables = DataTables::factory($paginate)
            ->request($this->request)
            ->view('App/TipoDashboard/list')
            ->execute()
            ->render();
        
        $this->response->body($datatables);
    }
    
    public function action_crear()
    {
        if ($this->request->param('id')) $this->request->redirect('app/tipodashboard/crear');
        $this->action_editar(NULL);
    }
    
    public function action_editar($editar = TRUE)
    {
        if($editar)
        {
           if (!$id = $this->request->param('id')) $this->redirect('app/tipodashboard/'); 
        }else{
            $id = NULL;
        }
        
        $oTipoDashboard = ORM::factory('Rep_Tipodashboard',$id);

        
        if($this->request->method() == 'POST')
        {
            if($oTipoDashboard->loaded())
            {
               $oTipoDashboard->update_at = date('Y-m-d H:i:s'); 
            }else{
               $oTipoDashboard->created_at = date('Y-m-d H:i:s');  
            }

            $oTipoDashboard->values($this->request->post());
            $oTipoDashboard->save();
            
            Message::success("Guardado exitosamente!");
            $this->redirect('app/tipodashboard/'); 
        }
        
        if($id)
        {
            $this->title = "CMS :: TIPO DE DASHBOARD - EDITAR ".strtoupper($oTipoDashboard->name);
        }else{
            $this->title = "CMS :: TIPO DE DASHBOARD - CREAR";  
        }

        $this->template->content = View::factory('App/TipoDashboard/editar')
                                    ->set(compact("oTipoDashboard"));
    }
    
    public function action_eliminar()
    {
        $oTipoDashboard = ORM::factory('Rep_Tipodashboard', $this->request->param('id'));

        if ( ! $oTipoDashboard->loaded()) $this->request->redirect('app/tipodashboard/'); 

        $status = $oTipoDashboard->status;

        $oTipoDashboard->status = ($status == '1') ? '0' : '1';
        $oTipoDashboard->update_at = date('Y-m-d H:i:s');
        $oTipoDashboard->save();
        Message::success("Eliminado exitosamente!");
        //if ($status == 'blo' AND $oUser->status == 'cre') $this->active_user($oUser);

        $this->redirect('app/tipodashboard/'); 
    }
}