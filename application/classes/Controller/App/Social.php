<?php defined('SYSPATH') or die('No direct script access.');


class Controller_App_Social extends Controller_App_Template
{
    
     public $title = ""; 
    
    public function action_index()
    {
        $this->title = "CMS :: SOCIAL";
        
        $this->template->styles= array(
            URL::base(TRUE)."media/lib/data-tables/media/css/dataTables.bootstrap.css",
        );

        
        $this->template->scripts= array(
            URL::base(TRUE)."media/lib/data-tables/media/js/jquery.dataTables.js",
            URL::base(TRUE)."media/lib/data-tables/media/js/dataTables.bootstrap.js",
            URL::base(TRUE)."media/js/app.social.index.js",
        );

        $this->template->content = View::factory('App/Social/index');
    }
    
    public function action_list()
    {
        $empresa = ORM::factory('Redes_Social');

        $paginate = Paginate::factory($empresa)
            ->columns(array('id','name','descripcion','status'));

        $datatables = DataTables::factory($paginate)
            ->request($this->request)
            ->view('App/Social/list')
            ->execute()
            ->render();
        
        $this->response->body($datatables);
    }
    
    public function action_crear()
    {
        if ($this->request->param('id')) $this->request->redirect('app/social/crear');
        $this->action_editar(NULL);
    }
    
    public function action_editar($editar = TRUE)
    {
        if($editar)
        {
           if (!$id = $this->request->param('id')) $this->redirect('app/social/'); 
        }else{
            $id = NULL;
        }
        
        $oSocial = ORM::factory('Redes_Social',$id);

        
        if($this->request->method() == 'POST')
        {
            if($oSocial->loaded())
            {
               $oSocial->update_at = date('Y-m-d H:i:s'); 
            }else{
               $oSocial->created_at = date('Y-m-d H:i:s');  
            }

            $oSocial->values($this->request->post());
            $oSocial->save();
            
            Message::success("Guardado exitosamente!");
            $this->redirect('app/social/'); 
        }
        
        if($id)
        {
            $this->title = "CMS :: SOCIAL - EDITAR ".strtoupper($oSocial->name);
        }else{
            $this->title = "CMS :: SOCIAL - CREAR";  
        }

        $this->template->content = View::factory('App/Social/editar')
                                    ->set(compact("oSocial"));
    }
    
    public function action_eliminar()
    {
        $oSocial = ORM::factory('Redes_Social', $this->request->param('id'));

        if ( ! $oSocial->loaded()) $this->request->redirect('app/social/'); 

        $status = $oSocial->status;

        $oSocial->status = ($status == '1') ? '0' : '1';
        $oSocial->update_at = date('Y-m-d H:i:s');
        $oSocial->save();
        Message::success("Eliminado exitosamente!");
        //if ($status == 'blo' AND $oUser->status == 'cre') $this->active_user($oUser);

        $this->redirect('app/social/'); 
    }
}