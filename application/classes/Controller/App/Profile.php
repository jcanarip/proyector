<?php defined('SYSPATH') or die('No direct script access.');

class Controller_App_Profile extends Controller_App_Template
{
    public $title = ""; 
    
    public function action_index()
    {
        $this->title = "CMS :: PROFILE";
                    
        $this->template->styles= array(
            URL::base(TRUE)."media/lib/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css"
        );
        
        $this->template->scripts= array(
            URL::base(TRUE)."media/lib/moment-with-locales.js",
            URL::base(TRUE)."media/lib/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js",
            URL::base(TRUE)."media/js/app.profile.index.js"
        );
        $oUser = $this->oUser;

        $valid = Validation::factory($this->request->post())
                ->rule('first_name', 'min_length', array(':value', '3'))
                ->rule('first_name', 'max_length', array(':value', '50'))
                ->rule('last_name', 'min_length', array(':value', '3'))
                ->rule('last_name', 'max_length', array(':value', '50'))
                ->rule('email', 'not_empty')
                ->rule('email', 'email')
                ->rule('email', 'Model_User_User::unique_email_profile')
                ->rule('address', 'min_length', array(':value', '20'))
                ->rule('address', 'max_length', array(':value', '100'))
                ->rule('birthday', 'date')
                ->rule('genero', 'alpha')
                ->rule('genero', 'in_array', array(':value', array('M','F')))
        ->labels(array(
                'first_name'	=> 'Nombres',
                'last_name'		=> 'Apellidos',
                'email'			=> 'Email',
                'address'		=> 'Direcciòn',
        ));
        
        $contenido = View::factory("App/Profile/index");
        
        $auxUSer = ORM::factory("User_User",$oUser->id);
        
        if($this->request->method() == "POST")
        {
            if($valid->check())
            {
                if($auxUSer->loaded())
                {
                    $auxUSer->first_name = $this->request->post('first_name');
                    $auxUSer->last_name = $this->request->post('last_name');
                    $auxUSer->email = $this->request->post('email');
                    $auxUSer->address = $this->request->post('address');
                    $auxUSer->birthday = date("Y-m-d",strtotime($this->request->post('birthday')));
                    $auxUSer->genero = $this->request->post('genero');
                    $auxUSer->update_at = date("Y-m-d H:i:s");
                    $auxUSer->save();
                    
                    Auth::instance()->complete_login($auxUSer);
                    Message::success("Guardado exitosamente!");
                    
                }
                
            }else{
                   Message::danger($valid->errors('register'));
                //$error = current($errors);
                
            }
        }
        
        $contenido->set(compact('auxUSer'));
        
        $this->template->content = $contenido;
    }
    
}

