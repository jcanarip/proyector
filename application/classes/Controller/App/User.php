<?php defined('SYSPATH') or die('No direct script access.');

class Controller_App_User extends Controller_App_Template
{
    public $title = ""; 
    
    public function action_index()
    {
        $this->title = "CMS :: USERS";
        
        $this->template->styles= array(
            URL::base(TRUE)."media/lib/data-tables/media/css/dataTables.bootstrap.css",
        );
        
        $script =  URL::base(TRUE)."media/js/app.user.intro.js";
        
        
        if($this->oUser->aHistorial->count_all() > 1)
        {
            $script = null;
            unset($_COOKIE['value']);
        }
        
        
        if(isset($_COOKIE['value']))
        {
            if(base64_decode($_COOKIE['value'])  == 'user' )
            {
                $script = null;
                unset($_COOKIE['value']);
            }
        }
        
        $this->template->scripts= array(
            URL::base(TRUE)."media/lib/data-tables/media/js/jquery.dataTables.js",
            URL::base(TRUE)."media/lib/data-tables/media/js/dataTables.bootstrap.js",
            URL::base(TRUE)."media/js/app.user.index.js",$script,
        );

        $this->template->content = View::factory('App/User/index');
    }
    
    public function action_list()
    {  
        $user = ORM::factory('User_User');

        $paginate = Paginate::factory($user)
            ->columns(array('id','email','username','first_name','last_name','birthday','status'));

        $datatables = DataTables::factory($paginate)
            ->request($this->request)
            ->view('App/User/list')
            ->execute()
            ->render();
        
        $this->response->body($datatables);
    }

    
    public function action_crear()
    {
        if ($this->request->param('id')) $this->request->redirect('app/user/crear');
        $this->action_editar(NULL);
        
    }
    
    public function action_editar($editar = TRUE)
    {
        $this->template->styles= array(
            URL::base(TRUE)."media/lib/bootstrap-multiselect/css/bootstrap-multiselect.css",
            URL::base(TRUE)."media/lib/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css"
        );
        
        $this->template->scripts= array(
            URL::base(TRUE)."media/lib/moment-with-locales.js",
            URL::base(TRUE)."media/lib/bootstrap-multiselect/js/bootstrap-multiselect.js",
            URL::base(TRUE)."media/lib/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js",
            URL::base(TRUE)."media/js/app.empresa.editar.js"
        );
        
        if($editar)
        {
           if (!$id = $this->request->param('id')) $this->redirect('app/user/'); 
        }else{
            $id = NULL;
        }
        
        $oUser = ORM::factory('User_User',$id);
        
        $aProfile = ORM::factory('User_Profile')->find_all();
        
        $aEmpresa = ORM::factory('User_Empresa')->where('status','=',1)->find_all();
        
        $aUserEmpresa = array();
        $lista = $oUser->aEmpresa->find_all();
        
        foreach($lista as $value)
        {
            array_push($aUserEmpresa,$value->id);
        }
        
        $valid = Validation::factory($this->request->post())
                ->rule('first_name', 'min_length', array(':value', '3'))
                ->rule('first_name', 'max_length', array(':value', '50'))
                ->rule('last_name', 'min_length', array(':value', '3'))
                ->rule('last_name', 'max_length', array(':value', '50'))
                ->rule('email', 'not_empty')
                ->rule('email', 'email')
                ->rule('email', 'Model_User_User::unique_email_create')
                //->rule('address', 'min_length', array(':value', '20'))
                //->rule('address', 'max_length', array(':value', '100'))
               // ->rule('birthday', 'date')
                ->rule('genero', 'alpha')
                ->rule('genero', 'in_array', array(':value', array('M','F')))
        ->labels(array(
                'first_name'	=> 'Nombres',
                'last_name'		=> 'Apellidos',
                'email'			=> 'Email',
                'address'		=> 'Direcciòn',
        ));
        
        

        if($this->request->method() == 'POST')
        {
            if($valid->check())
            {
                if($oUser->loaded())
                {
                   $oUser->update_at = date('Y-m-d H:i:s'); 
                }else{
                   $oUser->created_at = date('Y-m-d H:i:s');  
                }

                $oUser->email = $this->request->post("email");
                $oUser->first_name = $this->request->post("first_name");
                $oUser->last_name = $this->request->post("last_name");
                //$oUser->birthday = date("Y-m-d",strtotime($this->request->post('birthday')));
                
                if($this->request->post("profile_id") == 3)
                {
                   $oUser->pasos = 3; 
                }
                
                $oUser->profile_id = $this->request->post("profile_id");
                $oUser->genero = $this->request->post("genero");
                //$oUser->address = $this->request->post("address");
                $oUser->password = $this->hashpass($this->request->post("email"));
               // $oUser->values($this->request->post());

                $oUser->save();
                
                if(is_null($id))
                {
                     Email::factory('Usuario Creado ', 'Ingresa a '.URL::base(TRUE).'. Su usuario es '. $oUser->email.' y su password es '. $oUser->email)
                        ->to($oUser->email)
                        ->from('hola@proyectotribal.com', 'Proyecto R')
                        ->send();
                }

                //DB::delete('user_userempresa')->where('user_user_id', '=', $oUser->id)->execute();
                if($this->request->post("profile_id") != 1)
                {
                    if($this->request->post("empresa"))
                    {
                        $oUser->remove('aEmpresa',$this->request->post("empresa"));

                        foreach($this->request->post("empresa") as $value)
                        {
                            $oUserempresa                   = ORM::factory('User_Userempresa');
                            $oUserempresa->user_user_id     = $oUser->id;
                            $oUserempresa->user_empresa_id  = $value;
                            $oUserempresa->created_at       = date('Y-m-d H:i:s');
                            $oUserempresa->save();
                        }
                    }else{
                         $oUser->remove('aEmpresa');
                    }
                }else{
                    $oUser->remove('aEmpresa');
                    
                    $aEmpresa = ORM::factory('User_Empresa')->find_all();
                    
                    foreach($aEmpresa as $value)
                    {
                       $oUserempresa                   = ORM::factory('User_Userempresa');
                       $oUserempresa->user_user_id     = $oUser->id;
                       $oUserempresa->user_empresa_id  = $value->id;
                       $oUserempresa->created_at       = date('Y-m-d H:i:s');
                       $oUserempresa->save();
                    }
                }


                Message::success("Guardado exitosamente!");
                $this->redirect('app/user/'); 
            
            }else{
                 Message::danger($valid->errors('register'));
            }
        }
        
        if($id)
        {
            $this->title = "CMS :: USERS - EDITAR ".strtoupper($oUser->username);
        }else{
            $this->title = "CMS :: USERS - CREAR";
        }
        
        

        $this->template->content = View::factory('App/User/editar')
                                    ->set(compact("oUser",'aProfile','aEmpresa','aUserEmpresa'));
    }
    
    public static function hashpass($pass)
    {
        return sha1(md5($pass));
    }
    
    public function action_eliminar()
    {
        $oUser = ORM::factory('User_User', $this->request->param('id'));

        if ( ! $oUser->loaded()) $this->request->redirect('app/user/'); 

        $status = $oUser->status;
        
        if($status == 4)
        {
            $oUser->password = $this->hashpass($oUser->email);
            $oUser->status = '1';
            $oUser->update_at = date('Y-m-d H:i:s');
            $oUser->save();
            Message::success("Usuario activado. El correo ha sido enviado satisfactoriamente.");
            
            Email::factory('Usuario Activado ', 'Su usuario es '.$email.' y su password es '.$email)
                    ->to($oUser->email)
                    ->from('hola@proyectotribal.com', 'Proyecto R')
                    ->send();
            
            $this->redirect('app/user/'); 
        }
        

        $oUser->status = ($status == '1') ? '0' : '1';
        $oUser->update_at = date('Y-m-d H:i:s');
        $oUser->save();
        Message::success("Eliminado exitosamente!");
        $this->redirect('app/user/'); 
                
    }
    
    public function action_changepass()
    {
        $valid = Validation::factory($this->request->post())
                ->rule('password', 'not_empty')
                ->rule('password', 'min_length', array(':value', 8))
                ->rule('npassword', 'matches', array(':validation', 'npassword', 'password'))
              
        ->labels(array(
                'password'	=> 'Contraseña',
                'npassword'	=> 'Nueva contraseña',
        ));
        
        $oUser = ORM::factory('User_User',$this->oUser->id);

        if($this->request->method() == 'POST')
        {
            if($valid->check())
            {
                if($oUser->loaded())
                {
                   $oUser->update_at = date('Y-m-d H:i:s'); 
                }else{
                   $oUser->created_at = date('Y-m-d H:i:s');  
                }

                $oUser->password = $this->hashpass($this->request->post("password"));
               // $oUser->values($this->request->post());

                $oUser->save();

                //DB::delete('user_userempresa')->where('user_user_id', '=', $oUser->id)->execute();

                Message::success("Cambiado exitosamente!");
                $this->redirect('app/user/changepass'); 
            
            }else{
                 Message::danger($valid->errors('password'));
            }
        }
        
        
        $this->title = "CMS :: USERS - CAMBIO DE PASSWORD";

        $this->template->content = View::factory('App/User/changepass');
    }

}