<?php defined('SYSPATH') or die('No direct script access.');

class Controller_App_Template extends Controller_Reporte {
   
    public $template = "App/template";

    public function before()
    {  

        parent::before(); 
        
        //$this->verificarSiSessionDeLogueoExiste(); 
        
        
        if ($this->auto_render)
        {
            $margin = "";
            
            $menu = View::factory('App/Landing/menu');
            
            if($this->oUser)
            {
                
                if($this->oUser->profile_id == 3 or $this->oUser->profile_id == 2)
                {
                    $margin = "style='margin:0px'";
                    $menu = null;
                }else{
                    $seleccion = strtolower($this->request->controller()."/".$this->request->action());
                    $menu ->set('oUser', $this->oUser)->set('seleccion', $seleccion);
                }
                
                if(($this->oUser->profile_id != 1 and in_array($this->request->controller() ,array("User","Empresa", "Home","Social") ) and !in_array($this->request->action() ,array("changepass") )))
                {
                    $this->redirect("/dashboard");  
                }
            }else{
                $margin = "style='margin:0px'";
                $menu = null;
            }
            
            View::set_global('margin', $margin);
            
            $this->template->title = $this->title;
            $this->template->scripts = array();
            $this->template->styles = array(); 
            $this->template->menu = $menu;
            $this->template->cabecera = View::factory('App/Landing/cabecera')
                                        ->set('oUser', $this->oUser);
        }
        
    } 

    public function after()
    {
        parent::after();
    }
    
   
}

?>