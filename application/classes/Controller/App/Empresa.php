<?php defined('SYSPATH') or die('No direct script access.');

class Controller_App_Empresa extends Controller_App_Template
{
    public $title = ""; 
        
    public function action_index()
    {
        $this->title = "CMS :: EMPRESAS";
        
        $this->template->styles= array(
            URL::base(TRUE)."media/lib/data-tables/media/css/dataTables.bootstrap.css",
        );

        
        $this->template->scripts= array(
            URL::base(TRUE)."media/lib/data-tables/media/js/jquery.dataTables.js",
            URL::base(TRUE)."media/lib/data-tables/media/js/dataTables.bootstrap.js",
            URL::base(TRUE)."media/js/app.empresa.index.js",
        );

        $this->template->content = View::factory('App/Empresa/index');
        
    }
    
    public function action_list()
    {
                
        $empresa = ORM::factory('User_Empresa');

        $paginate = Paginate::factory($empresa)
            ->columns(array('id','name','phone','adress','status'));

        $datatables = DataTables::factory($paginate)
            ->request($this->request)
            ->view('App/Empresa/list')
            ->execute()
            ->render();
        
        $this->response->body($datatables);
    }
    
    public function action_crear()
    {
        if ($this->request->param('id')) $this->request->redirect('app/empresa/crear');
        $this->action_editar(NULL);
        
    }
    
    public function action_editar($editar = TRUE)
    {
        if($editar)
        {
           if (!$id = $this->request->param('id')) $this->redirect('app/empresa/'); 
        }else{
            $id = NULL;
        }
        
        $oEmpresa = ORM::factory('User_Empresa',$id);

        
        if($this->request->method() == 'POST')
        {
            if($oEmpresa->loaded())
            {
               $oEmpresa->update_at = date('Y-m-d H:i:s'); 
            }else{
               $oEmpresa->created_at = date('Y-m-d H:i:s');  
            }

            $oEmpresa->values($this->request->post());
            $oEmpresa->save();
            
            $aUser = ORM::factory('User_User')
                     ->where("profile_id", "=", 1)
                     ->where("status","<>",0)
                     ->find_all();
            
            $aEmpresa = ORM::factory('User_Empresa')->find_all();
            
            foreach($aUser as $oUser)
            {
                $oUser->remove('aEmpresa');
                
                foreach($aEmpresa as $value)
                {
                   $oUserempresa                   = ORM::factory('User_Userempresa');
                   $oUserempresa->user_user_id     = $oUser->id;
                   $oUserempresa->user_empresa_id  = $value->id;
                   $oUserempresa->created_at       = date('Y-m-d H:i:s');
                   $oUserempresa->save();
                }
            }
            
            Message::success("Guardado exitosamente!");
            $this->redirect('app/empresa/'); 
        }
        
        if($id)
        {
            $this->title = "CMS :: EMPRESA - EDITAR ".strtoupper($oEmpresa->name);
        }else{
            $this->title = "CMS :: EMPRESA - CREAR";
        }

        $this->template->content = View::factory('App/Empresa/editar')
                                    ->set(compact("oEmpresa"));
    }

    
    public function action_eliminar()
    {
        $oEmpresa = ORM::factory('User_Empresa', $this->request->param('id'));

        if ( ! $oEmpresa->loaded()) $this->request->redirect('app/empresa/'); 

        $status = $oEmpresa->status;

        $oEmpresa->status = ($status == '1') ? '0' : '1';
        $oEmpresa->update_at = date('Y-m-d H:i:s');
        $oEmpresa->save();
        Message::success("Eliminado exitosamente!");
        //if ($status == 'blo' AND $oUser->status == 'cre') $this->active_user($oUser);

        $this->redirect('app/empresa/'); 
    }
                
}