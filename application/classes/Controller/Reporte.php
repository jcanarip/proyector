<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Reporte extends Controller_Template {
   
    public $template = "App/template";
    public $auto_render = TRUE;

    public function before()
    {  
// en caso haya problema con session 
//Session::instance()->destroy(TRUE, TRUE);
        if ($this->request->is_ajax())
            $this->auto_render = FALSE;


        parent::before(); 
               
        $this->load_session_data();
        
        if ($this->auto_render)
        {
            $this->template->title = $this->title; 
            $this->template->scripts = array();
            $this->template->styles = array();
        }
            
    } 

    public function after()
    {
        parent::after();
    }
    
    protected function load_session_data()
    {

        if (!Auth::instance()->logged_in())
        {
            if((in_array( $this->request->controller() ,array("Auth","Home","Campania") )  and in_array( $this->request->action() ,array("index","login","register","forget","iniciarcronjob","cronjob") )))
            {
                $this->oUser = Auth::instance()->get_user();
            }else{
                $this->redirect(URL::base(TRUE)); 
            }
        }else if(Auth::instance()->logged_in() || Auth::instance()->auto_login()){
            $this->oUser = Auth::instance()->get_user();
        }
    }
}

?>