<?php defined('SYSPATH') or die('No direct script access.');

class Pager {
	
	public $total;

	public $current;

	public $next;

	public $previous;

	public $offset;

	public function __construct($totalRecords, $pageSize, $current = 1)
	{
		$this->current = $current;

		$this->total = floor($totalRecords / $pageSize);

		$this->total = ($totalRecords % $pageSize) > 0 ? ($this->total + 1) : $this->total;

		$this->next = ($this->current < $this->total) ? ($this->current + 1) : NULL;

		$this->previous = (($this->current <= $this->total) AND ($this->current > 1)) ? ($this->current - 1) : NULL;

		$this->offset = (($this->current - 1) * $pageSize);
	}
}