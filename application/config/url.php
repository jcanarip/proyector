<?php defined('SYSPATH') OR die('No direct script access.');

return array(

	'trusted_hosts' => array(
		// Set up your hostnames here
		//
		// Example:
		//
		//        'example\.org',
		//        '.*\.example\.org',
		//
		// Do not forget to escape your dots (.) as these are regex patterns.
		// These patterns should always fully match,
		// as they are prepended with `^` and appended with `$`
            "www.tribal121.com",
            "tribal121.com",
            "localhost:8888",
            "serviciosmovistarperu.com",
	),

);
