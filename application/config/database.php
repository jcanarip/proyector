<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
	/*'default' => array
	(
		'type'       => 'MySQL',
		'connection' => array(

			'hostname'   => 'localhost',
			'database'   => 'bd_miller',
			'username'   => 'root',
			'password'   => '',
			'persistent' => FALSE,
		),
		'table_prefix' => '',
		'charset'      => 'utf8',
		'caching'      => FALSE,
	), */  
	'default' => array(
		'type'       => 'PDO',
		'connection' => array(
			/**
			 * The following options are available for PDO:
			 *
			 * string   dsn         Data Source Name
			 * string   username    database username
			 * string   password    database password
			 * boolean  persistent  use persistent connections?
			 */
			'dsn'        => 'mysql:host=tribal121.com;dbname=tribu121_proyecto121',
			'username'   => 'tribu121_proyect',
			'password'   => 'tribal121',
			'persistent' => FALSE,
              'options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'),
		),
		/**
		 * The following extra options are available for PDO:
		 *
		 * string   identifier  set the escaping identifier
		 */
		'table_prefix' => '',
		'charset'      => 'utf8',
		'caching'      => FALSE,
	),
);
