<?php defined('SYSPATH') or die('No direct script access.');

return array(
        'first_name' => array(
            'min_length'				=> 'Nombre debe tener minimo 3 caracteres',
            'max_length'				=> 'Nombre debe tener maximo 50 caracteres',
	),
        'last_name' => array(
            'min_length'				=> 'Apellido debe tener minimo 3 caracteres',
            'max_length'				=> 'Apellido debe tener maximo 50 caracteres',
	),
	'username' => array(
            'min_length'				=> 'Username debe tener minimo 3 caracteres',
            'max_length'				=> 'Username debe tener maximo 45 caracteres',
	),
    	'address' => array(
            'min_length'				=> 'Direccion debe tener minimo 20 caracteres',
            'max_length'				=> 'Direccion debe tener maximo 100 caracteres',
	),
        'birthday' => array(
            'date'					=> 'Fecha de nacimiento debe tener una fecha vàlida',
	),
    	'genero' => array(
            'alpha'					=> 'Genero debe tener un caracter vàlida',
            'in_array'					=> 'Genero debe tener uno de los dos caracteres validos (M o F)',
	),
	'email'	=> array(
            'not_empty'					=> 'Debe ingresar un email',
            'email'         				=> 'Debe ingresar un email válido',
            'Model_User_User::unique_email'		=> 'Ese email ya se encuentra registrado. Si olvidó su contraseña puede recuperarla haciendo click en "Olvidé mi contraseña"',
	),
);
