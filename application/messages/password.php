<?php defined('SYSPATH') or die('No direct script access.');

return array(
        'password' => array(
            'min_length'				=> 'Debe ingresar minimo 8 caracteres',
            'not_empty'					=> 'Debe ingresar un password',
	),
       
	'npassword'	=> array(
            'matches'					=> 'Ambas contraseñas deben coincidir',
         
	),
);
